/* Quarn OS / Manes
 *
 * Managed Execution Controller
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _MANEC_H_
#define _MANEC_H_

#include "cds/component.h"
#include "cds/type.h"
#include "libs/pointer.h"
#include "arch/low/irq.h"
#include "arch/low/general.h"

namespace resources {
	class did;
}

namespace services {
	class kernel_state;
}

namespace manes {
	class error;
	namespace cds {
		class root;
		class factory;
	}

	/* Managed Execution Controller */
	class manec : public cds::component {
	private:
		p<cds::root> main;
		p<cds::factory> types;

		p<services::kernel_state> kstate;

		arch::irq_op irqs;
public:
		bool usermode;
		static p<cds::component> comp;
		static p<manec> instance;
		manec();
	public:

		virtual p<cds::component> get_component(const cds::component_name &name);
		virtual p<cds::component> get_component(const string cname);

		/**
		 * @brief Get component specified by path.
		 * 
		 * @details This method parses given path and then uses multiple invocations
		 * of get_component method to get specified component. This is the syntax
		 * of component path:
		 * @code
		 * /type,id/type,id/type,id
		 * @endcode
		 * @param path Path to the requested component.
		 * @return Requested component.
		 */
		virtual p<cds::component> get_by_path(const string &path);

		/**
		 * @brief Get certain interface of the specific component.
		 *
		 * @details This is a part of simplified interface of Manes manager.
		 * It gets specified component using method get_by_path, then narrows
		 * it to the needed interface T.
		 * @param path Path to the requested component.
		 * @return Requested interface of the component.
		 */
       		template<typename T>
		p<T> get(const string &path);

		virtual void register_type(const string&, const string&, delegate<p<cds::component> >);
		virtual void register_driver(const string&, const string&, delegate<p<cds::component> >, delegate<bool, p<resources::did> >);

		/**
		 * @brief Register new type in Manes.
		 *
		 * @details When given a pointer to a completely initialized type this function
		 * inserts that type into internal Manes database, so that it is available
		 * for all clients and operations on components of that type are possible.
		 * @param newtype Pointer to newly created type.
		 */
		virtual void register_type(p<cds::type> newtype);

		/**
		 * @brief Get type from Manes.
		 *
		 * @details This method looks for requested type in internal Manes database.
		 * An error will occur if type wasn't previously registered.
		 * @param name Name of the requested type.
		 * @return Requested type.
		 */
		virtual p<cds::type> get_type(const cds::component_name &name);

		virtual p<cds::type> get_driver(p<resources::did>);

		virtual arch::irq_op *get_low();


		virtual void connect_manes(p<cds::root> root_mgr);
		static void connect_manes();

		static p<manec> get();

		virtual p<cds::root> get_root();
		virtual p<cds::factory> get_factory();

		virtual p<services::kernel_state> state();

		virtual p<error> err_msg(const string &x);

		inline bool far_calls() {
			return !in_kernel();
		}

		template<typename T>
		void register_type(const string&, const string&);

		template<typename T>
		void register_driver(const string&, const string&, delegate<bool, p<resources::did> >);

		virtual void register_abstract(const string&, const string&);
	};

	template<typename T, typename U>
	p<T> create_object() {
		return (T*)new U();
	}

template<typename T>
void manec::register_type(const string &me, const string &parent) {
	register_type(me, parent, delegate<p<cds::component> >::function(create_object<cds::component, T>));
}

template<typename T>
void manec::register_driver(const string &me, const string &parent, delegate<bool, p<resources::did> > check) {
	register_driver(me, parent, delegate<p<cds::component> >::function(create_object<cds::component, T>), check);
}

template<typename T>
p<T> manec::get(const string &path) {
	return get_by_path(path).cast<T>();
}

}

#include "error.h"

#endif
