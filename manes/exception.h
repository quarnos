#ifndef _EXCEPTION_H_
#define _EXCEPTION_H_

class string;
class exception {
private:
//	const string message;
//	const p<exception> inner;

public:
	exception() {}

	/* XXX: In C++0x reference to r-value may be useful here */
	exception(string);
//	exception(string, p<exception>);
//	exception(p<exception>);

	const string &get_message() const;
//	const p<exception> get_inner() const;
};

class null_pointer_exception : public exception { 
public:
	null_pointer_exception(const char *x) {}
};

class cpu_exception : public exception { };
class not_implemented_exception : public exception { };
class not_supported_exception : public exception { };
class arithmetic_exception : public exception { };

class runtime_exception : public exception { };

/* Argument exceptions */

class argument_exception : public exception {
public:
	argument_exception() { }
	argument_exception(const char*) { }
};
class argument_null_exception : public argument_exception { 
public:
	argument_null_exception() { }
	argument_null_exception(const char*) { }
};
class argument_out_of_range_exception : public argument_exception { };


class out_of_memory_exception : public exception { };
class invalid_cast_exception : public exception { };
class stack_overflow_exception : public exception { };
class index_out_of_range_exception : public exception { };

/* Manes exceptions */

class manes_exception : public exception { };
class component_not_found_exception : public manes_exception {
public:
	component_not_found_exception(const char *x) {}
};

class type_not_found_exception : public manes_exception {
public:
	type_not_found_exception() {}
	type_not_found_exception(const char *x) {}
};

class driver_not_found_exception : public type_not_found_exception {
public:
	driver_not_found_exception() {}
};

#endif
