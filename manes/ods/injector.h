/* Quarn OS / Manes / ODS
 *
 * Injector
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _INJECTOR_H_
#define _INJECTOR_H_

namespace manes {
	namespace ods {
		/**
		 * @brief Procedure Call Injector
		 * @details This is a base class for procedure call injectors, which
		 * allow to modify object vtable in the way that makes it easy
		 * to observe and catch all invocations of its methods.
		 */
		class injector {
		protected:
			delegate<void*,unsigned char*> catcher;

		public:
#if __GNUC__ == 4
			typedef gcc4_x86_injector default_injector;
#else
#error No injector found for your version of GCC.
#endif

			/**
			 * Set function to be called after caught invocation */
			virtual void set_catcher(delegate<unsigned int,unsigned char*>);

			/**
			 * @brief Inject catcher functions
			 *
			 * Inject catcher functions into each invocation of obj method in order
			 * to allow transport them through ODS network.
			 */
			virtual void inject(void *obj) = 0;
		};
	}
}

#endif
