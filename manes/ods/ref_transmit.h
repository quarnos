#ifndef _REF_TRANSMIT_H_
#define _REF_TRANSMIT_H_

namespace manes {
	namespace ods {
		/**
		 * @brief Stub of obj_ref instances
		 *
		 * @details Instances of this class are used to transport
		 * information about objects marshalled by references. On
		 * its basis are generated fake obj_ref instances that allow
		 * to access the real object.
		 */
		class rpc_stub : public serialized_object {
		public:
			virtual ~rpc_stub() { }
		 };

		/**
		 * @brief Serializer of objects transported by reference.
		 *
		 * @details This is a base class for all serializers able to 
		 * marshall objects that can be transmitted only by reference.
		 */
		class ref_transmit : public object_serializer {
		public:
			/**
			 * Serialize object
			 * This method serializes object by creating a stub which
			 * allows to locate and acces the certain instance of
			 * class from anywhere in ODS network.
			 * @param obj object that is to be serialized
			 * @return object stub, ready to be sent
			 */
			virtual serialized_object serialize(p<object> obj) = 0;

			/**
			 * Deserialize object
			 * This method creates a fake instance of obj_ref basing
			 * of received stub. That instance allows to transparently
			 * transport calls between client and server.
			 * @param stub serialized instance of obj_ref
			 * @return fake instance of obj_ref
			 */
			virtual p<object> deserialize(const serialized_object &stub) = 0;

		};
	}
}

#endif
