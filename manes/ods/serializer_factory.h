#ifndef _SERIALIZER_FACTORY_H_
#define _SERIALIZER_FACTORY_H_

namespace manes {
	namespace ods {
		/**
		 * @brief Factory of serializers
		 *
		 * @details This is a base class for factories that create
		 * three types of serializers: reference serializer, data serializer,
		 * call serializer.
		 */
		class serializer_factory {
		public:
			/** Create data serializer */
			virtual p<data_transmit> create_data_transmitter() const = 0;

			/** Create reference serializer */
			virtual p<ref_transmit> create_ref_transmitter() const = 0;

			/** Create call serializer */
			virtual p<rpc_caller> create_caller() const = 0;
		};
	}
}

#endif
