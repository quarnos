#ifndef _OBJECT_SERIALIZER_H_
#define _OBJECT_SERIALIZER_H_

namespace manes {
	namespace ods {
		/**
		 * @brief Object serializer
		 *
		 * @details This is a base class of all serializers that are
		 * able to marshall all, or only specific ods::object subclasses.
		 */
		class object_serializer {
		public:
			/**
			 * Serialize object
			 * @param obj object that is to be serialized
			 * @return serialized object, ready to be sent
			 */
			virtual serialized_object serialize(p<object> obj) = 0;

			/**
			 * Deserialize object
			 * @param buf serialized data
			 * @return ready to use object
			 */
			virtual p<object> deserialize(const serialized_object &buf) = 0;
		};
	}
}

#endif
