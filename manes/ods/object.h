#ifndef _OBJECT_H_
#define _OBJECT_H_

namespace manes {
	namespace ods {
		/**
		 * @brief Object in ODS
		 *
		 * @details This is a base class for all objects spported by
		 * Object Distribution System.
		 */
		class object {
		protected:
		public:
			virtual ~object() {}
		};
	}
}

#endif
