#include "efc.h"
#include "object.h"

#include "libs/pointer.h"

extern "C" void inject_efc(void*);
extern "C" void inject_remote_efc(unsigned int, unsigned int);
extern "C" unsigned int get_symbol_size(void*);

using namespace manes::ods;

p<rpc_caller> manes::ods::rpc_caller::instance;

extern "C" int functioncalls_count;
/* Injected function */
extern "C" int call_function(int (*method)(void*,struct __fill_size), int *stack) {
	functioncalls_count++;

	void *obj = (void*)stack[1];

	struct __fill_size *args = (struct __fill_size*)&stack[2];
	buffer buf(args, 32);
	return manes::ods::rpc_caller::get()->call_local((obj_ref*)obj, (manes::ods::rpc_caller::method_id)method, buf);
}


unsigned int efc::call(p<obj_ref>, method_id, buffer&) { return 0; }

unsigned int efc::call_local(p<obj_ref> obj, method_id method, buffer &stack) {
	struct fill_stack {
		char fill[32];
	};

	fill_stack *stck = (fill_stack*)stack.get_address();

	typedef unsigned int (*obj_method)(void*,fill_stack);
	obj_method mth = (obj_method)method;

	return mth((obj_ref*)obj, *stck);
}

unsigned int efc::call_far(p<obj_ref> obj, method_id method, buffer &stack) {
	unsigned int ret;
	__asm__("int	$0x33" : "=a" (ret) : "a" (method), "c" ((obj_ref*)obj), "d" (stack.get_address()));
	return ret;
}

unsigned int efc::call_remote(p<obj_ref>, method_id, buffer&) { return 0; }

void efc::inject(p<obj_ref> obj, caller_type tpe) {
	switch (tpe) {
		case local_call : 
			inject_efc((void*)(obj_ref*)obj);
			break;
		default: break;
	}
}

p<rpc_stub> efc::create_stub(p<obj_ref> obj, caller_type tpe) {
	p<efc_stub> stub = new efc_stub;
	stub->main = (void*)(obj_ref*)obj;

	/* Copy vtable */
	int *old_vtable = (int*)*(void**)(obj_ref*)obj;
	int vtable_size = get_symbol_size((void*)((unsigned int)old_vtable-8));
	old_vtable = (int*)((int)old_vtable - 8);

	void *vtable = (void*)new int[vtable_size/sizeof(int) + 3];
	for (unsigned int i = 0; i < vtable_size/sizeof(int)+2; i++)
		((int*)vtable)[i] = old_vtable[i];
	((int*)vtable)[vtable_size] = 0;

	stub->vtable = buffer(vtable, vtable_size + 12);

	/* Inject */
	switch (tpe) {
		case far_call : 
			inject_remote_efc((unsigned int)vtable + 8, stub->vtable.get_size() - 8);
			break;
		default: break;
	}

	return stub.cast<rpc_stub>();
}

p<obj_ref> efc::ref_from_stub(p<rpc_stub> _stub) {
	p<obj_stub> obj = new obj_stub;
	p<efc_stub> stub = _stub.cast<efc_stub>();
	obj->vtable = (void*)((unsigned int)stub->vtable.get_address() + 8);
	obj->object = stub->main;

	p<object> objstub = (object*)buffer::to_mem(obj).get_address();

	return objstub.cast<obj_ref>();
}
