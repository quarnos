#ifndef _OBJ_VAL_H_
#define _OBJ_VAL_H_

#include "object.h"

namespace manes {
	namespace ods {
		class object_stream;

		/**
		 * @brief Object marshalled by value
		 *
		 * @details This is a base class for all classes whose instances
		 * may be tranported by value.
		 */
		class obj_val : public object {
		public:
			/**
			 * Serialize itself
			 *
			 * This method invoked on object puts all its data into
			 * provided object_stream.
			 * @param ostr stream of objects to serialize to
			 */
			virtual void serialize(object_stream &ostr) = 0;

			/**
			 * Deserialize itself
			 *
			 * This method invoked on object gets all its data from
			 * provided object_stream.
			 * @param ostr stream of objects to deserialize from
			 */
			virtual void deserialize(object_stream &ostr) = 0;
		};
	}
}

#endif
