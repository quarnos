/* Quarn OS / Manes
 *
 * Execution Flow Controller
 * Function Injector for GCC 4.x
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "arch/low/fast_util.h"
#include "injector.h"

unsigned int syscalls_count = 0;
unsigned int functioncalls_count = 0;

/* Functions */
void *new_ventry(int impl);
void inject_caller(void *object);
int call_function(int (*method)(void*,struct __fill_size), int *stack);
void *get_vptr(void *instance);

/* Syscalls */
int do_syscall(int (*method)(void*,struct __fill_size), int *stack) {
	syscalls_count++;
	functioncalls_count--;
	int *stub = (int*)stack[1];
	stack[1] = stub[1];
	return call_function(method, stack);
}

int call_system(int (*method)(void*,struct __fill_size), int *stack) {
	int ret;
	__asm__("int	$0x33" : "=a" (ret) : "a" (method), "d" (stack));
	return ret;
}

/* Symbols from vcall.S */
extern char vcall;
extern char vmth;
extern char end_vcall;

extern char rcall;
extern char rmth;
extern char end_rcall;

/* TODO: dynamic structure instead of static */
#define comp_vtable_size	0x4000
char *comp_vtable = (char*)0x1b0000; //[comp_vtable_size];
int comp_vtable_ptr = 0;

extern unsigned int get_symbol_size(unsigned int);

/* Replace old vtable entry with hacked one */
void *new_ventry(int impl) {
	int entry_size = (int)&end_vcall - (int)&vcall;

	/* Put hacked vtable entry function in comp_vtable */
	memcpy(&comp_vtable[comp_vtable_ptr], &vcall, (int)&end_vcall - (int)&vcall);

	/* Set correct method address (vcall.S) */
	*(unsigned int*)((int)comp_vtable + comp_vtable_ptr + (int)&vmth - (int)&vcall + 1) = (unsigned int)impl;

	/* Get address of injected code */
	void *ventry = &comp_vtable[comp_vtable_ptr];
	comp_vtable_ptr += entry_size;

	if (comp_vtable_ptr >= comp_vtable_size)
		__asm__ ("cli\nhlt"::"a"(0xfeedbeef),"b"(entry_size));

	/* Return value to put in vtable */
	return ventry;
}

/* Replace old vtable entry with hacked one */
void *new_remote_ventry(int impl) {
	/* Put hacked vtable entry function in comp_vtable */
	memcpy(&comp_vtable[comp_vtable_ptr], &rcall, (int)&end_rcall - (int)&rcall);

	/* Set correct method address (vcall.S) */
	*(unsigned int*)((int)comp_vtable + comp_vtable_ptr + (int)&rmth - (int)&rcall + 1) = (unsigned int)impl;

	/* Get address of injected code */
	void *ventry = &comp_vtable[comp_vtable_ptr];
	comp_vtable_ptr += (int)&end_rcall - (int)&rcall;

	if (comp_vtable_ptr >= comp_vtable_size)
		__asm__ ("cli\nhlt"::"a"(0xfeedbeef));

	/* Return value to put in vtable */
	return ventry;
}

/* Inject EFC */
void inject_efc(void *object) {
	int *vtable = (int*)get_vptr(object);
	int size = get_symbol_size((unsigned int)vtable - 8) / sizeof(void*) - 2;

	/* Replace all entries in Vtable */
	for (int i = 0; i < size; i++)
		if (vtable[i] < 0x1b0000 || vtable > 0x2fffff)
		vtable[i] = (int)new_ventry(vtable[i]);
}

void inject_remote_efc(void *object, int size) {
	int *vtable = object;
	size = size / sizeof(void*) - 2;

	/* Replace all entries in Vtable */
	for (int i = 0; i < size; i++)
		if (vtable[i] < 0x1b0000 || vtable > 0x2fffff)
			vtable[i] = (int)new_remote_ventry(vtable[i]);
}


#if DEBUG_MODE == CONF_YES
unsigned int last_call0 = 0;
unsigned int last_call1 = 0;
#endif

/* Get Vtable of an object */
void *get_vptr(void *instance) {
	return *(void**)instance;
}

/* Vtable was hacked incorrectly */
void efc_incomplete() {
	while(1);
}

