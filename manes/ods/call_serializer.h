#ifndef _CALL_SERIALIZER_H_
#define _CALL_SERIALIZER_H_

#include "obj_ref.h"
#include "obj_val.h"

#include "libs/pointer.h"
#include "libs/buffer.h"

namespace manes {
	namespace ods {
		/**
		 * @brief Remote procedure call
		 *
		 * Instances of this class contains serialized data required
		 * to perform a rpc.
		 */
		class rpc_call : public serialized_object {
			virtual ~rpc_call() {}
		};

		/**
		 * @brief Functor
		 * 
		 * @details Instances of this class carries all data needed
		 * to perform a procedure call invoked by the client. They are
		 * used to represent deserialized remote procedure calls.
		 */
		class foreign_call {
		public:
			typedef unsigned int ret_val;

			/**
			 * Invokes functor
			 *
			 * This member function invokes procedure call requested
			 * by the client.
			 */
			virtual ret_val operator()() = 0;
		};

		/**
		 * @brief Call serializer
		 *
		 * @details This is a base class for all serializers able to
		 * marshall procedure invocation.
		 */
		class call_serializer {
		public:
			/**
			 * Serialize procedure call
			 * @param mid method identifier
			 * @param stk part of stack coinatining invocation arguments
			 * @return marshalled, ready to sent, data
			 */
			virtual p<rpc_call> serialize(ods_endpoint::method_id mid, ods_endpoint::stack stk) = 0;

			/**
			 * Deserialize procedure call
			 * @param buf serialized procedure call
			 * @return functor performing rpc
			 */
			virtual foreign_call deserialize(p<rpc_call> buf) = 0;

		};
	}
}

#endif
