#ifndef _ODS_ENDPOINT_H_
#define _ODS_ENDPOINT_H_

namespace manes {
	/**
	 * @brief Object Distribution System
	 *
	 * @details ODS is a vital part of Managed Execution System, which
	 * allows to distribute objects regardless of connection types
	 * between endpoints.
	 */
	namespace ods {
		/**
		 * @brief Endpoint in Object Distribution System
		 *
		 * @details Instances of this class represent an endpoint of
		 * Object Distribution System network. It is in charge
		 * of transmitting and receiving both data and
		 * procedure calls.
		 */
		class ods_endpoint {
		protected:
			/**
			 * Serializer of objects transported by value.
			 */
			p<data_transmit> data;

			/**
			 * Serializer of objects transported by reference.
			 */
			p<ref_transmit> ref;

			/**
			 * Procedure calls serializer.
			 */
			p<call_serializer> caller;

			/**
			 * Code injector.
			 */
			p<injector> inject_tool;

			/** Default serializers factory */
			typedef binary_serializers default_factory;
		public:
			/** Representation of public methods */
			typedef unsigned int method_id;
			typedef unsigned char* stack;

			/**
			 * Catch all calls of objects methods.
			 * @param obj observed object
			 */
			virtual void inject(p<obj_ref> obj) = 0;

			/**
			 * Connect to ODS network 
			 * @note Subclasses may require additional configuration,
			 * before connection will be established.
       			 */
			virtual void connect() = 0;

			/** Call caught */
			virtual void do_call(method_id, stack) = 0;
		};
	}
}

#endif
