/*
 * Execution Flow Controller
 * Function Injector for GCC 4.x
 */

namespace manes {
	namespace ods {
		/**
		 * @brief Injector for GCC4 x86
		 * @details This is procedure call injector that works on GCC 4
		 * x86 and compatible.
		 */
		class gcc4_x86_injector : public injector {
		private:
			void *get_vptr(void *instance);

		public:
			/** 
			 * @brief Hack vtable.
			 *
			 * @details Replaces default vtable entries with addresses of injected function which code
			 * is placed in vcall.S at label vcall. Note that functions are injected to all
			 * instances of the specific class at the same time.
			 * @param object Pointer to an instance of class that code has to be injected to.
			 */
			virtual void inject(void *obj);
		};
	}
}
