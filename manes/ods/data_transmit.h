#ifndef _DATA_TRANSMIT_
#define _DATA_TRANSMIT_

#include "object_stream.h"
#include "object_serializer.h"

namespace manes {
	namespace ods {
		/**
		 * @brief Data pack representing obj_val
		 *
		 * @details Instances of this class are data packs cointaining
		 * all fields of obj_val object.
		 */
		class data_pack : public serialized_object {
		public:
			virtual ~data_pack(){}
		};

		/**
		 * @brief Serializer of objects transported by value.
		 *
		 * @details This is a base class for all serializers able to
		 * marshall objects that can be transmitted only by their value.
		 */
		class data_transmit : public object_serializer {
		public:
			/**
			 * Get object stream 
			 * This method returns a stream of objects that can be
			 * used by an instance to serialize or deserialize
			 * itself.
			 * @return stream of objects
			 */
			virtual object_stream &get_stream() const = 0;

			/**
			 * Serialize object
			 * This method serializes object by creating a pack of data
			 * it is cointainging. In most implementations contribution
			 * of the object itself is required.
			 * @param obj object that is to be serialized
			 * @return pack of data, ready to be sent
			 */
			virtual serialized_object serialize(p<object> obj) = 0;

			/**
			 * Deserialzie object
			 * This method creates a exact copy of object basing on
			 * received pack of data. In most implementations
			 * contribution of the object itself is required.
			 * @param data received pack of data
			 * @return deserialized object
			 */
			virtual p<object> deserialize(const serialized_object &data) = 0;
		};
	}
}

#endif
