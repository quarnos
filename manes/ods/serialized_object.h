#ifndef _SERIALIZED_OBJECT_H_
#define _SERIALIZED_OBJECT_H_

namespace manes {
	namespace ods {
		/**
		 * @brief Serialized object
		 *
		 * @details This is a base class for all serialized types of
		 * data supported by ODS.
		 */
		class serialized_object {
		public:

	       		/**
			 * Creates buffer of bytes coinating this object.
			 * @return object represented as buffer of bytes
			 */
			virtual buffer to_mem() const;

			/**
			 * Creates an instance of serialized_object from
			 * provided buffer of bytes.
			 * @param buf buffer of bytes
			 * @return decoded object
			 */
			static serialized_object from_mem(const buffer &buf) const;
		};
	}
}

#endif
