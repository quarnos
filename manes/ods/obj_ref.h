#ifndef _OBJ_REF_H_
#define _OBJ_REF_H_

#include "object.h"
#include "obj_val.h"

#include "libs/pointer.h"

namespace manes {
	namespace ods {
		/**
		 * @brief Object marshalled by reference
		 *
		 * @details This is a base class for all classes whose instances
		 * may be tranported by reference.
		 */
		class obj_ref : public object {
		public:
			virtual ~obj_ref(){}
		};
	}
}

#endif
