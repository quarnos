#ifndef _OBJECT_STREAM_
#define _OBJECT_STREAM_

#include "libs/pointer.h"

#include "obj_val.h"
#include "obj_ref.h"

namespace manes {
	namespace ods {
		/**
		 * @brief Objects collector
		 *
		 * @details This class allows object_stream to store or to get
		 * objects, depending on their type and serialization method.
		 */
		class object_collector {
		public:
			/** Add plain array of bytes 
			 * @param buf address of the array
			 * @param size size of array in bytes
	       		 */
			virtual void get_int(char *buf, unsigned int size) = 0;

			/** Get plain array of bytes
			 * @param buf address of the array
			 * @param size size of array in bytes
			 */
			virtual void add_int(char *buf, unsigned int size) = 0;

			/** Get obj_ref at specified address
			 * @param addr place in memory where obj_ref is expected
			 * to be placed
			 */
			virtual void get_ref(obj_ref *addr) = 0;

			/** Get obj_ref
			 * @return address of obj_ref
			 */
			virtual obj_ref *get_ref() = 0;
			
			/** Add obj_ref
			 * @param addr addres of obj_ref
			 */
			virtual void add_ref(obj_ref *addr) = 0;

			/** Get obj_val at specified address
			 * @param addr place in memory where obj_val is expected
			 * to be placed
			 */
			virtual void get_val(obj_val *addr) = 0;

			/** Get obj_val
			 * @return address of obj_val
			 */
			virtual obj_val *get_val() = 0;
			
			/** Add obj_val
			 * @param addr addres of obj_val
			 */
			virtual void add_val(obj_val *addr) = 0;

			/** Set size of the data structure */
			virtual void set_size(int size) = 0;
		};

		/**
		 * @brief Stream of objects
		 * 
		 * @details Instances of this class are used to provide obj_vals
		 * with a possibility to manually deserialize themselves. In
		 * fact object_stream is a wrapper of object_collector class
		 * which provides more low-level interface.
		 */
		class object_stream {
		private:
			p<object_collector> collector;

		public:
			/**
			 * Creates stream that puts and gets data from specified
			 * object collector.
			 */
			object_stream(p<object_collector> col) : collector(col) { }

			/**
			 * Start serialization
			 * This method has to be invoked before serialization begins.
			 * If serialization has already started object_stream will
			 * ignore this call.
			 */
			template<typename T>
			void start() {
				collector->set_size(sizeof(T));
			}

			/**
			 * Add object
			 * This method adds object to the stream.
			 */
			template<typename T>
			object_stream &operator<<(T &obj) {
				collector->add_int((char*)&obj, sizeof(T));
				return *this;
			}

			/**
			 * Get object
			 * This method gets object from the stream in the order
			 * they were added to.
			 */
			template<typename T>
			object_stream &operator>>(T &obj) {
				collector->get_int((char*)&obj,0);
				return *this;
			}
		};

		template<>
		inline object_stream &object_stream::operator>><obj_val&>(obj_val &obj) {
			collector->get_val(&obj);
			return *this;
		}

		template<>
		inline object_stream &object_stream::operator>><p<obj_val> >(p<obj_val> &obj) {
			obj = collector->get_val();
			return *this;
		}

		template<>
		inline object_stream &object_stream::operator>><p<obj_ref> >(p<obj_ref> &obj) {
			obj = collector->get_ref();
			return *this;
		}

		template<>
		inline object_stream &object_stream::operator<<<obj_val&>(obj_val &obj) {
			collector->add_val(&obj);
			return *this;
		}

		template<>
		inline object_stream &object_stream::operator<<<p<obj_val> >(p<obj_val> &obj) {
			collector->add_val(&*obj);
			return *this;
		}

		template<>
		inline object_stream &object_stream::operator<<<p<obj_ref> >(p<obj_ref> &obj) {
			collector->add_ref(&*obj);
			return *this;
		}
	}
}

#endif
