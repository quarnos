#include "rpc_caller.h"

#include "object_stream.h"

namespace manes {
	namespace ods {
		class efc_stub : public rpc_stub {
		public:
			virtual ~efc_stub(){}
			buffer vtable;
			void *main;

			void serialize(manes::ods::object_stream &ostr) {
				ostr.start<efc_stub>();
				ostr << vtable;
				ostr << main;
			}

			void deserialize(manes::ods::object_stream &ostr) {
				ostr >> vtable;
				ostr >> main;
			}
		};

		class efc : public rpc_caller {
		protected:
			struct obj_stub {
				void *vtable;
				void *object;
			};

		public:
			virtual void inject(p<obj_ref> obj, caller_type tpe);
			virtual p<rpc_stub> create_stub(p<obj_ref> obj, caller_type tpe);
			virtual p<obj_ref> ref_from_stub(p<rpc_stub> stub);

			virtual unsigned int call(p<obj_ref>, method_id, buffer&);

			virtual unsigned int call_local(p<obj_ref>, method_id, buffer&);
			virtual unsigned int call_far(p<obj_ref>, method_id, buffer&);
			virtual unsigned int call_remote(p<obj_ref>, method_id, buffer&);

			static void create() {
				instance = new efc();
			}
		};
	}
}
