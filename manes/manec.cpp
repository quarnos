/* Quarn OS / Manes
 *
 * Manes Controller
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "cds/creator.h"
#include "manec.h"
#include "cds/type.h"
#include "error.h"
#include "cds/root.h"
#include "cds/driver.h"
#include "cds/factory.h"
#include "cds/abstract.h"
#include "services/kernel_state.h"

using namespace manes;
using namespace manes::cds;

p<component> manec::comp = (component*)0;
p<manec> manec::instance = (manec*)0;

manec::manec() : usermode(false) {}

void manec::connect_manes(p<root> root_mgr) {
        main = root_mgr;
	types = main->get_component(component_name::from_path("/factory"));
}

void manec::connect_manes() {
	manec *main;

	__asm__ ("int $0x32": "=a" (main));
	
	instance = main;
	main->usermode = true;
}

p<manec> manec::get() {
	if (!instance.valid()) {
		instance = new manec;
	//	p<type> tpe = new abstract(type::zero, type_name("nme",true), type_name("nme",true));
//		comp = new component((component*)0, tpe, instance.cast<implementation>());
	}

	return instance; //comp->get<manec>();
}

p<services::kernel_state> manec::state() {
	if (!kstate.valid())
		kstate = get<services::kernel_state>("/kernel_state");
	return kstate;
}
/*
bool manec::far_calls() {
	return usermode;
}*/

p<error> manec::err_msg(const string &x) {
	return state()->new_error(x);
}

p<component> manec::get_component(const component_name &obj) {
	return main->get_component(obj);
}

p<component> manec::get_component(const string cname) {
	return get_component(component_name::from_index(cname,0));
}

p<component> manec::get_by_path(const string &tpath) {
	return get_component(component_name::from_path(tpath));
}

void manec::register_type(const string &me, const string &parent, delegate<p<component> > deleg) {
	register_type(new type(component_name::from_path((string)"/type," + me), component_name::from_path((string)"/type," + parent), deleg));
}

void manec::register_driver(const string &me,const string &parent,  delegate<p<component> > deleg, delegate<bool, p<resources::did> > check) {
	register_type(new driver(component_name::from_path((string)"/type," + me), component_name::from_path((string)"/type," + parent), deleg, check));
	main->type_added(component_name::from_path((string)"/type," + me));
}

void manec::register_abstract(const string &me, const string &parent) {
	register_type(new abstract(component_name::from_path((string)"/type," + me), component_name::from_path((string)"/type," + parent)));
}

void manec::register_type(p<type> tpe) {
	if (!tpe.valid())
		throw new argument_null_exception();	

	types->add_existing(tpe);
}

p<type> manec::get_type(const component_name &tpe) {
	return types->get_component(tpe).cast<type>();
}

p<type> manec::get_driver(p<resources::did> id) {
	for (int i = 0; i < types->count(); i++) {
		p<type> drv = types->get_component(component_name::from_index("driver", i)).cast<type>();
		if (drv.cast<driver>().valid() && drv.cast<driver>()->check_device(id))
			return drv;
	}

//	throw new driver_not_found_exception();
	return p<type>::invalid;
}

arch::irq_op *manec::get_low() {
	return &irqs;
}

p<root> manec::get_root() {
	return main;
}


p<factory> manec::get_factory() {
	return types;
}
