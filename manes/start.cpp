/* Quarn OS
 *
 * Startup code
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */


/* This file consist of functions that are called in order to run Manes. It
 * includes initialization of hardware and internal kernel data structures.
 * After this operation kernel can change to the standard way of working witch
 * preemption, resources, actors and so on.
 */

/**
 * @mainpage Quarn OS Documentation
 * @image html http://quarnos.org/img/qoslogo.png
 */
#if __GNUC__ < 4
#error Quarn OS is needed to be compiled on GCC 4 or later.
#endif

#include "runtime.h"
#include "services/kernel_state.h"
#include "manec.h"
#include "manei.h"
#include "manes/error.h"
#include "resources/stream.h"
#include "arch/low/general.h"
#include "resources/block.h"

#include "arch/low/launch.h"
#include "resources/fs.h"
#include "services/logger.h"
#include "services/dns_client.h"
#include "resources/bus.h"
#include "resources/net/ethernet.h"
#include "resources/fat.h"
#include "resources/pci.h"
#include "resources/isa.h"
#include "libs/delegate.h"
#include "libs/string.h"
#include "modules/loader.h"
#include "resources/file.h"
#include "services/ntp_client.h"
#include "services/unmangler.h"
#include "actors/actor.h"
#include "actors/director.h"
#include "modules/module.h"
#include "resources/pio.h"
#include "libs/setjmp.h"
#include "services/stats.h"
#include "libs/pointer.h"
#include "resources/keybscr.h"
#include "typeinfo"
#include "resources/ne2k.h"
#include "resources/net/ipv4.h"
#include "resources/net/arp.h"
#include "resources/net/udp.h"
#include "resources/net/icmp.h"
#include "resources/net/tcp.h"

using namespace services;
using namespace modules;
using namespace resources;
using namespace manes;
using namespace arch;
using namespace actors;
using namespace net;

void next();

void *operator new(unsigned int, int);

extern char *comp_vtable;
extern p<nic> faar;

void launch_kernel();

extern "C" void jump_v86();

#include "arch/low/pit.h"
#if USE_MULTIBOOT == CONF_YES
extern "C" void start() {
#else
extern "C" void _start() {
#endif
	/* Prepare to execute C++ code */
	runtime_start();

	try {
		launch_kernel();
	}
	catch (exception *ex) {
		critical("WTF?");
	}

}

void launch_kernel() {
	delegate<void,const char> del;

	/* Launch Managed Execution System */
	manei::get()->launch_manes();

	manec::get()->connect_manes(manei::get()->get_root());

	p<manec> main = manec::get();

	/* Abstract types */
	main->register_abstract("device", "none");
	main->register_abstract("storage", "device");
	main->register_abstract("console", "device");
	main->register_abstract("nic", "device");

	main->register_abstract("allocator", "none");

	main->register_abstract("service", "none");

	/* Run low-level procedures */
	arch::launch();

	/* Register creators */
	manei::get()->register_creators();

	/* Register built in types */
	manei::get()->register_builtin_types();

	/* Run creators */
	manei::get()->animate_creators();

	/* Initialize clock */
	arch::pit_init();

	/* Manes is ready to use */
	p<logger> log = main->get<logger>("/early_logger");

	jmp_buf ab;
	if (!setjmp(ab)) {
		log->print("OK.\nStarting Quarn OS...",logger::log_critical);
		longjmp(ab,1);
	}

	p<stream> tty;

	try {
	//	throw 5;
		tty = main->get<stream>("/keybscr");
	}
	catch (char) {
		while(1);
	}
	catch (int) {
		
		log->print("cannot create tty", services::logger::log_info);
		//while(1);
	}

	p<stats> stat = main->get<stats>("/stats");

	p<logger> tlog = main->get<logger>("/tty_logger");
	tlog->print((string)"\n\nEFC caught so far " + stat->get_value(services::stats::efc_calls) + " function calls.", services::logger::log_info);

	p<bus> mainbus = main->get<bus>("/isa");
	tlog->print((string)"\nFound ISA devices: " + mainbus->get_count(), services::logger::log_info);

	p<bus> pbus = main->get<bus>("/pci");
	tlog->print((string)"\nFound PCI devices: " + pbus->get_count(), services::logger::log_info);

	tlog->print((string)"\nAvailable memory: " + manec::get()->state()->get_memory_size() / 1024 / 1024 + " MB\n\n", services::logger::log_info);

	if (faar.valid()) {
		ethernet eth;
		eth.set_nic(faar);

		ipv4 ip;
		ip.set_link_layer(&eth);
		ip.configure_ipv4(ipv4_addr::from_le(192 << 24 | 168 << 16 | 1 << 8 | 50), ipv4_addr::from_le(255 << 24 | 255 << 16 | 255 << 8), ipv4_addr::from_le(192 << 24 | 168 << 16 | 1 << 8 | 1));

		string str = "GET / HTTP/1.1\r\n\r\n";

		string qu = "quarnos.org";
		/*tcp t;
		t.set_internet_layer(&ip);
		p<client_socket> tcli = t.create_client();
		tcli->connect(ipv4_addr::from_le(192 << 24 | 168 << 16 | 1 << 8 | 3), 80);
		tcli->write(str.to_mem());
	//	tcli->write(str.to_mem());
		tcli->close();*/

		udp u;
		u.set_internet_layer(&ip);

		dns_client dns;
		dns.set(&u);
		ipv4_addr addr = dns.get_ipv4(qu);
		asm("cli\nhlt"::"a"(addr.to_le()));

		ntp_client ntp;
		ntp.set(&u);
		ntp.get_timestamp();

		icmp ping;
		ping.set_internet_layer(&ip);
		ping.ping(ipv4_addr::from_le(192 << 24 | 168 << 16 | 1 << 8 | 2));

		ping.ping(ipv4_addr::from_le(192 << 24 | 168 << 16 | 1 << 8 | 2));
	
		while (true) asm("hlt");
	}

	p<block> floppy = main->get<block>("/fdc");
	p<fs> fs_floppy = main->get<fs>("/fat");

	fs_floppy->mount(floppy);
	p<file> temp = main->get<file>("/file,authors");

	tlog->print((string)(const char*)((const string)*temp), services::logger::log_warning);

	p<module> mod = main->get<module>("/module,rs232.ko");

	mod->place(module::module_kernel_space);
	mod->get_delegate()();

	p<unmangler> unman = main->get<unmangler>("/unmangler");

	p<stream> serial = main->get<stream>("/console,1");

	string name = unman->unmangle(typeid(*serial).name());
	serial->write(name.to_mem());

	//jump_v86();

	p<director> sched = main->get<director>("/director");
	p<actor> proc = sched->new_component(cds::component_name::from_path("/type,actor")).cast<actor>();

	proc->set(delegate<void>::function(next));
/* Benchmark
	for (int i = 0; i < 10000; i++)
		for (int j = 0; j < 1000; j++)
			stat->get_value(stats::ks_timeticks);
//*/
	tlog->print((string)"EFC caught so far " + stat->get_value(services::stats::efc_calls) + " function calls.\n", services::logger::log_info);

//	string ss = stat->full_stats();
//	serial->write(ss.to_mem());

	sched->launch(proc);
}

void next() {
	p<manec> main = manec::get();

//	p<modules::module> mod = main->get<modules::module>("/module,shell.ko");
//	mod->place(module::module_kernel_space);

	p<director> sched = main->get<director>("/director");
//	p<actor> proc = sched->new_component((type_name)"actor")->get<actor>();
//	proc->set(mod->get_delegate());
//	sched->start(proc);

	/* PIO */
	p<stream> ttyS = main->get<stream>("/console,1");

	p<pio> str = new pio;

	string buf = "\n\xdHello World!!!";
	str->set_dep(ttyS);
	str->send_packet(buf.to_mem());
	str->set_speed(3);
	delegate<void> nul;

//	str->start(false, nul);

	str.dispose();

	while (1)  __asm__("hlt");
}
