/* Quarn OS / Manes
 *
 * Manes Initializer implementation
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "cds/creator.h"
#include "manec.h"
#include "manei.h"
#include "cds/type.h"

#include "services/server.h"
#include "services/tty_logger.h"
#include "services/early_logger.h"
#include "services/timer.h"
#include "services/unmangler.h"
#include "services/stats.h"
#include "services/interrupt_manager.h"
#include "services/kernel_state.h"
#include "services/asynch_tasks.h"

#include "resources/isa.h"
#include "resources/pci.h"
#include "resources/keybscr.h"
#include "resources/fdc.h"
#include "resources/fat.h"
#include "resources/speaker.h"
#include "resources/foma.h"
#include "resources/slob.h"
#include "resources/uhci.h"
#include "resources/file.h"
#include "resources/unknown.h"
#include "resources/virtual_bus.h"
#include "resources/usb_mass_storage.h"
#include "resources/pio.h"
#include "resources/physmem.h"
#include "resources/ne2k.h"
#include "resources/usb.h"
#include "resources/rtl8139.h"

#include "modules/loader.h"
#include "modules/module.h"
#include "modules/elf.h"

#include "actors/roundrobin.h"
#include "actors/actor.h"

using namespace manes;
using namespace manes::cds;

extern p<resources::memm> allocator;

p<manei> manei::instance = p<manei>::invalid;

p<manei> manei::get() {
	if (!instance.valid())
		instance = new manei;
	return instance;
}

void manei::launch_manes() {
	main = new root();
	types = new factory(new type(component_name::from_path("/type,factory"), component_name::invalid, delegate<p<component> >()));
	main->initialize(types);
}


void manei::register_creators() {
	/* server */
	services::server::register_type();

	/* loader */
	modules::loader::register_type();

	/* director */
	actors::roundrobin::register_type();

	/* fat */

	/* slob */
	resources::slob::register_type();

	/* foma */
	resources::foma::register_type();

	/* physmem */
	resources::physmem::register_type();

	/* isa-bus */
	resources::isa::register_type();

	/* pci-bus */
	resources::pci::register_type();

	/* virtual_bus */
	resources::virtual_bus::register_type();
}

void manei::register_builtin_types() {
	/* ---- SERVICES ---- */

	/* early logger */
	services::early_logger::register_type();

	/* tty logger */
	services::tty_logger::register_type();

	/* timer */
	services::timer::register_type();

	/* unmangler */
	services::unmangler::register_type();

	/* stats */
	services::stats::register_type();

	/* interrupt_manager */
	services::interrupt_manager::register_type();

	/* kernel_state */
	services::kernel_state::register_type();

	/* asynch_tasks */
	services::asynch_tasks::register_type();

	/* ---- DEVICES ---- */

	/* keybscr */
	resources::keybscr::register_type();

	/* fdc */
	resources::fdc::register_type();

	/* speaker */
	resources::speaker::register_type();

	/* pio */
	resources::pio::register_type();

	/* ne2k */
	resources::ne2k::register_type();

	/* usb */
	resources::usb::register_type();

	/* uhci */
	resources::uhci::register_type();

	/* unknown */
	resources::unknown::register_type();

	/* rtl8139 */
	resources::rtl8139::register_type();

	/* usb mass storage */
	resources::usb_mass_storage::register_type();

	/* ---- FILE SYSTEMS ---- */
	resources::fat::register_type();

	resources::file::register_type();

	/* ---- ACTORS ---- */
	actors::actor::register_type();

	/* ---- MODULES ---- */
	modules::module::register_type();

	modules::elf::register_type();
}

void manei::animate_creators() {
	/* animate server */
	main->new_component(component_name::from_path("/type,server")).cast<creator>()->initialize();

	kstate = main->new_component(component_name::from_path("/type,kernel_state")).cast<services::kernel_state>();

	main->new_component(component_name::from_path("/type,physmem"));//.cast<resources::memm>()->initialize();

	/* animate slob */
	allocator = main->new_component(component_name::from_path("/type,foma")).cast<resources::memm>();

	/* animate loader */
	main->new_component(component_name::from_path("/type,loader")).cast<creator>()->initialize();

	/* animate isa - main bus */
	main->new_component(component_name::from_path("/type,isa")).cast<creator>()->initialize();

	/* animate pci */
	main->new_component(component_name::from_path("/type,pci")).cast<creator>()->initialize();

	/* animate virtual_bus */
	main->new_component(component_name::from_path("/type,virtual_bus")).cast<creator>()->initialize();

	/* animate fat - main fs (temp) */
	main->new_component(component_name::from_path("/type,fat")).cast<creator>()->initialize();

	/* animate director */
	main->new_component(component_name::from_path("/type,director")).cast<creator>()->initialize();
}

p<root> manei::get_root() {
	return main;
}

p<services::kernel_state> manei::state() {
	return kstate;
}
