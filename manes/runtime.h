/* Quarn OS
 *
 * Runtime functions for C++ (header)
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

/* Header file that provides declaration of interfece for the startup code to
 * create a correct environment to execute the code written in C++.
 */

#ifndef _MANES_RUNTIME_H_
#define _MANES_RUNTIME_H_

void runtime_start();

namespace __cxxabiv1 {
	extern "C" {
		void __cxa_pure_virtual(void);
		void __cxa_atexit(void (*)(void*),void*,void*);
	}
}

namespace abi = __cxxabiv1;

#endif
