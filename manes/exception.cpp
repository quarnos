/* Exception Support
 * Based on DWARF2
 */

/* Copyright (C) 2009 Pawel Dziepak */

#include "typeinfo"
//#include "manes/error.h"

namespace std {
	void terminate() {
		asm("cli\nhlt"::"a"(0xdeadbeef));
	}
}

typedef char uint64[8];
typedef unsigned int cpu_word;

int strncpy(char*,const char*,int);
int memset(void*,int,int);

typedef enum {
	_URC_NO_REASON = 0,
	_URC_FOREIGN_EXCEPTION_CAUGHT = 1,
	_URC_FATAL_PHASE2_ERROR = 2,
	_URC_FATAL_PHASE1_ERROR = 3,
	_URC_NORMAL_STOP = 4,
	_URC_END_OF_STACK = 5,
	_URC_HANDLER_FOUND = 6,
	_URC_INSTALL_CONTEXT = 7,
	_URC_CONTINUE_UNWIND = 8
} _Unwind_Reason_Code;


struct _Unwind_Context {
	cpu_word eip;
	cpu_word ebp;
	cpu_word lsda;
	cpu_word proc;

	/* Phase 2 */
	cpu_word eax;
	cpu_word edx;
	cpu_word landing_pad;
};


typedef void (*_Unwind_Exception_Cleanup_Fn)(_Unwind_Reason_Code reason, struct _Unwind_Exception *exc);

struct _Unwind_Exception {
	uint64 exception_class;
	_Unwind_Exception_Cleanup_Fn exception_cleanup;
	uint64 private_1;
	uint64 private_2;
};


/* Personality Routine Actions */
typedef int _Unwind_Action;
static const _Unwind_Action _UA_SEARCH_PHASE = 1;
static const _Unwind_Action _UA_CLEANUP_PHASE = 2;
static const _Unwind_Action _UA_HANDLER_FRAME = 4;
static const _Unwind_Action _UA_FORCE_UNWIND = 8;

void jump_to(cpu_word eax, cpu_word edx, cpu_word ebp, cpu_word eip) {
	/* TODO: This routine should also set old esp value! */
	asm("movl	%%ecx, %%ebp\n" \
	    "pushl	%%esi\n" \
	    "ret" ::"a"(eax), "d"(edx), "c"(ebp), "S" (eip));
}

typedef _Unwind_Reason_Code (*personality_fn)(int version, _Unwind_Action actions, uint64 exception_class,_Unwind_Exception *ue_header, _Unwind_Context *context);
/* Based on DWARF3 Specification */

/* Read unsigned little endain base 128 value */
cpu_word read_uleb128(unsigned char *data, int &index) {
	cpu_word value = 0;
	unsigned char shift = 0;
	do {
		value |= ((data[index] & 0x7f)  << shift);
		shift += 7;
		index++;
	} while(data[index - 1] & 0x80);

	return value;
}

/* Read signed little endain base 128 value */
signed int read_sleb128(unsigned char *data, int &index) {
	signed int value = 0;
	unsigned char shift = 0;
	do {
		value |= ((data[index] & 0x7f)  << shift);
		shift += 7;
		index++;
	} while(data[index - 1] & 0x80);

	cpu_word minus = 0xffffffff & ~((1 << (shift - 1)) - 1);
	if (value & (1 << (shift - 1)))
		value |= minus;
	return value;
}

struct fde {
	cpu_word length;
	fde *cie_pointer;
	cpu_word initial_location;
	cpu_word address_range;
};
struct return_fde {
	fde *found_fde;
	cpu_word personality;
	cpu_word lsda;
};
	
cpu_word get_personality(cpu_word cie) {
	cie += sizeof(cpu_word) * 2; // length and cie_id
	for (; *reinterpret_cast<unsigned char*>(cie); cie++); // augmentation string
	int i = 1;
	read_uleb128(reinterpret_cast<unsigned char*>(cie), i); // code align
	read_uleb128(reinterpret_cast<unsigned char*>(cie), i); // data align
	read_uleb128(reinterpret_cast<unsigned char*>(cie), i); // return address index
	cie += i;
	cie++; // augmentation size

	if (! *reinterpret_cast<char*>(cie)) {
		cie++;
		return *reinterpret_cast<cpu_word*>(cie);
	} else
		return 0;
}

cpu_word get_lsda(fde *current) {
	cpu_word pointer = reinterpret_cast<cpu_word>(current);
	pointer += sizeof(fde);
	if (*reinterpret_cast<char*>(pointer) == 0 || *reinterpret_cast<char*>(pointer) == 4) {
		pointer++;
		return *reinterpret_cast<cpu_word*>(pointer);
	} else
		return 0;
}

return_fde get_fde(cpu_word eip) {
	extern void *_eh_frame;
	extern void *_eh_frame_end;

	fde *current = reinterpret_cast<fde*>(&_eh_frame);

	while ((cpu_word)current < (cpu_word)&_eh_frame_end) {
		/* do not want CIE */
		if (current->cie_pointer != (fde*)0) {

			if (eip >= current->initial_location
			    && eip <= current->initial_location + current->address_range) {
				return_fde ret;
				ret.found_fde = current;
				cpu_word cie = (cpu_word)current + sizeof(unsigned int) - (cpu_word)current->cie_pointer;
				ret.personality = get_personality(cie);
				ret.lsda = get_lsda(current);

				return ret;
			}
		}

		current = reinterpret_cast<fde*>(reinterpret_cast<cpu_word>(current) + current->length + sizeof(cpu_word));
	}
}

/* Based on IA-64 ABI Specification */


struct __cxa_exception { 
	std::type_info *exceptionType;
	void (*exceptionDestructor) (void *); 
/*	unexpected_handler unexpectedHandler;
	terminate_handler terminateHandler;*/
	__cxa_exception * nextException;

	int handlerCount;
	int handlerSwitchValue;
	const char *actionRecord;
	const char *languageSpecificData;
	void *catchTemp;
	void *adjustedPtr;

	_Unwind_Exception unwindHeader;
};

struct __cxa_eh_globals {
	__cxa_exception *caughtExceptions;
	cpu_word uncaughtExceptions;
};

__cxa_eh_globals eh_globals;

__cxa_eh_globals *__cxa_get_globals(void) {
	return &eh_globals;
}

extern "C" void _Unwind_Resume(_Unwind_Exception ex) {
}

struct frame_pointer {
	frame_pointer *next;
	cpu_word eip;
};

cpu_word _Unwind_GetIP(struct _Unwind_Context *context) {
	return context->eip;
}

cpu_word _Unwind_GetCFA(struct _Unwind_Context *context) {
	return context->ebp;
}

cpu_word _Unwind_GetLanguageSpecificData(struct _Unwind_Context *context) {
	return context->lsda;
}

cpu_word _Unwind_GetRegionStart(struct _Unwind_Context *context) {
	return context->proc;
}

void _Unwind_SetGR(_Unwind_Context *context, int index, cpu_word new_value) {
	if (index == 0)
		context->eax = new_value;
	else
		context->edx = new_value;
}

void _Unwind_SetIP(_Unwind_Context *context, cpu_word new_value) {
	context->landing_pad = new_value;
}


void *get_eh_frame();

void InstallContext(_Unwind_Context *context) {
	/* TODO: Set old esp and register values (other than eax and edx)! */
	jump_to(context->eax, context->edx, context->ebp, context->landing_pad);
}

extern "C" _Unwind_Reason_Code _Unwind_RaiseException(_Unwind_Exception *ex) {

	/* Phase 1
	 * Ask personality at each eip if there is an exception handler.
	 */
	get_eh_frame();
	frame_pointer *current_fp;
	personality_fn personality;
	_Unwind_Context context;
	__asm__("movl	%%ebp, %%eax" : "=a" (current_fp));
	current_fp = current_fp->next;
	do {
		if (!current_fp)
			return _URC_END_OF_STACK;

		cpu_word return_point = current_fp->eip;

		return_fde ret = get_fde(return_point);
		context.eip = return_point;
		context.ebp = reinterpret_cast<cpu_word>(current_fp->next);
		context.proc = ret.found_fde->initial_location;
		context.lsda = ret.lsda;

		union {
			cpu_word val;
			personality_fn personality;
		} cast;
		/* UD and what? All this code is implementation-specific */
		cast.val = ret.personality;
		personality = cast.personality;

		current_fp = current_fp->next;

	} while(personality(1, _UA_SEARCH_PHASE, ex->exception_class, ex, &context) != _URC_HANDLER_FOUND);

	/* Phase 2
	 * Ask personality to perform cleanup at each eip and eventually jump to the handler.
	 */

	_Unwind_Context handler = context;
	__asm__("movl	%%ebp, %%eax" : "=a" (current_fp));
	do {
		cpu_word return_point = current_fp->eip;

		return_fde ret = get_fde(return_point);
		context.eip = return_point;
		context.ebp = reinterpret_cast<cpu_word>(current_fp->next);
		context.proc = ret.found_fde->initial_location;
		context.lsda = ret.lsda;

		union {
			cpu_word val;
			personality_fn personality;
		} cast;
		/* UD and what? All this code is implementation-specific */
		cast.val = ret.personality;
		personality = cast.personality;

		context.eip = return_point;
		context.ebp = reinterpret_cast<cpu_word>(current_fp->next);

		current_fp = current_fp->next;
	} while (context.ebp != handler.ebp && personality(1, _UA_CLEANUP_PHASE, ex->exception_class, ex, &context) != _URC_FATAL_PHASE2_ERROR);

	/* Call handler */
	personality(1, _UA_HANDLER_FRAME, ex->exception_class, ex, &handler);
}

/*

struct lsda_header_info
{
  _Unwind_Ptr Start;
  _Unwind_Ptr LPStart;
  const unsigned char *TType;
  const unsigned char *action_table;
  unsigned char ttype_encoding;
  unsigned char call_site_encoding;
};
*/

struct lsda_header {
	cpu_word lpstart;
	unsigned char *ttype;
	cpu_word call_site_encoding;
	unsigned char *action_table;
	unsigned char *call_site;
};

#define DW_EH_PE_omit 0xff

lsda_header read_lsda_header(unsigned char *lsda, _Unwind_Context *context) {
	lsda_header info;
	unsigned char encoding = *lsda++;

	/* Read LPStart */
/*	if (encoding != 0xff) {
	//	while(1);
	} else {*/
		info.lpstart = _Unwind_GetRegionStart(context);
//	}

	/* Read TType */
	encoding = *lsda++;
	if (encoding != 0xff) {
		int i = 0;
		cpu_word ttype = read_uleb128(lsda, i);
		info.ttype = reinterpret_cast<unsigned char*>(ttype + reinterpret_cast<cpu_word>(lsda) + i);
		lsda += i;
	}

	/* Read Call-site entries encoding */
	info.call_site_encoding = *lsda++;

	/* Read Action table */
	int i = 0;
	cpu_word action = read_uleb128(lsda, i);
	info.action_table = reinterpret_cast<unsigned char*>(action + reinterpret_cast<cpu_word>(lsda) + i);
	lsda += i;

	/* Call-site */
	info.call_site = lsda;
	
	return info;
}

extern "C" _Unwind_Reason_Code __gxx_personality_v0(int version, _Unwind_Action actions, uint64 exception_class, _Unwind_Exception *ue_header, _Unwind_Context *context) {
	if (version != 1)
		return _URC_FATAL_PHASE1_ERROR;

	if (actions == _UA_SEARCH_PHASE) {
		cpu_word lsda = _Unwind_GetLanguageSpecificData(context);

		if (!lsda)
			return _URC_CONTINUE_UNWIND;

		lsda_header hdr = read_lsda_header(reinterpret_cast<unsigned char*>(lsda), context);
		cpu_word ip = _Unwind_GetIP(context) - 5; // sizeof(call addr32) == 5

		// we assume that call_site encoding == uleb128
		int i = 0;
		cpu_word cs_ip = 0, cs_len = 0, cs_lp = 0, cs_action = 0;
		do {
			/* Go through call site table (look for ip) */
			cs_ip = read_uleb128(hdr.call_site, i) + hdr.lpstart;
			cs_len = read_uleb128(hdr.call_site, i);
			cs_lp = read_uleb128(hdr.call_site, i);
			cs_action = read_uleb128(hdr.call_site, i) - 1;

			if (cs_ip == ip) {
				int next;
				__cxa_exception *full_exception = (__cxa_exception*)((cpu_word)ue_header - sizeof(__cxa_exception) + sizeof(_Unwind_Exception));

				/* Go through action table (look for type to catch) */
				do {
					int position = cs_action;

					int action_id = read_uleb128(hdr.action_table, position);
					if (action_id == 0)
						break;

					cs_action = position;
					next = read_sleb128(hdr.action_table, position);

					std::type_info *type = *reinterpret_cast<std::type_info**>(reinterpret_cast<cpu_word>(hdr.ttype) - action_id * sizeof(cpu_word));

					if (*type == *full_exception->exceptionType) {
						/* cache collected data */

						/* F*ck cleanup phase, jump now */
						_Unwind_SetGR(context, 0, reinterpret_cast<cpu_word>(ue_header));
						_Unwind_SetGR(context, 1, action_id);
						_Unwind_SetIP(context, hdr.lpstart + cs_lp);
						InstallContext(context);

						return _URC_HANDLER_FOUND;
					}

					cs_action += next;
				} while (next);
				
			}
		} while(reinterpret_cast<cpu_word>(&hdr.call_site[i]) <= reinterpret_cast<cpu_word>(hdr.action_table));

		return _URC_CONTINUE_UNWIND;
		
	} else if (actions == _UA_CLEANUP_PHASE) {
	
	} else if (actions == _UA_HANDLER_FRAME) {

	}
}


extern "C" void *__cxa_allocate_exception(cpu_word thrown_size){
	char *excp = new char[thrown_size + sizeof(__cxa_exception)];
	memset(excp, 0, sizeof(__cxa_exception));
	return reinterpret_cast<void*>(reinterpret_cast<cpu_word>(excp) + sizeof(__cxa_exception));
}

extern "C" void __cxa_throw(void *obj, std::type_info *tinfo, void (*dest) (void *)) {
	__cxa_exception *header = reinterpret_cast<__cxa_exception*>(obj) - 1;

	header->exceptionDestructor = dest;
	header->exceptionType = tinfo;
	strncpy(header->unwindHeader.exception_class, "QUARC++\0", 8);

	__cxa_get_globals()->uncaughtExceptions++;

	_Unwind_RaiseException(&header->unwindHeader);

	//critical("unhandled exception thrown");
	asm("cli\nhlt"::"a"(0xdeadbeef));
}

extern "C" void __cxa_begin_catch() {
}

extern "C" void __cxa_end_catch(){}
