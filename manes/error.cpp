/* Quarn OS
 *
 * Error messages implementation
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "libs/colors.h"
#include "arch/low/hlt.h"
#include "error.h"
#include "libs/string.h"
#include "resources/keybscr.h"
#include "resources/x86_keybscr.h"

using namespace manes;

p<resources::stream> error::msg_output = p<resources::stream>::invalid;

error::error(const string &msg) : base_err(0), er_message(msg) {
	if (!msg_output.valid())
		msg_output = new resources::keybscr::arch_keybscr;
}

error::error(p<error> er) : base_err(er) {
	if (!msg_output.valid())
		msg_output = new resources::keybscr::arch_keybscr;
}

error::error(p<error> er, const string &msg) : base_err(er), er_message(msg) {
	if (!msg_output.valid())
		msg_output = new resources::keybscr::arch_keybscr;
}

void error::critical() {
	er_print("Critical Quarn OS error has occurred!\n", color::red);
	for (p<error> er = this; er.valid(); er = er->base_err)
		er_print(er->er_message, color::red);
	er_print("\nSystem is halted.", color::red);

	halt_forever();
}

void error::assertion() {
#if DEBUG_MODE == CONF_YES
	er_print("Assertion failed!\n", color::yellow);
	for (p<error> er = this; er.valid(); er = er->base_err)
		er_print(er->er_message, color::yellow);
	er_print("\nPress any key to continue...", color::yellow);
	wait_key();
#endif
}

#if DEBUG_MODE == CONF_YES
extern unsigned int last_call0;
extern unsigned int last_call1;
#endif

void error::debug() {
#if DEBUG_MODE == CONF_YES
	er_print("(", color::darkgray);
	for (p<error> er = this; er.valid(); er = er->base_err)
		er_print(er->er_message, color::darkgray);
	er_print(((string)" [last call: 0x" /*+ string(last_call0,true) + ", 0x" + string(last_call1,true) +*/ + "]"),color::darkgray);
	er_print(")", color::darkgray);
	wait_key();
#endif
}

void error::er_print(const string &_message, color::color color) {
	if (msg_output.is<color_output>())
		msg_output.cast<color_output>()->set_forecolor(color);
	msg_output->write(_message.to_mem());
	if (msg_output.is<color_output>())
		msg_output.cast<color_output>()->default_color();
}
