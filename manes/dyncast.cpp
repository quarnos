/* Quarn OS
 *
 * dynamic_cast<>
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "manes/error.h"
#include "typeinfo"

typedef signed int ptrdiff_t;
using namespace __cxxabiv1;
typedef unsigned int cpu_word;
extern "C" void *__dynamic_cast(const void *src_ptr,
				const __class_type_info *src_type,
				const __class_type_info *dst_type,
				ptrdiff_t src2dst) {
	/* Get real source type information */
	const int **src = (const int **)src_ptr;
	const std::type_info *ti = (const std::type_info*)((*src)[-1]);

	/* Get base */
	void *pointer = (void*)src_ptr;
	void *old_ptr = (void*)src_ptr;

	if (!ti->__do_upcast(src_type, &old_ptr))
		return (void*)0;

	pointer = (void*)((cpu_word)pointer + (cpu_word)src_ptr - (cpu_word)old_ptr);

	/* Upcast */
	if (ti->__do_upcast(dst_type, &pointer))
		return (void*)pointer;
	else
		return (void*)0;
}


