/* Quarn OS
 *
 * C++ memory allocation functions
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

/* Functions that makes it possible to allocate more memory using operators
 * new and delete. After booting they uses static part of memory, but then
 * they change to the memory allocator.
 */

#include "services/kernel_state.h"
#include "manec.h"
#include "error.h"
#include "manes/cds/component.h"
#include "resources/memm.h"

using namespace manes;
using namespace resources;

static const int _static_mem_amount = 0x50000;
char _static_mem[_static_mem_amount];
int _static_mem_ptr = 0;

unsigned int allocated_memory = 0;

p<memm> allocator = p<memm>::invalid;

void *operator new(unsigned int size) {
	allocated_memory += size;
//	if (!allocator.valid()) {
		int begin = _static_mem_ptr;
		_static_mem_ptr += size;
		if (_static_mem_ptr >= _static_mem_amount)
			critical("mem_alloc: not enough space on static heap to allocate more memory");
		char *buf = (char*)&(_static_mem[begin]);
		for (unsigned int i = 0; i < size; i++)
			buf[i] = 0;

	if ((int)buf < 0x100000 || (int)buf >= 0x200000)
		asm("cli\nhlt"::"a"(buf), "b"(0xbaddcafe));
		return (void*)buf;
//	}
//char *buf = (char*)allocator->allocate_space(size);
	for (unsigned int i = 0; i < size; i++)
		buf[i] = 0;
	if ((int)buf < 0x300000 || (int)buf >= 0x400000)
		critical((string)"mem_alloc: returned value is incorrect: 0x" + string((unsigned int)buf,true) + " (" + string(size) + ")");

	return (void*)buf;
}

void *operator new(unsigned int size, int align) {
	void *ptr = new char[size + align];
	int add = align - (int)ptr % align;
	ptr = (void*)((int)ptr + add);

#if DEBUG_MODE == CONF_YES
	if ((int)ptr & (align - 1))
		__asm__("cli\nhlt"::"a"(ptr));
#endif

	return ptr;
}

void *operator new[](unsigned int size) {
	return operator new(size);
}

void *operator new[](unsigned int size, int align) {
	return operator new(size, align);
}

void operator delete(void *ptr) {
	if ((unsigned int)ptr < (unsigned int)_static_mem + _static_mem_ptr)
		return;
	if (!allocator.valid())
		return;
	allocator->deallocate_space(ptr);
}

void operator delete[](void *ptr) {
	operator delete(ptr);
}
