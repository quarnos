/* Quarn OS
 *
 * Error messages
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _ERROR_H_
#define _ERROR_H_

#include "libs/pointer.h"
#include "libs/string.h"
#include "libs/colors.h"
#include "libs/stream.h"

namespace manes {
	class error {
	private:
		static p<resources::stream> msg_output;

		p<error> base_err;
		const string er_message;

		void er_print(const string&, color::color);
	public:
		error(const string&);
		explicit error(p<error>);
		error(p<error>, const string&);

		void debug();
		void assertion();
		void critical() __attribute__((noreturn));
	};
}

#define on_stack(x) ((unsigned int)(x) < 0x2fffff && (unsigned int)(x) > 0x200000)

static inline void critical(const string&) __attribute__((noreturn));

static inline void critical(const string &info) {
	p<manes::error> er = new manes::error(info);
	er->critical();
	while(1);
}

static inline void debug(const string &info) {
	p<manes::error> er = new manes::error(info);
	er->debug();
}

#define _QUOTE(x) #x
#define QUOTE(x) _QUOTE(x)

#if DEBUG_MODE == CONF_YES
#define assert(msg, cond) \
	if (cond) { \
		p<manes::error> er = new manes::error((string)msg + " @" +  __FILE__ +  ":" +  QUOTE(__LINE__) ); \
		er->assertion(); \
	}
#else
#define assert(msg, cond)
#endif

#endif
