/* Quarn OS
 *
 * Runtime functions for C++
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

/* This is set of functions that are needed to correctly execute code written
 * in the C++ programming language. Main part of this file is runtime_start()
 * function, which calls all constructors of global and static objects.
 */

#include "manes/error.h"

/* Calls constructors of all global and static objects */
void runtime_start() {
	extern int _bss_end;
	extern int _bss;
	
	const unsigned int end = 0x80000;

	unsigned int bss_size = (unsigned int)&_bss_end - (unsigned int)&_bss + 0x10000;
	char *bss = (char*)&_bss;

	char *to = (char*)0x200000;
	char *from = (char*)0x100000;
	for (unsigned int i = 0; i < end; i++)
		to[i] = from[i];

	/* Clear bss section */
	for (unsigned int i = 0; i < bss_size; i++)
		bss[i] = 0;

	/* Threre is a constructors list: __CTOR_LIST__
	 * First entry of the list is the number of constructors.
	 * Other entries are pointers to the constructors.
	 * Everyting is set in the linker script
	 */
	extern void (*__CTOR_LIST__)();
	void (**constr)() = &__CTOR_LIST__;
	int number = *(int*)constr;

	for (int i = 1; i <= number; i++) (constr[i])();
}

/* Code that should be executed when program exit, which won't ever 
 * happen to Manes
 */
void *__dso_handle __attribute__((weak));
extern "C" void __cxa_atexit(void (*)(void *), void *, void *) {
	/* manes::error er("runtime: attempt to terminate kernel process");
	er.critical(); */
}

namespace __cxxabiv1 {
	/* Function shows error message when a call to pure virtual function 
	 * has happened */
	extern "C" void __cxa_pure_virtual(void) {
		manes::error er("runtime: call to pure virtual function");
		er.critical();
	}
}

