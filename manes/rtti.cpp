/* Quarn OS
 *
 * RTTI support
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "typeinfo"

__cxxabiv1::__fundamental_type_info::~__fundamental_type_info() {}

__cxxabiv1::__class_type_info::~__class_type_info() {}

bool __cxxabiv1::__class_type_info::__do_upcast(const __cxxabiv1::__class_type_info *info, void **) const {
	return *this == *info;
}

__cxxabiv1::__si_class_type_info::~__si_class_type_info() {}

bool __cxxabiv1::__si_class_type_info::__do_upcast(const __cxxabiv1::__class_type_info *info, void **obj) const { 
	if (__class_type_info::__do_upcast(info, obj))
		return true;

	return __base_type->__do_upcast(info, obj);
}

__cxxabiv1::__vmi_class_type_info::~__vmi_class_type_info() {}

bool __cxxabiv1::__vmi_class_type_info::__do_upcast(const __cxxabiv1::__class_type_info *info, void **obj) const { 
	if (__class_type_info::__do_upcast(info, obj))
		return true;

	 for (unsigned int i = 0; i < __base_count; i++) {
		if (__base_info[i].__base_type->__do_upcast(info, obj)) {
			*obj = (void*)((unsigned int)*obj + (__base_info[i].__offset_flags >> 8));
			return true;
		}
	}

	return false;
}

__cxxabiv1::__pbase_type_info::~__pbase_type_info() {}

bool __cxxabiv1::__pbase_type_info::__do_upcast(const __cxxabiv1::__class_type_info *dst, void **) const {
	/* ??? */
	return false; //__pointee->__do_upcast(dst, 0);
}

bool __cxxabiv1::__pbase_type_info::__is_pointer_p() const {
	return true;
}


__cxxabiv1::__pointer_type_info::~__pointer_type_info() {}

std::type_info::~type_info() {}

bool std::type_info::__is_pointer_p() const {
	return false;
}

bool std::type_info::__is_function_p() const {
	return false;
}

bool std::type_info::__do_catch(const type_info *, void **, unsigned int) const {
	return false;
}

bool std::type_info::__do_upcast(const __cxxabiv1::__class_type_info *, void **) const {
	return false;
}

extern "C" std::type_info const &__cxa_bad_typeid () {
	return typeid (void);
}
