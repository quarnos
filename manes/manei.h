/* Quarn OS / Manes
 *
 * Managed Execution Initializer
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _MANEI_H_
#define _MANEI_H_

#include "manec.h"
#include "cds/root.h"
#include "cds/factory.h"
#include "services/kernel_state.h"

namespace manes {
	/* Managed Execution Initializer */
	class manei {
	private:
		p<cds::root> main;
		p<cds::factory> types;
		p<services::kernel_state> kstate;
		

		static p<manei> instance;
		manei(){}
	public:
		static p<manei> get();

		void launch_manes();

		void register_creators();
		void register_builtin_types();
		void animate_creators();

		p<cds::root> get_root();
		p<services::kernel_state> state();
	};

}

#endif
