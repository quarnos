/* Quarn OS / Manes
 *
 * Driver class
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */


#ifndef _DRIVER_H_
#define _DRIVER_H_

#include "type.h"

namespace resources {
	class did;
}

namespace manes {
	namespace cds {
		/**
		 * @brief Driver class
		 */
		class driver : public type {
		protected:
			delegate<bool, p<resources::did> > recognize_device;

		public:
			driver(const component_name &name, const component_name &base, delegate<p<component> > create, delegate<bool, p<resources::did> > recognize) :
				type(name, base, create), recognize_device(recognize) {	}

			virtual ~driver() {}

			/**
			 * Check if device is supported by the driver
			 */
			virtual bool check_device(p<resources::did> id) {
				return recognize_device(id);
			}
		};
	}
}

#endif
