/* Quarn OS / Manes
 *
 * Creator class
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _CREATOR_H_
#define _CREATOR_H_

#include "component.h"
#include "component_name.h"

#include "libs/pointer.h"
#include "libs/list.h"

namespace manes {
	namespace cds {
		/**
		 * @brief Components creator.
		 * @details This is an abstract base class for all component creators.
	         * It stores list of created components and provides interface needed
		 * to manage them.
		 */
		class creator : public component {
		protected:
			/**
			 * @brief List of components stored by creator.
			 */
			list<p<component> > components;

		public:
			creator() {}
			creator(p<type> tpe) : component(tpe) {}

			/**
			 * @brief Performs creator initialization.
			 * @details This method is called once creator is construced and
			 * ready to work since Manes does not allow some things to be
			 * performed in constructors.
			 * @return information if initialization was successful
			 */
			virtual bool initialize();

			/**
			 * @brief New type was registered in the system.
			 * @details This method is called each time new type is registered,
			 * what allows creator to perform any actions that such event
			 * may require. It is commonly used by bus classes that support
			 * lazy drivers loading.
			 * @param newtype name of recently registered type
			 * @return ignored value
			 */
			virtual bool type_added(const component_name &newtype);

			/**
			 * @brief Create new component.
			 * @details Create new component of specified type. Instances of
			 * class type provides interface that allow to create an object
			 * of specific type. To get type from type_name manager is used.
			 * Notice that some subclasses may make more restricted or
			 * completely impossible creation of new components.
			 * @param name type of requested component
			 * @return created component
			 */
			virtual p<component> new_component(const component_name &name);

			virtual p<component> add_existing(p<component>);

			/**
		      	 * @brief Returns number of components stored in this creator.
			 * @return components count
			 */
			virtual int count() const;

			/**
		       	 * @todo Implement this method.
			 */
			virtual void delete_component(const component_name&);

			/**
			 * @brief Get an exsiting component.
			 * @details Search in components list for a specifed component.
			 * In most cases this function should not be invoced directly, but
			 * client should use manager's method manager::get_component.
			 * @param name name of the requested component
			 * @return requested component
			 */
			virtual p<component> get_component(const component_name &name);
		};
	}
}

#endif
