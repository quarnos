/* Quarn OS / Manes
 *
 * Component class
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "component.h"
#include "manes/manec.h"
#include "manes/typeinfo"
#include "manes/exception.h"

using namespace manes;
using namespace std;
using namespace manes::ods;
using namespace manes::cds;

component::component() : name(component_name::from_index("none",0)) {}

component::component(p<type> _type) :
	obj_type(_type),
	name(component_name::from_index(_type->get_name().get_name(), 0)) {}

void component::set(p<type> _type) {
	obj_type = _type;
	name = component_name::from_index(_type->get_name().get_name(), 0);
}

const component_name &component::get_name() const {
	return name;
}

p<type> component::get_type() const {
	return obj_type;
}

