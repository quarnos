/* Quarn OS / Manes / CDS
 *
 * Component class
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _COMPONENT_H_
#define _COMPONENT_H_

#include "manes/ods/obj_ref.h"
#include "component_name.h"

#include "libs/pointer.h"

namespace manes {
	/**
	 * @brief Component Distribution System
	 */
	namespace cds {
		class type;

		/**
		 * @brief Component
		 *
		 * @details Ths is a representation of component in CDS
		 */
		class component : public ods::obj_ref {
		protected:
			p<type> obj_type;

			component_name name;

		public:
			component();
			component(p<type>);
			void set(p<type>);

			const component_name &get_name() const;
			p<type> get_type() const;
		};
	}
}

#endif
