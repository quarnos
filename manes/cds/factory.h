/* Quarn OS / Manes
 *
 * Factory
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _FACTORY_H_
#define _FACTORY_H_

#include "type.h"
#include "creator.h"

namespace manes {
	namespace cds {
		/**
		 * @brief Types manager
		 * @details This is main type manager in Manes. At leas one insance
		 * of this class exists and is created by manei. In most cases
		 * construction of such class is performed manually, not using
		 * automatic Manes mechanisms.
		 * @see manei
		 */
		class factory : public creator {
		public:
			factory(){}
			factory(p<type> tpe) : creator(tpe) {}

			/**
			 * @brief Not supported
			 */
			virtual p<component> new_component(const component_name &name);

			/**
			 * @brief Get the specified type
			 *
			 * @details This function overrides base behavior introducing mechanisms
			 * necessary to correctly store and retrieve types from database.
			 */
			virtual p<component> get_component(const component_name &name);
	
			/**
			 * @brief Register type in system.
			 * @details This is a decorator that allows client to get rid
			 * off unecessary code required when any other class instances are
			 * created. In this case there is only need to provide factory
			 * with an existing instance of type class.
			 * @param newtype type to be registered
			 */
			virtual p<component> add_existing(p<component>);
		};
	}
}

#endif
