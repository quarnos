/* Quarn OS / Manes
 *
 * Root class
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _ROOT_H_
#define _ROOT_H_

#include "creator.h"
#include "type.h"
#include "libs/pointer.h"

namespace manes {
	namespace cds {
		/**
		 * @brief Main component creator.
		 *
		 * @details In most cases there is only one instance of this class. It stores
		 * pointers to all child-creators.
		 */
		class root : public creator {
		public:
			virtual bool initialize(p<component>);
			virtual bool type_added(const component_name &ntype);

			/**
			 * @brief Get specific component.
			 *
			 * @details Search in compontens list for the specified one. If none is
			 * found then ask for it each known that is able to maintain such component.
			 * @param name The name of requested component.
			 * @return Requested component.
			 */
			virtual p<component> get_component(const component_name &name);

			virtual p<component> add_existing(p<component>);
		};
	}
}

#endif
