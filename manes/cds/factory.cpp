/* Quarn OS / Manes / CDS
 *
 * Factory
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "factory.h"

using namespace manes::cds;

p<component> factory::new_component(const component_name &obj_type) {
	throw new not_supported_exception;
}


p<component> factory::get_component(const component_name &obj) {
	if (obj.get_type().get_name() == "type") {
		/* Client looks for type with a certain name */
		for (int i = 0; i < components.get_count(); i++)
			if (components[i]->get_name() == obj)
				return components[i];

	} else	if (obj.get_type().get_name() == "driver") {
		/* Clients looks for appropiate driver and uses index to get type */
		return components[obj.get_index()];
	}

	return p<component>::invalid;
}


p<component> factory::add_existing(p<component> tpe) {
	if (tpe.is<type>())
		components.add(tpe);
	else
		throw new argument_exception;

	return tpe;
}

