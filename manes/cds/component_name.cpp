/* Quarn OS / Manes
 *
 * Component name class implementation
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "component_name.h"

using namespace manes::cds;

component_name::component_name(const component_name &x) :
	type_index(x.type_index),
	text_name(x.text_name) { 
	if (x.type_name)
		type_name = new component_name(*x.type_name);
}

const component_name &component_name::get_type() const {
	return *type_name;
}

unsigned int component_name::get_index() const {
	return type_index;
}

string component_name::get_name() const {
	return text_name;
}

bool component_name::operator==(const component_name &x) const {
	if (!x.type_name && type_name)
		return false;
	if (x.type_name && !type_name)
		return false;

	if (!x.type_name && !type_name && x.text_name == text_name)
		return true;
	if (*x.type_name == *type_name && x.text_name == text_name)
		return true;
	return false;
}

component_name component_name::from_path(const string &tpath) {
	char *path  = new char[strlen(tpath) + 1];
	strcpy(path, tpath);

	int i = 1, j = 1;

	char *dep_res = (char*)0;
	char *main_res = (char*)0;
	char *specific_name = (char*)0;
	int num = 0;

	main_res = &path[1];
	while(1) {
		if (!path[i]) {
			main_res = &(path[j]);
			break;
		}
		if (path[i] == ',') {
			path[i] = 0;
			i++;
			if (string::is_digit(path[i]))
				for (; path[i] && path[i] != '/'; i++)
					num = num * 10 + path[i] - '0';
			else
				specific_name = &path[i];
		} else if (path[i] == '/') {
			path[i] = 0;
			dep_res = &(path[j]);
			i++;
			j = i;
		}
		for (; path[i] && path[i] != ',' && path[i] != '/'; i++);
		if (!path[i])
			break;
	}

        component_name cname;

	cname.type_name = new component_name;
	cname.type_name->text_name = main_res;
	if (strcmp(main_res, "type")) {
		cname.type_name->type_name = new component_name;
		cname.type_name->type_name->text_name = "type";
	}
	if (specific_name)
		cname.text_name = (string)specific_name;
	else
		cname.text_name = "";
	cname.type_index = num;

	return cname;
}

component_name component_name::from_index(const string &tname, unsigned int i) {
	component_name cname;

	cname.type_name = new component_name;
	cname.type_name->text_name = tname;
	cname.type_name->type_name = new component_name;
	cname.type_name->type_name->text_name = "type";

	cname.type_index = i;

	return cname;
}

component_name::component_name() : text_name("") {}

void component_name::serialize(ods::object_stream &ostr) {

}

void component_name::deserialize(ods::object_stream &ostr) {

}

const component_name component_name::invalid;
