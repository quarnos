/* Quarn OS / Manes
 *
 * Type class
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _TYPE_H_
#define _TYPE_H_

#include "libs/pointer.h"
#include "libs/delegate.h"

#include "component.h"

namespace manes {
	namespace cds {

		/**
		 * @brief Type class
		 * @details This class describes type of components. It provides
		 * inheritance information as well as possibility to create a new
		 * instance. As long as types are just special kind of components
		 * client code can inherit from them and introduce new functionality.
		 * @see driver
		 */
		class type : public component {
		protected:
			const component_name base;

			const delegate<p<component> > create_impl;

		public:

			type(const component_name&, const component_name&, delegate<p<component> >);

			component_name get_base() const;

			/**
			 * @brief Tell if type1 is-a type2.
			 * @details This function checks if this type is or inherits
			 * from type given in argument.
			 * @param x type to compare with
			 * @return true if cast would be successful
			 */
			bool is(const component_name &x) const;

			/**
			 * @brief Create new instance of the type.
			 * @details This method creates new instance of the type described
			 * by this class. It mainly uses create_impl delegate.
			 * @param parent component that wants to create new object
			 * @return pointer to newly created instance
			 */
			p<component> create_component();

			bool operator==(const type&) const;
			bool operator!=(const type&) const;
		};
	}
}

#endif
