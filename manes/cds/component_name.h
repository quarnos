/* Quarn OS / Manes / CDS
 *
 * Component name class
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _COMPONENT_NAME_H_
#define _COMPONENT_NAME_H_

#include "manes/ods/obj_val.h"

#include "libs/string.h"

namespace manes {
	namespace cds {
		/**
		 * @brief Component name
		 *
		 * @details Instances of this class are used to explicitly identify
		 * the component in the CDS network.
		 */
		class component_name : public ods::obj_val {
		private:
			component_name *type_name;
			unsigned int type_index;

			string text_name;

			component_name();
		public:
			component_name(const component_name &);

			virtual ~component_name() {}

			/**
			 * Get component_name of component's type
			 */
			const component_name &get_type() const;

			/**
			 * Get component's text name
			 */
			string get_name() const;

			/**
			 * Get component's index
			 */
			unsigned int get_index() const;

			bool operator==(const component_name&) const;

			/**
			 * Create component_name from path
			 *
			 * @code /type,name/type,index @endcode
			 */
			static component_name from_path(const string &);

			/**
			 * Create component_name from index
			 */
			static component_name from_index(const string &, unsigned int);

			static const component_name invalid;

			virtual void serialize(ods::object_stream &ostr);
			virtual void deserialize(ods::object_stream &ostr);
		};
	}
}

#endif
