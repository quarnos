/* Quarn OS / Manes / CDS
 *
 * Abstract type class
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _ABSTRACT_H_
#define _ABSTRACT_H_

#include "type.h"

namespace manes {
	namespace cds {
		/**
		 * @brief Abstract type class
		 */
		class abstract : public type {
		private:
			delegate<p<component> > pure_virtual;

		public:
			abstract(const component_name &name, const component_name &base) :	type(name, base, pure_virtual) { }

			virtual ~abstract() { }
		};
	}
}

#endif
