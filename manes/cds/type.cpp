/* Quarn OS / Manes
 *
 * Type class
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "component.h"
#include "type.h"
#include "manes/error.h"
#include "factory.h"
#include "abstract.h"

#include "libs/string.h"

#include "manes/manec.h"

using namespace manes;
using namespace manes::cds;

type::type(const component_name &_name, const component_name &_base, const delegate<p<component> > creat) :
	base(_base),
	create_impl(creat) {
	this->name = _name;
}

component_name type::get_base() const {
	return base;
}

bool type::is(const component_name &x) const {
	if (x == name)
		return true;

	p<type> tpe = manec::get()->get_factory()->get_component(component_name::from_path((string)"/type," + base.get_name())).cast<type>();
	if (!tpe.cast<abstract>().valid())
		return false;

	if (!tpe.valid())
		return false;

	return tpe->is(x);
}

p<component> type::create_component() {
	assert((string)"type: attempt to create implementation of abstract type: " + name.get_name(), create_impl.null());

	p<component> comp = create_impl();
	comp->set(this);

	return comp;
}

bool type::operator==(const type &x) const {
	return x.name == name;
}

bool type::operator!=(const type &x) const {
	return !operator==(x);
}
