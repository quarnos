/* Quarn OS / Manes
 *
 * Root implementation
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "root.h"
#include "services/server.h"
#include "modules/loader.h"
#include "factory.h"

using namespace manes::cds;

bool root::initialize(p<component> comp) {
	components.add(comp);

	return true;
}

bool root::type_added(const component_name &ntype) {
	p<creator> creat;
	for (int i = 0; i < components.get_count(); i++)
		if ((creat = components[i].cast<creator>()).valid())
			creat->type_added(ntype);

	return true;
}

p<component> root::get_component(const component_name &obj) {
	p<component> comp = creator::get_component(obj);

	if (comp.valid())
		return comp;

	for (int i = 0; i < components.get_count(); i++) {
		if (components[i].is<creator>()) {
			if ((comp = components[i].cast<creator>()->get_component(obj)).valid()) {
				return comp;
			}
		}
	}

	return p<component>::invalid;
//	throw new component_not_found_exception((const char*)((string)"Attempt to get not existing compontnt" + obj.get_name() + ", " + obj.get_type().get_name()));
}

p<component> root::add_existing(p<component> comp) {
	return comp;
}
