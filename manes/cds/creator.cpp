/* Quarn OS / Manes
 *
 * Creator class
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "creator.h"
#include "manes/manec.h"

using namespace manes;
using namespace manes::cds;

bool creator::initialize() {
	return true;
}

bool creator::type_added(const component_name&) {
	return true;
}

int creator::count() const {
	return components.get_count();
}

p<component> creator::new_component(const component_name &obj_type) {
	p<component> comp = manec::get()->get_type(obj_type)->create_component();
	components.add(comp);
	return comp;
}

void creator::delete_component(const component_name &obj) {
	for (int i = 0; i < components.get_count(); i++)
		if (components[i]->get_name() == obj)
			return components.remove(i);
}

p<component> creator::get_component(const component_name &obj) {
	for (int i = 0, j = 0; i < components.get_count(); i++) {
		if (components[i]->get_name() == obj) {
			if ((unsigned int)j == obj.get_index()) {
				return components[i];
			} else {
			     	j++;
			}
		}
	}

	return p<component>::invalid;
}

p<component> creator::add_existing(p<component> comp) {
	return comp;
}
