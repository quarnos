# Quarn OS
#
# Main Makefile
#
# Copyright (C) 2008-2009 Pawel Dziepak
#

#
# make help describes all possible targets
#

MAKEFLAGS += --no-print-directory

MAJREL = 0
MINREL = 0
PROREL = 95
ADDONS = -dev

$(info Quarn OS $(MAJREL).$(MINREL).$(PROREL)$(ADDONS))
$(info Copyright (C) 2008 Pawel Dziepak)
$(info  )

# Check host OS
$(info Checking host environment...)
$(if $(shell which gcc), , $(error GCC is needed to compile Quarn))
$(if $(shell which g++), , $(error G++ is needed to compile Quarn))
$(if $(shell which ld), , $(error LD is needed to compile Quarn))
$(if $(shell which sed), , $(error SED is needed to compile Quarn))
$(if $(shell which pwd), , $(error PWD is needed to compile Quarn))
$(if $(shell which perl), , $(error PERL is needed to compile Quarn))

PERL = perl

LATEX=latex
DVIPDF=dvipdf

# Main tools
CC = gcc
CPP = g++
LD = ld

DIR = $(shell pwd)
DIR_SHORT = $(shell pwd | sed 's/\/.*\///')

CONFFILE = quarnconf.h
CONF = -include $(CONFFILE)

ifeq ($(shell $(PERL) scripts/mk_conf.pl OPT_SIZE), 0)
OPTIMIZE = -O2
POSTLD =
else
OPTIMIZE = -Os
POSTLD = #strip quarn
endif

# Main flags
CFLAGS = -Wall -pedantic -std=c99 $(OPTIMIZE) -fno-builtin -nostartfiles -nostdlib -I$(DIR) $(CONF)
CPPFLAGS = -Wall -pedantic $(OPTIMIZE) -fno-builtin -nostartfiles -nostdlib -I$(DIR) -isystem$(DIR) $(CONF)
LDFLAGS = 

ARCH=x86

ifeq ($(shell $(PERL) scripts/mk_conf.pl USE_MULTIBOOT), 0)
MB =
else
MB = arch/$(ARCH)/loaders/systemload.o
endif

ifeq ($(shell $(PERL) scripts/mk_conf.pl DEBUG_MODE), 0)
MAP =
QFLAGS =
POSTQEMU =
else
$(if $(shell which sed), , $(error GDB is needed to debug Quarn))
MAP = -Map quarn.map
CFLAGS += #-g
CPPFLAGS += #-g
QFLAGS = #-s -S &
POSTQEMU = gdb quarn
endif

# Directories that consist of code has to be compiled
DIRS = actors/ arch/low/ libs/ arch/low/loaders manes/ modules/ services/ resources/ hydra/

SUDO = sudo

LOOPDEV = $(shell $(SUDO) /sbin/losetup -f)

# Compile whole kernel
compile:
		$(info Compiling Quarn OS...)
		$(shell rm -f arch/low)
		$(shell ln -s $(DIR)/arch/$(ARCH)/ arch/low)
		@$(if $(wildcard $(CONFFILE)), , $(error You have to configure Quarn OS first.))
		@for dir in $(DIRS); do \
			(cd $$dir; $(MAKE) CC=$(CC) CPP=$(CPP) LD=$(LD) \
			 CFLAGS="$(CFLAGS)" CPPFLAGS="$(CPPFLAGS)" \
			 LDFLAGS=$(LDFAGS)); \
		done
		@echo -e "\t\t[$(LD)]\tquarn"
		@$(LD) -Tscripts/x86_kernel.ld $(MB) `find . -name *.o | grep -v -E 'loaders|modules_start|rs232|test|hydra'` -o quarn $(MAP)
		@$(POSTLD)

# Clean all and then compile
rebuild: clean compile

# Clean object files
clean:
		$(shell rm -f arch/low)
		$(shell ln -s $(DIR)/arch/$(ARCH)/ arch/low)
		$(info Cleaning project directory...)
		@for dir in $(DIRS); do \
			(cd $$dir; $(MAKE) clean); \
		done
		@(cd docs; $(MAKE) clean)
		@(cd tests; $(MAKE) clean)
		@rm -rf Documentation
		@echo -e "\t\t[rm]\tquarn"
		@rm -f quarn
		@rm -f quarn.map

# Clean also built tools
allclean: clean
		$(info Cleaning configuration files...)
		@echo -e "\t\t[rm]\tscripts/nc_config"
		@rm -f scripts/nc_config
		@echo -e "\t\t[rm]\tquarnconf.h"
		@rm -f quarnconf.h
		@echo -e "\t\t[rm]\tfd.img"
		@rm -f fd.img
		@rm -f arch/low

# Compile configuration program (ncurses) and run it
config: scripts/nc_config
		@echo -e "\t\t[sh]\tscripts/nc_config"
		@scripts/nc_config

scripts/nc_config: scripts/nc_config.c
		@echo -e "\t\t[$(CC)]\tscripts/nc_config.c"
		@$(CC) scripts/nc_config.c -lmenu -lncurses -I/usr/include/ncurses -o scripts/nc_config

defconf: clean
		$(info Restoring default configuration...)
		@echo -e "\t\t[$(PERL)]\tscripts/text_config.pl"
		@$(PERL) scripts/text_config.pl def

# Run configuration script (doesn't need ncurses)
textconf: clean
		$(info Launching configuration tool...)
		@echo -e "\t\t[$(PERL)]\tscripts/text_config.pl"
		@$(PERL) scripts/text_config.pl $(MAJREL).$(MINREL).$(PROREL)$(ADDONS)

# Create bootable floppy image
fd_img: compile
		$(info Creating floppy image...)
		@rm -f fd.img
		@/sbin/mkdosfs -F 12 -C fd.img 1440
		@$(SUDO) /sbin/losetup $(LOOPDEV) fd.img
		@$(SUDO) dd if=arch/low/loaders/boot of=$(LOOPDEV)
		@mkdir image
		@$(SUDO) mount $(LOOPDEV) image
		@-$(SUDO) cp arch/low/loaders/loader image/
		@$(SUDO) cp quarn image/
		@$(SUDO) cp AUTHORS image/
		@-$(SUDO) cp modules/*.ko image/
		@-$(SUDO) cp hydra/*.ko image/
		@$(SUDO) umount $(LOOPDEV)
		@rmdir image
		@$(SUDO) /sbin/losetup -d $(LOOPDEV)

qemu: fd_img
		@qemu -fda fd.img -usb -net nic,model=ne2k_pci -net user

debug: fd_img
		@qemu -fda fd.img -usb -net nic,model=ne2k_pci -net user $(QFLAGS)
		@$(POSTQEMU)

# Put bootable image on the floppy
floppy: fd_img
		$(info Putting image on a floppy...)
		@$(SUDO) dd if=fd.img of=/dev/fd0

# Create build package
build: fd_img doc
		$(if $(shell which tar), , $(error TAR is needed to compile Quarn))
		$(if $(shell which gzip), , $(error GZIP is needed to compile Quarn))
		$(info Creating build package...)
		@-rm -f ../quarnos_`date +%Y-%m-%d`.tar
		@tar -cf ../quarnos_`date +%Y-%m-%d`.tar ../$(DIR_SHORT)
		@tar --delete -f ../quarnos_`date +%Y-%m-%d`.tar $(DIR_SHORT)/.git
		@gzip -f ../quarnos_`date +%Y-%m-%d`.tar

pack: allclean
		$(if $(shell which tar), , $(error TAR is needed to compile Quarn))
		$(if $(shell which gzip), , $(error GZIP is needed to compile Quarn))
		$(info Creating release package...)
		@-rm -f ../quarnos_$(MAJREL).$(MINREL).$(PROREL)$(ADDONS).tar
		@tar -cf ../quarnos_$(MAJREL).$(MINREL).$(PROREL)$(ADDONS).tar ../$(DIR_SHORT)
		@tar --delete -f ../quarnos_$(MAJREL).$(MINREL).$(PROREL)$(ADDONS).tar $(DIR_SHORT)/.git
		@gzip -f ../quarnos_$(MAJREL).$(MINREL).$(PROREL)$(ADDONS).tar

# Run doxygen
doxygen:
		$(if $(shell which doxygen), , $(error doxygen is needed to compile Quarn))
		@echo -e "\t\t[doxygen]\tquarnos"
		@doxygen scripts/Doxyfile > /dev/null
		@cp scripts/tabs.css Documentation/html/

# Create documentation files
doc: doxygen
		$(if $(shell which $(LATEX)), , $(error $(LATEX) is needed to compile Quarn))
		$(if $(shell which $(DVIPDF)), , $(error $(DVIPDF) is needed to compile Quarn))
		@(cd docs; $(MAKE) LATEX=$(LATEX) DVIPDF=$(DVIPDF))

# Run tests
test:
		@#cd libs/tests; $(MAKE)
		@cd tests; $(MAKE)

# Show possible targets
help:
		@echo -e "\t\tQuarn OS $(MAJREL).$(MINREL).$(PROREL) Makefile"
		@echo -e "\t\t------------------------"
		@echo -e "\nCopyright (C) 2008-2009 Pawel Dziepak"
		@echo -e "\nConfiguration:"
		@echo -e "\tconfig\t\t- ncurses configuration tool (WIP)"
		@echo -e "\tdefconf\t\t- uses default configuration"
		@echo -e "\ttextconf\t- text configuration tool"
		@echo -e "\nCompiling:"
		@echo -e "\tcompile\t\t- compile whole kernel (default)"
		@echo -e "\trebuild\t\t- allclean + compile"
		@echo -e "\tdoc\t\t- create all documentation files"
		@echo -e "\ttest\t\t- run all tests"
		@echo -e "\nInstallation:"
		@echo -e "\tfd_img\t\t- create floppy image (requires root)"
		@echo -e "\tfloppy\t\t- fd_img + put image on the floppy (requires root)"
		@echo -e "\nPackaging:"
		@echo -e "\tbuild\t\t- create build package in the upper directory"
		@echo -e "\tpack\t\t- create release package in upper directory"
		@echo -e "\nCleaning:"
		@echo -e "\tallclean\t- clean + remove configuration files"
		@echo -e "\tclean\t\t- remove all files generated by target compile"
		@echo ""
