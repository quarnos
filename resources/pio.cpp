/* Quarn OS
 *
 * Programmable Input/Output
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "pio.h"
#include "services/timer.h"
#include "manes/manec.h"

using namespace services;
using namespace manes;
using namespace resources;

void pio::send_packet(const buffer &data) {
	buf = data;
}

void pio::set_speed(int v) {
	assert("pio: too high transmission speed", v > 1000);

	if (v != 0)
		speed = 1000 / v;
	else
		speed = 0;
}

void pio::set_dep(p<stream> _dep) {
	dep = _dep;
}

void pio::send_data_next() {
	if (ptr >= buf.get_size()) {
		ready = true;
		if (!notice_user.null())
			notice_user();
		return;
	}
	dep->write(buffer::to_mem(buf[ptr++]));

	tm->set_timer(speed, snd_dt_nxt);
}

bool pio::start(bool wait, delegate<void> notice) {
	tm = manec::get()->get<timer>("/timer");

	ready = false;
	if (speed != 0) {
		ptr = 0;
		snd_dt_nxt = delegate<void>::method(this,&pio::send_data_next);
		send_data_next();
	} else {
		for (int i = 0; i < buf.get_size(); i++)
			dep->write(buffer::to_mem(buf[i]));
		if (!notice.null())
			notice();
		return true;
	}
	if (!wait) return true;

	while (!ready) __asm__("hlt");
	return true;
}

void pio::register_type() {
	manes::manec::get()->register_type<pio>("pio", "virtual_bus");
}

