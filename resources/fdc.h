/* Quarn OS
 *
 * FDC
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "resources/block.h"
#include "resources/device.h"
#include "isa.h"

namespace resources {
	class fdc : public block {
	private:
		int sector_size;
		int sectors_per_track;

		void fdc_init();
		void fdc_read_sector(void *data, int head, int cyl, int start, int stop);
		void fdc_write_sector(void *source, int head, int cyl, int start, int stop);

		/* Specific fdc orders */
		void fdc_seek(int head, int cylinder);
		void fdc_sense_int_status();
		void fdc_recalibrate();
		void fdc_read_data(int head, int cyl, int start, int stop);
		void fdc_write_data(int head, int cyl, int start, int stop);
		void fdc_set_disk_type();
		void fdc_wait_command();
		void fdc_wait_data();
		void fdc_turn_off();
		void fdc_reset();
		void fdc_wait_int();

		void fdc_isr_normal();
		void fdc_isr_dma();

		isa_did isaaddr;

		volatile bool irq_occured;
	public:
		fdc();

		void read_block(int address, int count, void *buffer);
		void write_block(int address, int count, void *buffer);

		int get_granulity();

		bool init_device(p<did> iadr);

		static void register_type();
	};
}
