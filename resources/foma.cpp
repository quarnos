/* Quarn OS
 *
 * Fixed-size Objects Memory Allocator
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

/* Propably this implementation is LE only */

#include "foma.h"
#include "memm.h"

#include "arch/low/fast_util.h"
#include "services/kernel_state.h"
#include "manes/manec.h"
#include "physmem.h"

using namespace arch;
using namespace manes;
using namespace resources;

bool foma::initialize() {
}

foma::foma() {
	physical = manec::get()->get<physmem>("/physmem");
	memory_size = manec::get()->state()->get_memory_size();

	start_heap = (void*)0x300000;
	for (int i = 0; i < 20; i++) 
		first_block[i] = 0;
}

void *foma::get_page(int size) {
	void *addr = physical->allocate_space(0x1000);

	*(unsigned int*)addr = 1 << size;

	return addr;
}

int alloc_memory = 0;

void *foma::allocate(unsigned int fixed_size, unsigned int size) {
	unsigned int index = log2up(fixed_size);

	assert("foma: attempt to allocate too big object", index > PAGE_SHIFT);

	if (first_block[index] == 0) {
		first_block[index] = (unsigned int*)((unsigned int)get_page(index) + (1 << index));

		unsigned int i = 0;
		/* O(1) !!! */
		for (; i < PAGE_SIZE - fixed_size; i+= fixed_size) {
			*(unsigned int*)((unsigned int)first_block[index] + i) = (unsigned int)first_block[index] + i + fixed_size;
		}
		*(unsigned int*)((unsigned int)first_block[index] + i - fixed_size ) = 0;

		alloc_memory += fixed_size;
	}

	/* Allocate object */
	unsigned int *next_block = (unsigned int*)*first_block[index];
	unsigned int *found_block = first_block[index];

	first_block[index] = next_block;

	return (void*)found_block;
}

void foma::deallocate(void *ptr, unsigned int size) {
	unsigned int index = log2up(size);
	unsigned int fixed_size = 1 << index;

	if (fixed_size < 4)
		return;

	assert("foma: attempt to deallocate too big object", index > PAGE_SHIFT);
	if (index > PAGE_SHIFT)
		return;

	unsigned int next_block = (unsigned int)first_block[index];
	unsigned int *new_first_block = (unsigned int*)ptr;

	first_block[index] = new_first_block;
	*new_first_block = next_block;
}

//#if DEBUG_MODE == CONF_YES
int resources::foma::used_memory = 0;
//#endif

void *foma::allocate_space(unsigned int size) {
	if (size == 0)
		return (void*)0;

	if (size < PAGE_SIZE) {
		unsigned int fixed_size = 1 << log2up(size < sizeof(void*) ? sizeof(void*) : size);
#if DEBUG_MODE == CONF_YES
	used_memory += size;
	alloc_memory += fixed_size;
#endif
		return allocate(fixed_size < sizeof(void*) ? sizeof(void*) : fixed_size, size);
	} else {
		void *first_page = get_page(PAGE_SHIFT + 1);
		for (int i = size - PAGE_SIZE; i > 0; i -= PAGE_SIZE)
			get_page(PAGE_SHIFT + 1);

		return first_page;
	}
}

void foma::deallocate_space(void *ptr) {
	assert("foma: attempt to free null pointer", ptr == 0);
	if ((unsigned int)ptr < 0x300000 || (unsigned int)ptr >= 0x400000) {
		assert((string)"foma: attempt to free object not created by foma: 0x"  + string((int)ptr,true),1);
		return;
	}

	unsigned int size = get_size(ptr);

	if (size < PAGE_SIZE) {
		deallocate(ptr, size);
	} else {
		debug("foma: deallocating object larger than page");
	}
}

unsigned int foma::get_size(void *ptr) const {
	return *(unsigned int*)((unsigned int)ptr & ~(PAGE_SIZE - 1));
}

void foma::register_type() {
	manec::get()->register_type<foma>("foma", "allocator");
}

