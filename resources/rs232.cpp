/* Quarn OS
 *
 * RS232
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "rs232.h"
#include "arch/low/rs232.h"
#include "libs/delegate.h"
#include "isa.h"
#include "manes/manec.h"

using namespace resources;

void rs232::read(buffer &data) {

}

void rs232::write_dwords(const int *data, int count) {

}

void rs232::write(const buffer &data) {
	//access_write();
	for (int i = 0; i < data.get_size(); i++) {
		if (data[i] == '\n')
			arch::rs232_send(isaaddr, '\r');
		arch::rs232_send(isaaddr, data[i]);
	}
	//release();
}

void rs232::received(const char a) {
	write(buffer::to_mem(a));
}

void rs232::set_ondatareceived(delegate<void>){}
/*
resource_result *rs232_impl::configure(rs232_mode mode, int speed, int bits,
			   parity_check parity, int stop) {
	arch::rs232_configure(isaaddr, mode, speed, bits, parity, stop);
}*/

bool rs232::init_device(p<did> iadr) {
	isaaddr = *iadr.cast<isa_did>();

	arch::rs232_init(isaaddr, delegate<void, const char>::method(this, &rs232::received));

	inited = true;

	return inited;
}

bool rs232::check_device(p<did> id) {
	p<isa_did> iid = id.cast<isa_did>();
	if (!iid.valid())
		return false;

	if (iid->irq == 0x23 || iid->irq == 0x24)
		return true;

	return false;
}

void rs232::register_type() {
	manes::manec::get()->register_driver<rs232>("rs232", "console", delegate<bool, p<did> >::function(rs232::check_device));
}

extern "C" void module_init() {
	resources::rs232::register_type();
}
