/* Quarn OS
 *
 * UHCI driver
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _UHCI_H_
#define _UHCI_H_

#include "device.h"
#include "arch/low/general.h"
#include "pci.h"
#include "usb.h"
#include "usb_hc.h"

namespace resources {
	class uhci : public usb_hc {
	public:
		enum {
			hc_stop = 0,
			hc_run = 1,
			hc_reset = 2,
			hc_greset = 4,
			hc_egsm = 8,
			hc_fgr = 0x10,
			hc_swdgb = 0x20,
			hc_cf = 0x40,
			hc_maxp = 0x80
		};

		enum uhci_registers {
			uhci_command,
			uhci_status = 2,
			uhci_flbaseadd = 8,
			uhci_portsc1 = 0x10
		};

		/* TD, QH, frame */

		struct transfer_descriptor {
			u32 link_ptr;
			volatile u32 control;
			u32 token;
			u32 buffer;
		};

		struct queue_head {
			u32 head_ptr;
			volatile u32 element_ptr;
		};

		typedef u32 frame_pointer;

		void irq();

		volatile transfer_descriptor *td;
		frame_pointer *fp;
		int last_fp;
		queue_head *qh;

	protected:

		pci_did pciid;
	public:
		void init_structures();
		int get_free_address();
		void *control_transfer(int pid, void *val, int length, int rlength, u8 address);
		void bulk_transfer(int, void *, int, int, int);

		void init();
		void device_connected(int port);

		uhci() : td(0), fp(0), last_fp(0) {}		

		//virtual void init_port(int i) = 0;
		
		//virtual array<char> control_transfer(usb_pid, array<char>, int, int) = 0;

		bool init_device(p<did>);

		void scan_bus(delegate<void, int>);

		static bool check_device(p<did>);
		static void register_type();
	};
}

#endif
