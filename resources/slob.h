#ifndef _SLOB_H_
#define _SLOB_H_

#include "memm.h"
#include "physmem.h"
#include "arch/low/general.h"

namespace resources {
	class slob : public memm {
	private:
		p<physmem> physical;

		struct unit {
			unsigned int size;
			unit *next;
		};

		unit *first_block;

		bool merge(unit *ptr);
	public:
		slob();

		virtual void *allocate_space(unsigned int size);
		virtual void deallocate_space(void *ptr);

		virtual unsigned int get_size(void *ptr) const;

		static void register_type();
	};
}

#endif
