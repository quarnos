/* Quarn OS
 *
 * Memory Manager
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _MEMM_H_
#define _MEMM_H_

#include "manes/cds/creator.h"

namespace resources {
	class memm : public manes::cds::component {
	public:
		virtual void *allocate_space(unsigned int size) = 0;
		virtual void deallocate_space(void *ptr) = 0;

		virtual unsigned int get_size(void *ptr) const = 0;
	};
}

#endif
