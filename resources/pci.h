/* Quarn OS
 *
 * PCI bus
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _PCI_H_
#define _PCI_H_

#include "bus.h"
#include "did.h"

namespace resources {

	class pci_did : public io_did {
	public:
		int irq;
		int dma_channel;
		void *memory_base;
		int ibus, device, function;

		pci_did(p<bus> b) : io_did(b) {}
		pci_did() {}
		virtual ~pci_did() {}

		inline virtual void accepted();

		inline unsigned int get_devclass() const;
		inline unsigned int get_devid() const;
	};

	class pci : public bus {
	private:
		int dev_count;

		static const int max_pci_buses;
		static const int max_pci_devs;
		static const int max_pci_funcs;

		static int start_io;

		static const int free_irqs[];
		static int last_irq;

	protected:
		unsigned int get_conf(int bus, int dev, int func, int dword) const;
		void set_conf(int bus, int dev, int func, int dword, int value);
		bool dev_exist(int bus, int dev, int func);
		int allocate_irq();
		int allocate_io(int bar);

		void device_found(int, int, int);
		int scan_bus();

	public:
		void alloc_resources(p<pci_did> id);

		bool initialize();
		bool type_added(manes::cds::component_name);

		int get_count();

		unsigned int get_devclass(p<did>) const;
		unsigned int get_devid(p<did>) const;

		static void register_type();
	};

	unsigned int pci_did::get_devclass() const {
		return get_bus().cast<pci>()->get_devclass((did*)this);
	}

	unsigned int pci_did::get_devid() const {
		return get_bus().cast<pci>()->get_devid((did*)this);
	}

	void pci_did::accepted() {
		get_bus().cast<pci>()->alloc_resources(this);
	}
}

#endif
