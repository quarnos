/* Quarn OS
 *
 * Bus
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _BUS_H_
#define _BUS_H_

#include "manes/cds/creator.h"
#include "manes/cds/component_name.h"

namespace resources {
	class did;
	class device;
	class bus : public manes::cds::creator {
	protected:
		void create_device(p<did> id);

	public:
		p<manes::cds::component> get_component(const manes::cds::component_name &obj);
		//virtual bool initialize() = 0;

		virtual bool type_added(const manes::cds::component_name&);

		virtual int get_count() = 0;

		virtual bool init_device(p<did> id) {
			return false;
		}

		virtual bool init_bus(p<device> dev) {
			return false;
		}
	};
}

#endif
