/* Quarn OS
 *
 * File system
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "fs.h"
#include "file.h"

using namespace manes;
using namespace resources;

/* Only for testing */
p<manes::cds::component> fs::get_component(const manes::cds::component_name &fname) {
	if (fname.get_type().get_name() != string("file"))
		return p<manes::cds::component>::invalid;

	/* TODO: check if file's object already exist before create new one */
	p<manes::cds::component> file_comp = new_component(manes::cds::component_name::from_path("/type,file"));
	p<file> fp = file_comp.cast<file>();

	/* Till constructor's arguments are supported by manes */
	fp->set(fname.get_name(), this);

	/* In the near future component implementation will inherit from component
	 * class. That will be better solution that current aggregations.
	 */
	return file_comp;
}

bool fs::initialize() {
	return true;
}
