/* Quarn OS
 *
 * Speaker
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "speaker.h"
#include "arch/low/speaker.h"
#include "libs/delegate.h"
#include "manes/manec.h"

using namespace manes;
using namespace resources;

void speaker::read(buffer &data) {
	return;
}

void speaker::write(const buffer &data) {
	return;
}

void speaker::write_dwords(const int *data, int count) {
	assert("speaker: attempt to read data from null pointer", data == 0);

	for (int i = 0; i < count; i++) {
		arch::speaker_change_freq(data[i]);
	}
}

void speaker::set_ondatareceived(delegate<void>){}

bool speaker::init_device(p<did> iadr) {
	bool inited = true;
	return inited;
}

void speaker::register_type() {
	manec::get()->register_type<speaker>("speaker", "isa");
}
