/* Quarn OS
 *
 * Realtek 8139 ethernet card
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "rtl8139.h"
#include "pci.h"

#include "manes/manec.h"
#include "services/interrupt_manager.h"

#include "arch/low/lowlevel.h"
#include "arch/low/hlt.h"

using namespace resources;

extern p<net::nic> faar;

bool rtl8139::init_device(p<did> id) {
	device_id = id;
        this->id = *id.cast<pci_did>();
	init();
	faar = this;

	return true;
}

bool rtl8139::check_device(p<did> id) {
	p<pci_did> pid = id.cast<pci_did>();

	if (!pid.valid())
		return false;

	/* It must be an ethernet card */
	unsigned int devclass = pid->get_devclass() >> 8;
	if (devclass != 0x0200)
		return false;

	unsigned int devid = pid->get_devid();
	/* It must be a *compatible* ethernet card */
	if (devid != 0x813910ec)
		return false;

	return true;
}

void rtl8139::send_data(const buffer &x) {
	send_packet(x);
}


net::mac_addr rtl8139::get_haddr() const {
	u8 addr[6];
	get_mac_addr(addr);
	return net::mac_addr::from_be(*(u32*)addr, ((u16*)addr)[2]);
}


void rtl8139::irq_handler() {
	u16 status = id.inw(isr);
/*	if (status & 4) {
		id.outw(isr, 4);
		id.inw(isr);
	}*/
	if (status & 1)  {
		receive_packet();
		//delegate<void> rec;
		//rec.method(this, &rtl8139::receive_packet);
		//manes::manec::get()->get<services::asynch_tasks>("/asnych_tasks")->add_task(rec);
	} else if (status & 2) {
		if (!(id.inb(cmd) & 1))
			asm("cli\nhlt"::"a"(0xd00d00));

		id.outw(isr, status);
		id.inw(isr);
	} else {
		id.outw(isr, status);
		id.inw(isr);
	}

	arch::unlock_irqs(id.irq);

}

void rtl8139::init() {
	/* Set IRQ */
	manes::manec::get()->get<services::interrupt_manager>("/interrupt_manager")->register_interrupt(id.irq + 0x20, delegate<void>::method(this, &rtl8139::irq_handler));

	/* Power on */
	id.outb(config1, 0);

	/* Software reset */
	id.outb(cmd, 0x10);
	while (id.inb(cmd) & 0x10);

	/* Prepare receive buffer */
	rec_buffer = new char[8192 + 16];
	id.outl(rbstart, (u32)rec_buffer);

	/* Set IMR, ISR */
	/* IRQ on Transmit OK and Receive OK */
	id.outw(imr, 1);
	id.outw(isr, 0xffff);

	/* Configure receiver */
	id.outl(rec_conf, (0xf | (1 << 7)) & ~(u32)1);

	/* Enable receiver and transmitter */
	id.outb(cmd, 0xc);
}

void rtl8139::send_packet(const buffer &x) {
	assert("rtl8139: packet is too big", x.get_size() >= 0x700);
	/* Set TSAD */
	id.outl(tsad0 + send_index * 4, (u32)x.get_address());

	/* Set TSD */
	id.outl(tsd0 + send_index * 4, x.get_size());
	send_index = (send_index + 1) % 4;
}

void rtl8139::receive_packet() {
	while ((id.inb(cmd) & 1) == 0) {
		packet *pkg = (packet*)((u32)rec_buffer + received_index);
		u32 size = pkg->size;
		u32 status = pkg->status;

		received_index = (received_index + size + 4 + 3) & ~3;
		//received_index %= 8192 + 16;
 
	//	if (status & 1) {
			u8* buf = new u8[size];
			memcpy(buf, pkg, size);

			pkg = (packet*)buf;
			id.outw(capr, received_index - 16);
	//	}

		id.outw(isr, 1);
		id.inw(isr);

	//	if (status & 1)
			receiver(buffer((void*)&pkg[1], pkg->size - 4));
	
		delete []buf;
	}
}

void rtl8139::get_mac_addr(u8 *addr) const {
	for (int i = 0; i < 6; i++)
		addr[i] = id.inb(i);
}


void rtl8139::register_type() {
	manes::manec::get()->register_driver<rtl8139>("rtl8139", "nic", delegate<bool, p<did> >::function(&rtl8139::check_device));
}
