#include "device.h"
#include "did.h"

namespace resources {
	class usb_mass_storage : public device {
	private:
		p<usb_did> id;

		struct cbw {
			u32 dCBWSignature;
			u32 dCBWTag;
			u32 dCBWDataTransferLength;
			u8 bmCBWFlags;
			u8 bCBWLUN;
			u8 bCBWCBLength;
		} __attribute__((packed));

		int tag;
	public:
		bool init_device(p<did>);

		static bool check_device(p<did>);
		static void register_type();
	};
}
