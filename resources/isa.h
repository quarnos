/* Quarn OS
 *
 * ISA bus
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _ISA_H_
#define _ISA_H_

#include "bus.h"
#include "did.h"

namespace resources {
	class isa : public bus {
	private:
		int dev_count;

	protected:
		int scan_bus();

	public:
		bool initialize();
		int get_count();

		static void register_type();
	};

	class isa_did : public io_did {
	public:
		int irq;
		int dma_channel;

		isa_did(bus *b) : io_did(b) {}
		isa_did() {}
		virtual ~isa_did() {}
		virtual void accepted() { }
	};
}

#endif
