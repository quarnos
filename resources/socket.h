/* Quarn OS
 *
 * Socket
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _SOCKET_H_
#define _SOCKET_H_

#include "device.h"

namespace resources {
	class socket {
	public:
		virtual void send_packet(const buffer &) = 0;
		//virtual void set_receiver(delegate<void,const buffer &>) = 0;
	};

	class dev_socket : public device, public socket {
	public:
	};
}

#endif
