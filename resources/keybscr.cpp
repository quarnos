/* Quarn OS
 *
 * Keyboard and Screen
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "keybscr.h"
#include "x86_keybscr.h"

#include "manes/manec.h"
#include "libs/delegate.h"
#include "libs/colors.h"

using namespace manes;
using namespace resources;

keybscr::keybscr() : x(17), y(0) {}


void keybscr::read(buffer &data) {
	for (int i = 0; i < data.get_size(); i++)
		data[i] = buf.pop();
}

void keybscr::write(const buffer &data) {
	for (int i = 0; i < data.get_size(); i++) {

		if (data[i] == '\n') {
			if (y < get_height() - 1)
				y++;
			else
				scr_scroll();
			x = 0;
		} else if (data[i] == '\t') {
			x = (x/8 + 1) * 8;
		} else {
			screen_print(data[i], foreground | (background << 4), x, y);
			x++;
		}

		y += x / get_width();
		if (x / get_width() > 0 && y >= get_height() - 1)
			scr_scroll();
		y = y < get_height() ? y : get_height() - 1;
		x = x % get_width();
	}

}

void keybscr::set_ondatareceived(delegate<void> event) {
	assert("keybscr: attempt to set null event handler", event.null());
	on_data_received = event;
}

void keybscr::received(const char key) {
	buf.push(key);
	write(buffer::to_mem(key));

	/* delayed action */
	on_data_received();
}

void keybscr::register_type() {
	manec::get()->register_type<arch_keybscr>("keybscr", "console");
}
