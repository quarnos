/* Quarn OS
 *
 * File class
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "file.h"
#include "fs.h"
#include "manes/manec.h"
using namespace resources;

void file::set(string name, p<fs> _fs) {
	file_name = name;
	filesystem = _fs;

	metadata = filesystem->load_meta(file_name);
}

file::operator const string() {
	if (!buf.valid()) {
		buf = new buffer(metadata.size);
		filesystem->load_file(file_name, *buf);
	}
	return string(reinterpret_cast<char*>(buf->get_address()));
}

buffer file::get_buffer() {
	if (!buf.valid()) {
		buf = new buffer(metadata.size);
		filesystem->load_file(file_name, *buf);
	}
	return *buf;
}

void file::register_type() {
	manes::manec::get()->register_type<file>("file", "fat");
}
