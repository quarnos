/* Quarn OS
 *
 * FDC
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "fdc.h"
#include "manes/manec.h"

using namespace manes;
using namespace resources;

fdc::fdc() : sector_size(512), sectors_per_track(18) {
	//set_access_control(new spinlock);
}

void fdc::read_block(int address, int count, void *buffer) {
	//access_read();

	/* LBA -> CHS */
	int head = (address / sectors_per_track) & 1;
	int cyl = (address / sectors_per_track) >> 1;
	int start = address % sectors_per_track + 1;

	fdc_read_sector(buffer, head, cyl, start, start + count);

	//release();
}

void fdc::write_block(int address, int count, void *buffer){
	/* LBA -> CHS */
	int head = (address / sectors_per_track) & 1;
	int cyl = (address / sectors_per_track) >> 1;
	int start = address % sectors_per_track + 1;

	fdc_write_sector(buffer, head, cyl, start, start + count);
}

int fdc::get_granulity() {
	return sector_size;
}

bool fdc::init_device(p<did> iadr) {
	isaaddr = *iadr.cast<isa_did>();

	fdc_init();

	inited = true;

	return true;
}

void fdc::register_type() {
	manec::get()->register_type<fdc>("fdc", "isa");
}
