/* Quarn OS
 *
 * NE2000 ethernet card
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "ne2k.h"
#include "pci.h"

#include "manes/manec.h"

using namespace resources;

void get_mac_addr(u8 *mac);
void ne2k_pci_init(pci_did pciaddr);
void ne2k_pci_send_packet(pci_did pciaddr, u8 *buff, u16 len);

p<net::nic> faar;

bool ne2k::init_device(p<did> id) {
	device_id = id;
	faar = this;
	ne2k_pci_init(*id.cast<pci_did>());
	return true;
}

bool ne2k::check_device(p<did> id) {
	p<pci_did> pid = id.cast<pci_did>();

	if (!pid.valid())
		return false;

	/* It must be an ethernet card */
	unsigned int devclass = pid->get_bus().cast<pci>()->get_devclass(id) >> 8;
	if (devclass != 0x0200)
		return false;

	unsigned int devid = pid->get_bus().cast<pci>()->get_devid(id);
	/* It must be a *compatible* ethernet card (for example Realtek 8029) */
	if (devid != 0x802910ec)
		return false;

	return true;
}

void ne2k::send_data(const buffer &x) {
	ne2k_pci_send_packet(*device_id.cast<pci_did>(), (u8*)x.get_address(), x.get_size());
}


net::mac_addr ne2k::get_haddr() const {
	u8 addr[6];
	get_mac_addr(addr);
	return net::mac_addr::from_be(*(u32*)addr, ((u16*)addr)[2]);
}

void ne2k::register_type() {
	delegate<bool, p<did> > chk;
	chk.function(&ne2k::check_device);
	manes::manec::get()->register_driver<ne2k>("ne2k", "nic", chk);
}
