/* Quarn OS
 *
 * Physical Memory Manager
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "arch/low/e820.h"

#include "manes/manec.h"
#include "physmem.h"

using namespace resources;

physmem::physmem() : last_p((paddr)0x300000), available_memory(arch::get_memory_size()), default_region(0) {
	region kern = { (paddr)0, 0x80000000, (paddr)0x300000, region::kernel };
	regions.add(kern);

	region usr = { (paddr)0x80000000, 0x80000000, (paddr)0x80000000, region::user };
	regions.add(usr);
}

void physmem::map_page(paddr ppage, vaddr vpage) {

}

void *physmem::allocate_space(unsigned int) {
	paddr ppage = last_p;
	last_p += 0x1000;

	if (last_p >= 0x400000)
		critical("no more free memory");

	vaddr vpage = regions[default_region].last;
	regions[default_region].last += 0x1000;

	map_page(ppage, vpage);

	return (void*)vpage;
}

vaddr physmem::get_page(region::type t) {
	for (int i = 0; i < regions.get_count(); i++) {
		if (regions[i].rtype == t) {
			paddr ppage = last_p;
			last_p += 0x1000;

			if (last_p >= 0x400000)
				critical("no more free memory");
	
			vaddr vpage = regions[i].last;
			regions[i].last += 0x1000;

			map_page(ppage, vpage);

			return vpage;
		}
	}
	/* throw */
	return (vaddr)0;
}

void physmem::deallocate_space(void *page) {
}

void physmem::register_type() {
	manes::manec::get()->register_type<physmem>("physmem", "manec");
}

