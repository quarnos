#include "x86_keybscr.h"

#include "arch/low/general.h"
#include "arch/low/lowlevel.h"
#include "resources/isa.h"
#include "manes/manec.h"
#include "services/interrupt_manager.h"
#include "manes/error.h"


using namespace resources;

bool x86_keybscr::init_device(p<did> iadr) {
	shift = false;

	/* Set IRQ1 */
	manes::manec::get()->get<services::interrupt_manager>("/interrupt_manager")->register_interrupt(0x21, delegate<void>::method(this, &x86_keybscr::keyboard_received));

	return true;
}

/* Scroll screen */
void x86_keybscr::scr_scroll() {
	u8 *text_mem = (u8*)0xB8000;

	for (int i = 0; i < get_width() * get_height() * 2; i++)
		text_mem[i] = text_mem[i + get_width() * 2];

	int pos = get_width() * (get_height() - 1);
	arch::outb(0x3D4, 14);
	arch::outb(0x3D5, pos >> 8);
	arch::outb(0x3D4, 15);
	arch::outb(0x3D5, pos);
}

/* Show a sign of selected colour in certain place */
void x86_keybscr::screen_print(char sign, int color, int x, int y) {
	u8 *text_mem = (u8*)0xB8000;
	int pos = x + y * get_width();

	last_x = last_y = 0;
	assert("console: wrong coordinates", x > 79 || x < 0 || y > 24 || y < 0);

	/* Put char into graphic card memory */
	text_mem[pos * 2] = sign;
	text_mem[pos * 2 + 1] = color;

	/* Move a cursor */
	pos++;
	arch::outb(0x3D4, 14);
	arch::outb(0x3D5, pos >> 8);
	arch::outb(0x3D4, 15);
	arch::outb(0x3D5, pos);

	last_x = x;
	last_y = y;
}

/* Scancode to ASCII translators */

const char x86_keybscr::scan_to_ascii[] = {
	' ',' ',
	'1','2','3','4','5','6','7',
	'8','9','0','-','=','\b',' ',
	'q','w','e','r','t','y','u',
	'i','o','p','[',']','\n',' ',
	'a','s','d','f','g','h','j',
	'k','l',';','\'','\\',' ','\\',
	'z','x','c','v','b','n','m',
	',','.','/',' ',' ',' ',' ',' '
};

const char x86_keybscr::scan_to_bigascii[] = {
	' ',' ',
	'!','@','#','$','%','^','&',
	'*','(',')','_','+','\b',' ',
	'Q','W','E','R','T','Y','U',
	'I','O','P','{','}','\n',' ',
	'A','S','D','F','G','H','J',
	'K','L',':','\"','|',' ','|',
	'Z','X','C','V','B','N','M',
	'<','>','?',' ',' ',' ',' ',' '
};

void x86_keybscr::keyboard_received() {
	arch::unlock_irqs(1);

	u8 scan_code = arch::inb(0x60);

	if (scan_code == 0x36 || scan_code == 0x2a)
		shift = true;

	else if (scan_code == 0xb6 || scan_code == 0xaa)
		shift = false;
	else {
		if (scan_code > 0x35 && scan_code != 0x39) return;

		if (shift)
			received(scan_to_bigascii[scan_code]);
		else
			received(scan_to_ascii[scan_code]);
	}
}
