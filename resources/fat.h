/* Quarn OS
 *
 * FAT filesystem
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "fs.h"
#include "arch/low/general.h"
#include "libs/string.h"

namespace resources {
	class fat : public fs {
	private:
		enum fat_attribs {
			fat_read_only = 1,
			fat_hidden = 2,
			fat_system = 4,
			fat_volume_id = 8,
			fat_directory = 0x10,
			fat_archive = 0x20
		};

		struct dir_entry {
			u8 name[8];
			u8 format[3];
			u8 attr;
			u8 reserved[10];
			u16 time;
			u16 date;
			u16 cluster;
			u32 size;
		}  __attribute__((__packed__));

		static const int mbr_sectors_per_fat = 0x16;
		static const int mbr_root_entries = 0x11;
		static const int mbr_fat_count = 0x10;
		static const int mbr_reserved_sectors = 0xd;
		static const int mbr_dev_desc = 0x15;
		static const int mbr_fsid = 0x36;

		dir_entry *root_dir;
		dir_entry *current_dir;

		unsigned char *mbr;
		unsigned char *main_fat;

		int find_file(const char *filename);
		inline short get_cluster_val(int index);
		inline void set_cluster_val(int n, int val);
		void filename_to_fatname(const char *filename, char *fatname);
		void fatname_to_filename(const char *filename, char *fatname);

		bool mounted;
	public:
		fat();

		void load_file(const string &filename, buffer &addr);
		file::file_meta load_meta(const string &filename);

		list<p<file> > list_directory(const string &dirname);

		void save_file(const string &filename, buffer &addr);

		bool mount(p<block>);

		static void register_type();

	};
}
