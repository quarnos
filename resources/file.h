/* Quarn OS
 *
 * File class
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _FILE_H_
#define _FILE_H_

#include "libs/date.h"
#include "libs/string.h"
#include "libs/pointer.h"
#include "libs/list.h"
#include "libs/buffer.h"
#include "manes/cds/component.h"

#include "prvl.h"

namespace resources {
	class fs;
	class file : public manes::cds::component {
	public:
		typedef enum {
			attr_hidden = 1,
			attr_system = 2
		} attrib;
		typedef struct {
			date creation_d;
			time creation_t;

			prvl access;
			attrib attributes;
			bool directory;

			int size;
		} file_meta;

	protected:
		p<fs> filesystem;

		string file_name;

		file_meta metadata;

		/* Data loaded on demand */
		mutable p<buffer> buf;

		list<p<file> > children;
	public:
		void set(string, p<fs>);

		string get_name() const { return file_name; }
		unsigned int get_size() const { return metadata.size; }
		buffer get_buffer();

		void seek(int);
		void read(buffer &);
		void write(const buffer &);

		operator const string();

		void save() const;
		void remove();
		p<file> copy(string) const;

		static void register_type();
	};
}

#endif
