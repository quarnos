/* Quarn OS
 *
 * USB bus
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "pci.h"
#include "usb.h"
#include "uhci.h"

#include "manes/manec.h"

using namespace resources;

int usb_count = 0;

bool usb::init_bus(p<device> dev) {
	host = dev.cast<usb_hc>();

	if (!host.valid())
		return false;

	host->scan_bus(delegate<void, int>::method(this, &usb::device_connected));

	return true;
}

int usb::get_count() {
	return 2;
}

#define SET_ADDRESS 5
#define GET_DESCRIPTOR 6
#define GET_CONFIGURATION 8
#define SET_CONFIGURATION 9

string usb::get_dev_string(int index, int addr) {
	if (!index)
		return string();

	usb_setup_data setup;
	setup.bmRequestType = 0x80;
	setup.bRequest = GET_DESCRIPTOR;
	setup.wValue = 3 << 8 | index;
	setup.wIndex = 0;
	setup.wLength = 1;
	int size = *(char*)host->control_transfer(pid_setup, &setup, sizeof(usb_setup_data), 1, addr);

	setup.wLength = size;
	char *bytes = (char*)host->control_transfer(pid_setup, &setup, sizeof(usb_setup_data), size, addr);
	bytes += 2;

	return string::from_utf16(bytes);
}

void usb::device_connected(int port) {
	usb_count++;

	/* Get device descriptor */
	usb_setup_data setup;
	setup.bmRequestType = 0x80;
	setup.bRequest = GET_DESCRIPTOR;
	setup.wValue = 1 << 8;
	setup.wIndex = 0;
	setup.wLength = 16;

	device_descriptor *dev = (device_descriptor*)host->control_transfer(pid_setup, &setup, sizeof(usb_setup_data), sizeof(device_descriptor),0);

	u8 addr = host->get_free_address();

	/* Set device address */
	setup.bmRequestType = 0;
	setup.bRequest = SET_ADDRESS;
	setup.wValue = addr;
	setup.wIndex = 0;
	setup.wLength = 0;

	host->control_transfer(pid_setup, &setup, sizeof(usb_setup_data), 0,0);

	/* Get device descriptor */
	setup.bmRequestType = 0x80;
	setup.bRequest = GET_DESCRIPTOR;
	setup.wValue = 1 << 8;
	setup.wIndex = 0;
	setup.wLength = 16;

	dev = (device_descriptor*)host->control_transfer(pid_setup, &setup, sizeof(usb_setup_data), sizeof(device_descriptor),addr);
	get_dev_string(dev->iProduct, addr);

	/* Get configuration descriptor */
	setup.bmRequestType = 0x80;
	setup.bRequest = GET_DESCRIPTOR;
	setup.wValue = 2 << 8;
	setup.wIndex = 0;
	setup.wLength = 0x20;
	configuration_descriptor *conf = (configuration_descriptor*)host->control_transfer(pid_setup, &setup, sizeof(usb_setup_data), 0x20/* sizeof(configuration_descriptor)*/, addr);

	p<usb_did> id = new usb_did(this, addr, (interface_descriptor*)((unsigned int)conf + conf->bLength));
	endpoint_descriptor *endp = (endpoint_descriptor*)((unsigned int)conf + conf->bLength + 9);
	
	int read_ep = 0;
	int write_ep = 0;
	int control_ep = 0;

	while (endp->bDescriptorType == 5) {
		if (endp->bmAttributes != 2)
			continue;

		if (endp->bEndpointAddress & 0x80)
			read_ep = endp->bEndpointAddress & ~0x80;
		else
			write_ep = endp->bEndpointAddress;

		endp = endp + 1;
	}

	id->set_endps(control_ep, read_ep, write_ep);
	id->set_conf(conf->bConfigurationValue);
	create_device(id);
	__asm__ __volatile__("cli\nhlt"::"a"(conf),"b"(write_ep),"c"(read_ep));

}

void usb::set_conf(int addr, int conf) {
	usb_setup_data setup;
	setup.bmRequestType = 0;
	setup.bRequest = SET_CONFIGURATION;
	setup.wValue = conf;
	setup.wIndex = 0;
	setup.wLength = 0;

	host->control_transfer(pid_setup, &setup, sizeof(usb_setup_data), 0, addr);
}

void usb::register_type() {
	manes::manec::get()->register_type<usb>("usb", "pci");
}
