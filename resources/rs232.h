/* Quarn OS
 *
 * RS232
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _RS232_H_
#define _RS232_H_

#include "stream.h"
#include "device.h"
#include "isa.h"
#include "arch/low/rs232.h"

namespace resources {
	class rs232 : public dev_stream {
	private:
		/* some kind of buffer here */

		isa_did isaaddr;

	public:
		typedef arch::parity_check parity_check;
		typedef arch::rs232_mode rs232_mode;

		void read(buffer &);
		void write(const buffer &data);
		void write_dwords(const int *, int);

		void received(const char a);

		//resource_result *configure(rs232_mode mode, int speed, int bits,
		//	   parity_check parity, int stop);

		bool init_device(p<did> iadr);

		void set_ondatareceived(delegate<void>);

		static bool check_device(p<did> id);

		static void register_type();
	};
}

#endif
