/* Quarn OS
 *
 * UHCI driver
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "pci.h"
#include "usb.h"
#include "uhci.h"
//#include "manes/cds/
#include "manes/manec.h"
#include "manes/cds/root.h"

#include "arch/low/lowlevel.h"
//#include "irq.h"
#include "arch/low/hlt.h"
#include "manes/error.h"
#include "services/interrupt_manager.h"

using namespace resources;

int uhci_count = 0;

bool uhci::init_device(p<did> id) {
	p<pci_did> pid = id.cast<pci_did>();

	if (!pid.valid())
		return false;

	pciid = *pid;

	uhci_count++;
	init();

	manes::manec::get()->get_root()->new_component(manes::cds::component_name::from_path("/type,usb")).cast<bus>()->init_bus(this);	

	return true;
}


bool uhci::check_device(p<did> id) {
	p<pci_did> pid = id.cast<pci_did>();

	if (!pid.valid())
		return false;

	int devclass = pid->get_bus().cast<pci>()->get_devclass(id);
	if (devclass != 0x0c0300)
		return false;

	return true;
}

void *operator new(unsigned int, int);
void *operator new[](unsigned int, int);


#define UHCI_STOP	0
#define UHCI_RUN	1
#define UHCI_HCRESET	2
#define UHCI_GRESET	4
#define UHCI_EGSM	8
#define UHCI_FGR	0x10
#define UHCI_SWDGB	0x20
#define UHCI_CF		0x40
#define UHCI_MAXP	0x80

#define TD_TOKEN_DATATOGGLE	(1 << 19)

#define TD_CONTROL_ACTIVE	(1 << 23)

#define UHCI_INVALID		1
#define UHCI_VALID		0
#define UHCI_QUEUEHEAD		2
#define UHCI_TRANSFERDESC	0

void uhci::init_structures() {
	/* new does not give aligned pointers */
	/* Create TD */
        td = new (16) uhci::transfer_descriptor; 
	td->link_ptr = UHCI_INVALID;
	td->control = 0;
	td->token = 0;

	/* Create QH */
	qh = new (16) queue_head;
	qh->head_ptr = UHCI_INVALID;
	qh->element_ptr = (u32)td | UHCI_TRANSFERDESC;
	

	/* Create frame list pointer */
	if (!fp)
		fp = (frame_pointer*)new (4096) char[4096];
	fp[last_fp++] = (u32)qh | UHCI_VALID | UHCI_QUEUEHEAD;

	for (int i = last_fp; i < 1024; i++)
		fp[i] = UHCI_INVALID;

	/* give fp to the uhci */
	if ((int)fp & 0xfff)
		__asm__("cli\nhlt"::"a"(fp)); //critical("oh my cthulhu! frame list is not aligned!");
	pciid.outl(uhci_flbaseadd, (u32)fp);
}

void uhci::irq() {
	arch::unlock_irqs(pciid.irq);
	__asm__("cli\nhlt"::"a"(0xdeadbeef));
}

/* bulk transfer */
void uhci::bulk_transfer(int pid, void *val, int length, int address, int endp) {
	init_structures();

	td->link_ptr = UHCI_INVALID;
	td->buffer = (u32)val;
	td->control = 1 << 26 | 1 << 24 | 1 << 23;
	td->token = (length - 1) << 21 | pid | address << 8 | endp << 15;

	pciid.outw(uhci_command, UHCI_RUN);
	/* Wait until frame is completed */
	while (!(pciid.inw(uhci_status) & 1));
	pciid.outw(uhci_command, UHCI_STOP);

	/* Clear uhci status */
	pciid.outw(uhci_status, 0xffff);
}

/* control transfer */
void *uhci::control_transfer(int pid, void *val, int length, int rlength, u8 address) {
	/* Control message */
	init_structures();

	/* setup stage */
	td->link_ptr = (u32)new (16) transfer_descriptor;
	td->buffer = (u32)val;
	td->control = 1 << 26 | 1 << 23;
	td->token = (length - 1) << 21 | pid | address << 8;

	u8 *buffer = (u8*)0;

	transfer_descriptor *status = (transfer_descriptor*)td->link_ptr;

	/* optional data stage */
	if (rlength > 0) {
		buffer = (u8*)new char[rlength];

		/* In message (get data from device) */
		transfer_descriptor *rp = (transfer_descriptor *)td->link_ptr;
		rp->link_ptr = (u32)new (16) transfer_descriptor;
		status = (transfer_descriptor*)rp->link_ptr;
		rp->buffer = (u32)buffer;
		rp->control = 1 << 26 | 1 << 23;
		rp->token = (rlength - 1) << 21 | usb::pid_in | address << 8;
	}

	/* status stage */	
	status->link_ptr = UHCI_INVALID;
	status->buffer = 0;
	status->control = 1 << 26 | 1 << 23 | 1 << 24;
	if (rlength != 0)
		status->token = 0x7ff << 21 | usb::pid_out | address << 8;
	else
		status->token = 0x7ff << 21 | usb::pid_in | address << 8;

	pciid.outw(uhci_command, UHCI_RUN);
	/* Wait until frame is completed */
	while (!(pciid.inw(uhci_status) & 1));// __asm__("sti\nhlt");
	pciid.outw(uhci_command, UHCI_STOP);

	/* Clear uhci status */
	pciid.outw(uhci_status, 0xffff);

	return buffer;
}

int uhci::get_free_address() {
	return 1;
}

void uhci::init() {
	manes::manec::get()->get<services::interrupt_manager>("/interrupt_manager")->register_interrupt(pciid.irq, delegate<void>::method(this, &uhci::irq));

	/* Reset UHCI */
	pciid.outw(uhci_command, UHCI_STOP | UHCI_HCRESET);
	while (pciid.inw(uhci_command) & UHCI_HCRESET);

	/* Prepare structures */
//	uhci_init_structures(pciaddr);

	pciid.outw(4, 4);

	/* Reset ports */
	pciid.outw(uhci_portsc1, 0x200);
	pciid.outw(uhci_portsc1 + 2, 0x200);

	wait_loop();

	pciid.outw(uhci_portsc1, 0);
	pciid.outw(uhci_portsc1 + 2, 0);
}

void uhci::scan_bus(delegate<void, int> device_connected) {
	/* Wait for connected devices */
	u16 read1 = 0x80, read2 = 0x80;

	if ((read1 = pciid.inw(uhci_portsc1)) == 0x80 && (read2 = pciid.inw(uhci_portsc1 + 2)) == 0x80)
		return;

	if (read1 != 0x80) {
		/* Enable port */
		pciid.outw(uhci_portsc1, 4);
		device_connected(uhci_portsc1);
	}

	if (read2 != 0x80) {
		/* Enable port */
		pciid.outw(uhci_portsc1 + 2, 4);
		device_connected(uhci_portsc1 + 2);
	}

	read2 = pciid.inw(uhci_portsc1+2);

}

void uhci::register_type() {
	manes::manec::get()->register_driver<uhci>("uhci", "pci", delegate<bool, p<did> >::function(&uhci::check_device));
}
