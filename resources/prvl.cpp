/* Quarn OS
 *
 * Access control
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "prvl.h"

using namespace resources;

prvl::prvl(){}

prvl::prvl(int uid, int mod) {
	owner = uid;

	prvl_owner = (basic_prvls)(mod >> 8);
	prvl_group = (basic_prvls)(mod >> 4 & 0xf);
	prvl_other = (basic_prvls)(mod & 0xf);
}

bool prvl::check_prvls(int uid, basic_prvls action) {
	if (owner == uid) {
		if (prvl_owner & action)
			return true;
		else
			return false;
	} else {
		if (prvl_other & action)
			return true;
		else
			return false;
       }
}
