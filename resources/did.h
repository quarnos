/* Quarn OS
 *
 * Device ID
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _DID_H_
#define _DID_H_

#include "manes/ods/object_stream.h"

#include "arch/low/general.h"
#include "arch/low/lowlevel.h"
#include "bus.h"

namespace resources {
	class did : public manes::ods::obj_val {
	protected:
        	p<bus> bus_id;

	public:
		did (p<bus> b) : bus_id(b) {}
		did() {}
		virtual ~did() {}

		virtual void accepted() = 0;

		p<bus> get_bus() const {
			return bus_id;
		}

		void serialize(manes::ods::object_stream &ostr) {
			ostr.start<did>();
			ostr << bus_id;
		}

		void deserialize(manes::ods::object_stream &ostr) {
			ostr >> bus_id;
		}
	};

	class io_did : public did {
	private:
		int ioport_base;

	public:
		io_did(p<bus> b) : did(b) {}
		io_did() {}
		virtual ~io_did() {}

		void set_ioport(int ioport) {
			ioport_base = ioport;
		}

		void outb(int addr, u8 data) const {
			arch::outb(ioport_base + addr, data);
		}

		void outw(int addr, u16 data) const {
			arch::outw(ioport_base + addr, data);
		}

		void outl(int addr, u32 data) const {
			arch::outl(ioport_base + addr, data);
		}

		u8 inb(int addr) const {
			return arch::inb(ioport_base + addr);
		}

		u16 inw(int addr) const {
			return arch::inw(ioport_base + addr);
		}

		u32 inl(int addr) const {
			return arch::inl(ioport_base + addr);
		}

		void serialize(manes::ods::object_stream &ostr) {
			ostr.start<io_did>();
			did::serialize(ostr);

			ostr << ioport_base;
		}

		void deserialize(manes::ods::object_stream &ostr) {
			did::deserialize(ostr);

			ostr >> ioport_base;
		}
	};
}

#endif
