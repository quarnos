/* Quarn OS
 *
 * NE2000 ethernet card
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "net/nic.h"

#include "libs/pointer.h"

namespace resources {
	/* socket */
	class ne2k : public net::nic, public device {
	public:

		bool init_device(p<did>);
		void send_data(const buffer &x);
		net::mac_addr get_haddr() const;

		static bool check_device(p<did>);
		static void register_type();
	};
}
