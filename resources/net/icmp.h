#ifndef _ICMP_H_
#define _ICMP_H_

#include "ipv4.h"
#include "transport_layer.h"

namespace net {
	class icmp /*: public transport_layer*/ {
	private:
		struct icmp_packet {
			u8 type;
			u8 code;
			u16 checksum;
			u16 id;
			u16 sequence;
		};
		p<internet_layer> down;
		void receive(const ipv4_addr&, const buffer&);
	public:
		void set_internet_layer(p<internet_layer> x) {
			down = x;
			down->listen(1, delegate<void, const ipv4_addr&, const buffer&>::method(this, &icmp::receive));
		}

		void ping(const ipv4_addr &addr);
	};
}

#endif
