/* Based on RFC793 */

#include "tcp.h"
#include "ipv4.h"

#include "arch/low/general.h"

#include "libs/pointer.h"
#include "libs/array.h"

#include "tcp_client_socket.h"

using namespace net;

void tcp::send(const ipv4_addr &addr, u16 dst_port, u16 src_port, const buffer &data, tcp_flags flags, int seq, int ack) {
	p<tcp_packet> pkg = new tcp_packet;
	pkg->sender_port = to_be16(src_port);
	pkg->receiver_port = to_be16(dst_port);
	pkg->seq = to_be32(seq);
	pkg->ack = to_be32(ack);
	pkg->header_size = sizeof(tcp_packet) << 2;
	pkg->flags = flags;
	pkg->window_size = to_be16(1000); /*wtf?*/
	pkg->checksum = 0;
	pkg->piority = 0;


	p<pseudo_header> ph = new pseudo_header;
	ph->source = down.cast<ipv4>()->get_my_ip().to_be();
	ph->destination = addr.to_be();
	ph->zero = 0;
	ph->protocol = 6;
	ph->length = to_be16(sizeof(tcp_packet) + data.get_size());
	array<u16> ph_words = array<u16>::from_buffer(buffer::to_mem(ph));

	buffer buf = buffer::to_mem(pkg);

	buf += data;

	array<u16> words = array<u16>::from_buffer(buf);
	u32 sum = 0;

	for (unsigned int i = 0; i < (sizeof(tcp_packet) + data.get_size()) / 2; i++)
		sum += words[i];

	if (data.get_size() % 2)
		sum += (u16)data[data.get_size() - 1];

	for (unsigned int i = 0; i < sizeof(pseudo_header) / 2; i++)
		sum += ph_words[i];

	//ph.dispose();

	sum = (sum & 0xffff) + (sum >> 16);
	sum += (sum >> 16);

	buf.cast<tcp_packet>()->checksum = to_le16(~sum);

	down->send_data(addr, 6, buf);

	//pkg.dispose();
}

void tcp::receive(const ipv4_addr &sender, const buffer &data) {
	p<tcp_packet> pkg = data.cast<tcp_packet>();

	for (int i = 0; i < ports.get_count(); i++)
		if (ports[i] == from_be16(pkg->receiver_port))
			listeners[i](sender, from_be16(pkg->sender_port), data.cut_first(pkg->header_size >> 2), (tcp_flags)pkg->flags, from_be32(pkg->seq), from_be32(pkg->ack));
}

void tcp::listen(u16 port, port_listener listener) {
	for (int i = 0; i < ports.get_count();) {
		if (ports[i] == port) {
			ports.remove(i);
			listeners.remove(i);
		} else
			i++;
	}

	ports.add(port);
	listeners.add(listener);
}

p<client_socket> tcp::create_client() { 
	return (client_socket*)new tcp_client_socket(this);
}

p<server_socket> tcp::create_server(int port) { }
