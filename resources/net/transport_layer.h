#ifndef _TRANSPORT_LAYER_H_
#define _TRANSPORT_LAYER_H_

#include "internet_layer.h"

#include "client_socket.h"
#include "server_socket.h"

namespace net {
	class transport_layer : public osi_layer {
	protected:
		p<internet_layer> down;

	public:
		virtual void set_internet_layer(p<internet_layer> x) {
			down = x;
		}

		virtual int acquire_port() = 0;
		virtual void release_port(int) = 0;

		virtual p<client_socket> create_client() = 0;
		virtual p<server_socket> create_server(int port) = 0;
	};
}

#endif
