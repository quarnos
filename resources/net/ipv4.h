#ifndef _IPV4_H_
#define _IPV4_H_

#include "link_layer.h"
#include "arp.h"

namespace net {
	class ipv4 : public internet_layer {
	private:
		arp arp_prot;

		ipv4_addr my_ip;
		ipv4_addr mask;
		ipv4_addr gateway;

	protected:
//		void receive_data(void *buf, int length);

	public:
		void set_link_layer(p<link_layer> x);

		void configure_ipv4(const ipv4_addr &ip, const ipv4_addr &netmask, const ipv4_addr &gate) {
			my_ip = ip;
			mask = netmask;
			gateway = gate;
		}

		ipv4_addr get_my_ip() {
			return my_ip;
		}

		void send_data(const ipv4_addr &addr, int prot, const buffer &x);
		void receive_data(const buffer&);
	};
}

#endif
