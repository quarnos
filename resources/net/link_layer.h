#ifndef _LINK_LAYER_H_
#define _LINK_LAYER_H_

#include "nic.h"
#include "osi_layer.h"
#include "libs/pointer.h"

namespace net {
	class link_layer : public osi_layer {
	protected:
		p<nic> down;

		list<int> type_id;
		list<delegate<void, const buffer &> > handlers;

		virtual void receive_data(const buffer &) = 0;

	public:
		virtual void set_nic(p<nic>);
		virtual void add_handler(int, delegate<void, const buffer &>);

		virtual void send_data(const mac_addr &addr, u16 prot, const buffer &) = 0;
		virtual mac_addr get_haddr();
	};
}

#endif
