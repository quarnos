#include "ipv4.h"

#include "manes/error.h"
#include "libs/array.h"

#include "ipv4_packet.h"

using namespace net;

void ipv4::set_link_layer(p<link_layer> x) {
	arp_prot.set_link_layer(x);

	down = x;
	x->add_handler(0x800, delegate<void, const buffer&>::method(this, &ipv4::receive_data));
}

void ipv4::send_data(const ipv4_addr &addr, int prot, const buffer &x) {
	assert("ipv4: packet cannot be smaller than 20 bytes", x.get_size() < 20);

	p<ipv4_packet> pkg = new ipv4_packet;
	pkg->set_ptype((ipv4_packet::protocol)prot);

	pkg->set_source_paddr(my_ip);
	pkg->set_target_paddr(addr);

       	pkg->add_data(x);

	mac_addr haddr;
	if (addr.network(mask) == my_ip.network(mask))
		haddr = arp_prot.get_haddr(addr);
	else
		haddr = arp_prot.get_haddr(gateway);

	down->send_data(haddr, 0x800, pkg->generate());

	pkg.dispose();
}

void ipv4::receive_data(const buffer &data) {
	p<ipv4_packet> pkg = new ipv4_packet(data);

	for (int i = 0; i < protocols.get_count(); i++)
		if (protocols[i] == pkg->get_ptype()) 
			listeners[i](pkg->get_source_paddr(), pkg->get_data());

}
