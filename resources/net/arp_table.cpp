#include "arp_table.h"

using namespace net;

void arp_table::add(const arp_record &x) {
	for (int i = 0; i < table.get_count();)
		if (table[i].paddr == x.paddr)
			table.remove(i);
		else
			i++;

	table.add(x);
}

p<arp_table::arp_record> arp_table::get(const ipv4_addr &x) {
	for (int i = 0; i < table.get_count(); i++)
		if (table[i].paddr == x)
			return &table[i];

	return p<arp_table::arp_record>::invalid;
}
