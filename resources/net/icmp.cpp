#include "icmp.h"

#include "ipv4_packet.h"

#include "libs/array.h"

using namespace net;

void icmp::ping(const ipv4_addr &addr) {
	p<icmp_packet> pkg = new icmp_packet;
	pkg->type = 8;
	pkg->code = 0;
	pkg->checksum = 0;
	pkg->id = to_be16(0xfeed);
	pkg->sequence = to_be16(1);

	array<u8> data(36 - sizeof(icmp_packet));
	for (unsigned int i = 0; i < 36 - sizeof(icmp_packet); i++)
		data[i] = '0' + i;

	buffer buf = buffer::to_mem(pkg);
	buf += data.to_buffer();

	array<u16> words = array<u16>::from_buffer(buf);
	u32 sum = 0;
	for (int i = 0; i < words.length(); i++)
		sum += words[i];

	sum = (sum & 0xffff) + (sum >> 16);
	sum += (sum >> 16);

	buf.cast<icmp_packet>()->checksum = to_le16(~sum);

	down->send_data(addr, ipv4_packet::p_icmp, buf);

	pkg.dispose();
}

void icmp::receive(const ipv4_addr &addr, const buffer &data) {
	p<icmp_packet> request = data.cast<icmp_packet>();

	if (request->type != 8 || request->code != 0)
		return;

	p<icmp_packet> pkg = new icmp_packet;
	pkg->type = 0;
	pkg->code = 0;
	pkg->checksum = 0;
	pkg->id = request->id;
	pkg->sequence = request->sequence;

	buffer buf = buffer::to_mem(pkg);
	buf += data.cut_first(sizeof(icmp_packet));

	array<u16> words = array<u16>::from_buffer(buf);
	u32 sum = 0;
	for (int i = 0; i < words.length(); i++)
		sum += words[i];

	sum = (sum & 0xffff) + (sum >> 16);
	sum += (sum >> 16);

	buf.cast<icmp_packet>()->checksum = to_le16(~sum);

	down->send_data(addr, ipv4_packet::p_icmp, buf);

	request.dispose();
}
