#ifndef _INTERNET_LAYER_H_
#define _INTERNET_LAYER_H_

#include "osi_layer.h"
#include "link_layer.h"

#include "ipv4_addr.h"
#include "libs/list.h"
#include "libs/delegate.h"

namespace net {
	class internet_layer : public osi_layer {
	protected:
		p<link_layer> down;

		list<int> protocols;
		list<delegate<void, const ipv4_addr&, const buffer&> > listeners;
	public:
		virtual void set_link_layer(p<link_layer> x) {
			down = x;
		}

		virtual void send_data(const ipv4_addr &, int, const buffer &) = 0;

		virtual void listen(int prot, delegate<void, const ipv4_addr&, const buffer&> listener) {
			protocols.add(prot);
			listeners.add(listener);
		}
	};
}

#endif
