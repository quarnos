#ifndef _NIC_H_
#define _NIC_H_

#include "mac_addr.h"
#include "resources/device.h"

#include "libs/buffer.h"

namespace net {
	class nic {
	protected:
		delegate<void, const buffer &> receiver;

	public:
		virtual void send_data(const buffer &x) = 0;
		virtual mac_addr get_haddr() const = 0;

		void set_receiver(delegate<void, const buffer &> rec) {
			receiver = rec;
		}
	};
}

#endif
