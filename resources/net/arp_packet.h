#ifndef _ARP_PACKET_H_
#define _ARP_PACKET_H_

#include "arch/low/general.h"
#include "packet_builder.h"

namespace net {
	class arp_packet : public packet_builder {
	private:
		struct packet {
			u16 htype;
			u16 ptype;
			u8 hlen;
			u8 plen;
			u16 oper;
			u32 sha0;
			u16 sha1;
			u32 spa;
			u16 tha0;
			u32 tha1;
			u32 tpa;
		} __attribute__((packed));


		p<packet> pkg;
	public:
		arp_packet() : pkg(new packet) { }
		~arp_packet() {
			pkg.dispose();
		}

		buffer generate() const {
			return buffer::to_mem(pkg);
		}

		void add_data(const buffer&) { }

		enum ptypes {
			p_ipv4 = 0x800
		};

		enum htypes {
			h_ethernet = 1
		};

		void set_ptype(ptypes pt) {
			pkg->ptype = to_be16(pt);
			pkg->plen = ipv4_addr::in_bytes();
		}

		void set_htype(htypes ht) {
			pkg->htype = to_be16(ht);
			pkg->hlen = mac_addr::in_bytes();
		}

		enum arp_type {
			arp_request = 1,
			arp_reply = 2
		};

		void set_type(arp_type at) {
			pkg->oper = to_be16(at);
		}

		void set_source_haddr(const mac_addr &addr) {
			pkg->sha0 = addr.first_be32();
			pkg->sha1 = addr.last_be16();
		}

		void set_source_paddr(const ipv4_addr &addr) {
			pkg->spa = addr.to_be();
		}

		void set_target_haddr(const mac_addr &addr) {
			pkg->tha0 = addr.first_be16();
			pkg->tha1 = addr.last_be32();
		}

		void set_target_paddr(const ipv4_addr &addr) {
			pkg->tpa = addr.to_be();
		}

		arp_packet(const buffer &x) : pkg(x.cast<packet>()) { }

		ptypes get_ptype() const {
			return (ptypes)from_be16(pkg->ptype);
		}

		htypes get_htype() const {
			return (htypes)from_be16(pkg->htype);
		}

		arp_type get_type() const {
			return (arp_type)from_be16(pkg->oper);
		}

		mac_addr get_source_haddr() const {
			return mac_addr::from_be(pkg->sha0, pkg->sha1);
		}

		ipv4_addr get_source_paddr() const {
			return ipv4_addr::from_be(pkg->spa);
		}

		mac_addr get_target_haddr() const {
			return mac_addr::from_be_r(pkg->tha0, pkg->tha1);
		}

		ipv4_addr get_target_paddr() const {
			return ipv4_addr::from_be(pkg->tpa);
		}
	};
}

#endif
