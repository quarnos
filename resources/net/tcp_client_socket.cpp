#include "tcp_client_socket.h"

using namespace net;

void poll(volatile int *value, int wait_for) {
	while (*value != wait_for) asm volatile("nop");
}

void tcp_client_socket::connect(const ipv4_addr &addr, int port) {
	clean();

	server = addr;
	server_port = port;
	client_port = down->acquire_port();

	down_tcp->listen(client_port, tcp::port_listener::method(this, &tcp_client_socket::receive));

	sequence_number = 1;

	/* TCP three-way handshake 
	 * Part I: SYN packet */
	state = syn_sent;
	down_tcp->send(addr, port, client_port, buffer::empty, tcp::tcp_syn, sequence_number++, 0);
	poll(reinterpret_cast<volatile int*>(&state), established);
}

void tcp_client_socket::receive(const ipv4_addr &addr, u16 sender_port, const buffer &data, tcp::tcp_flags flags, int seq, int ack) {
	if (addr != server || server_port != server_port)
		return;

	ack_number = seq;

	if (state == syn_sent) {
		/* In the second part of handshake we should receive SYN/ACK packet */
		if (flags != (tcp::tcp_syn | tcp::tcp_ack)/* || ack != sequence_number - 1*/)
			return;

		/* TCP three-way handshake
		 * Part III: ACK packet */
		state = established;
		down_tcp->send(addr, sender_port, client_port, buffer::empty, tcp::tcp_ack, sequence_number++, seq + 1);
		return;
	} else if ((state == fin_wait1 || state == fin_wait2) && flags == (tcp::tcp_fin | tcp::tcp_ack)) {
		state = closed;
	} else if (state == fin_wait1 && flags == tcp::tcp_ack) {
		state = fin_wait2;
	} else if (state == established && flags == tcp::tcp_fin) {
		state = last_ack;
		down_tcp->send(server, server_port, client_port, buffer::empty, (tcp::tcp_flags)(tcp::tcp_fin | tcp::tcp_ack), sequence_number++, ack + 1);
	} else if (state == last_ack && flags == tcp::tcp_ack) {
		state = closed;
	}

	if (state == established) {
		incoming_data.push(data.copy());
	}
}

tcp_client_socket::tcp_client_socket(p<transport_layer> tl) : client_socket(tl), down_tcp(tl.cast<tcp>()) { }
tcp_client_socket::~tcp_client_socket() {
	clean();
}

void tcp_client_socket::write(const buffer &data) {
	if (state == established) {
		down.cast<tcp>()->send(server, server_port, client_port, data, (tcp::tcp_flags)(tcp::tcp_psh | tcp::tcp_ack), sequence_number, ack_number + 1);
		sequence_number += data.get_size() > 0 ? data.get_size() : 1;
	}
}

void tcp_client_socket::read(buffer &data) {
	if (state == closed || state == unknown)
		return;

	buffer buf = incoming_data.pop();
	data.copy_data(buf);
}

void tcp_client_socket::close() {
	state = fin_wait1;
	down_tcp->send(server, server_port, client_port, buffer::empty, tcp::tcp_fin, sequence_number++, 0);
	poll(reinterpret_cast<volatile int*>(&state), closed);
}

void tcp_client_socket::clean() {
	if (state == unknown)
		return;

	if (state != closed)
		close();

	if (client_port) {
	//	down.cast<tcp>()->remove_listener(client_port, tcp::port_listener::method(this, &tcp_client_socket::receive));
		down->release_port(client_port);
		client_port = 0;
	}

	state = unknown;
}
