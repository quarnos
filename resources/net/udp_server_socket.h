#ifndef _UDP_SERVER_SOCKET_
#define _UDP_SERVER_SOCKET_

#include "libs/fifo.h"
#include "libs/list.h"
#include "server_socket.h"

namespace net {
	class udp_server_socket : public server_socket {
	private:
		int server_port;

		fifo<p<remote_client_socket> > waiting_clients;
		list<p<remote_client_socket> > clients;

		void receive(const ipv4_addr &addr, u16 sender_port, const buffer &data);

	public:
		udp_server_socket(p<transport_layer> tl, int port);

		p<remote_client_socket> accept();
		void disconnect(p<remote_client_socket>);
	};
}

#endif
