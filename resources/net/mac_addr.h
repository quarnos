#ifndef _MAC_ADDR_H_
#define _MAC_ADDR_H_

#include "address.h"

#include "arch/low/general.h"

namespace net {
	class mac_addr : public address {
	private:
		u8 addr[6];

	public:
		mac_addr();
		mac_addr(const mac_addr &x);
		mac_addr &operator=(const mac_addr &x);

		static mac_addr from_be(u32, u16);
		//static mac_addr from_le(u32, u16);

		static mac_addr from_be_r(u16, u32);
		//static mac_addr from_le_r(u16, u32);

		u32 first_be32() const;
		u16 first_be16() const;

		u32 last_be32() const;
		u16 last_be16() const;

		static int in_bytes() {
			return 6;
		}

		virtual bool operator==(mac_addr &x);
		virtual bool operator!=(mac_addr &x) {
			return !operator==(x);
		}
	};
}

#endif
