#ifndef _ARP_TABLE_H_
#define _ARP_TABLE_H_

#include "mac_addr.h"
#include "ipv4_addr.h"
#include "arch/low/general.h"
#include "libs/list.h"
#include "libs/pointer.h"

namespace net {
	class arp_table {
	public:
		struct arp_record {
			u16 prot;
			mac_addr haddr;
			ipv4_addr paddr;
		};

	private:
		list<arp_record> table;

	public:
		void add(const arp_record &);
		p<arp_record> get(const ipv4_addr&);
	};
}

#endif
