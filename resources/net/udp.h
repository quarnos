#ifndef _UDP_H_
#define _UDP_H_

#include "transport_layer.h"
#include "ipv4_addr.h"

namespace net {
	class udp : public transport_layer {
	private:
		list<int> ports;
		list<delegate<void, const ipv4_addr&, u16, const buffer &> > listeners;

		int last_port;

		void receive(const ipv4_addr &, const buffer&);
	public:
		udp() : last_port(49152) { }

		typedef delegate<void, const ipv4_addr &, u16, const buffer &> port_listener;
		void set_internet_layer(p<internet_layer> x) {
			down = x;
			down->listen(17, delegate<void, const ipv4_addr&, const buffer&>::method(this, &udp::receive));
		}

		void send(const ipv4_addr&, u16, u16, const buffer &);
		void listen(u16, port_listener);
		void remove_listener(u16, port_listener);

		int acquire_port() {
			return last_port++;
		}

		void release_port(int) { }

		p<client_socket> create_client();
		p<server_socket> create_server(int port);
	};
}

#endif
