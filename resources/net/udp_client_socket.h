#ifndef _UDP_CLIENT_SOCKET_H_
#define _UDP_CLIENT_SOCKET_H_

#include "client_socket.h"
#include "libs/fifo.h"

namespace net {
	class udp_client_socket : public client_socket {
	private:
		int client_port;

		int server_port;
		ipv4_addr server;

		fifo<buffer> incoming_data;

		void receive(const ipv4_addr &addr, u16 sender_port, const buffer &data);

	public:
		udp_client_socket(p<transport_layer> tl);
		~udp_client_socket();

		void connect(const ipv4_addr &addr, int port);

		void write(const buffer &data);
	        void read(buffer &data);

		void close();
	};
}

#endif
