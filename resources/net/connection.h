namespace net {
	class connection : public socket {
	public:
		virtual void disconnect() = 0;
	}
}
