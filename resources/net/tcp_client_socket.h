#ifndef _TCP_CLIENT_SOCKET_H_
#define _TCP_CLIENT_SOCKET_H_

#include "tcp.h"
#include "client_socket.h"
#include "libs/fifo.h"

namespace net {
	class tcp_client_socket : public client_socket {
	private:
		p<tcp> down_tcp;

		int client_port;

		int server_port;
		ipv4_addr server;

		fifo<buffer> incoming_data;

		int sequence_number;
		int ack_number;

		enum connection_state {
			unknown,
			//listen,
			syn_sent,
			//syn_recv,
			established,
			fin_wait1,
			fin_wait2,
			//time_wait,
			//close_wait,
			last_ack,
			closed
		};

		volatile connection_state state;

		void receive(const ipv4_addr &addr, u16 sender_port, const buffer &data, tcp::tcp_flags, int, int);
		void clean();
	public:
		tcp_client_socket(p<transport_layer> tl);
		~tcp_client_socket();

		void connect(const ipv4_addr &addr, int port);

		void write(const buffer &data);
	        void read(buffer &data);

		void close();
	};
}

#endif
