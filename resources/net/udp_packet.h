#ifndef _UDP_PACKET_H_
#define _UDP_PACKET_H_

#include "libs/array.h"

#include "arch/low/general.h"
#include "packet_builder.h"

namespace net {
	class udp_packet : public packet_builder {
	private:
		struct pseudo_header {
			u32 source;
			u32 destination;
			u8 zero;
			u8 protocol;
			u16 length;
		} __attribute__((packed));

		struct packet {
			u16 sender_port;
			u16 receiver_port;
			u16 length;
			u16 checksum;
		} __attribute__((packed));


		mutable p<packet> pkg;
		mutable p<pseudo_header> ph;

		buffer data;
	public:
		udp_packet() : pkg(new packet), ph(new pseudo_header) { }
		~udp_packet() {
			pkg.dispose();
			ph.dispose();
		}

		buffer generate() const {
			/* Create pseudo header */
			ph->zero = 0;
			ph->protocol = 17;
			ph->length = to_be16(sizeof(packet) + data.get_size());

			/* Count packet length */
			pkg->length = to_be16(data.get_size() + sizeof(packet));
			
			/* Count checksum */
			array<u16> ph_words = array<u16>::from_buffer(buffer::to_mem(ph));

			buffer buf = buffer::to_mem(pkg);
			buf += data;

			array<u16> words = array<u16>::from_buffer(buf);
			u32 sum = 0;

			for (unsigned int i = 0; i < (sizeof(packet) + data.get_size()) / 2; i++)
				sum += words[i];

			if (data.get_size() % 2)
				sum += (u16)data[data.get_size() - 1];

			for (unsigned int i = 0; i < sizeof(pseudo_header) / 2; i++)
				sum += ph_words[i];


			sum = (sum & 0xffff) + (sum >> 16);
			sum += (sum >> 16);

			buf.cast<packet>()->checksum = to_le16(~sum);

			return buf;
		}

		void add_data(const buffer &x) {
			data = x;
		}

		void set_source_port(int port) {
			pkg->sender_port = to_be16(port);
		}

		void set_source_paddr(const ipv4_addr &addr) {
			ph->source = addr.to_be();
		}

		void set_target_port(int port) {
			pkg->receiver_port = to_be16(port);
		}


		void set_target_paddr(const ipv4_addr &addr) {
			ph->destination = addr.to_be();
		}

		udp_packet(const buffer &x) : pkg(x.cast<packet>()), data(x) { }

		buffer get_data() const {
			return data.cut_first(sizeof(packet));
		}

		int get_source_port() const {
			return from_be16(pkg->sender_port);
		}

		ipv4_addr get_source_paddr() const {
			//return ipv4_addr::from_be(pkg->src);
		}

		int get_target_port() const {
			return from_be16(pkg->receiver_port);
		}

		ipv4_addr get_target_paddr() const {
			//return ipv4_addr::from_be(pkg->dst);
		}
	};
}

#endif
