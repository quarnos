#ifndef _ARP_H_
#define _ARP_H_

#include "libs/buffer.h"
#include "mac_addr.h"
#include "arp_table.h"
#include "internet_layer.h"
#include "link_layer.h"
#include "arp_packet.h"

namespace net {
	class arp : public osi_layer {
	private:
		p<link_layer> down;

		enum {
			et_arp = 0x806
		};

		mac_addr my_haddr;
		ipv4_addr my_paddr;

		volatile bool received;

		arp_table table;

		void receive_data(const buffer &x);
	public:
		void set_link_layer(p<link_layer> x) {
			down = x;
			my_haddr = x->get_haddr();
			my_paddr = ipv4_addr::from_le(192 << 24 | 168 << 16 | 1 << 8 | 50);

			x->add_handler(et_arp, delegate<void, const buffer&>::method(this, &arp::receive_data));
		}

		void set_ipv4(const ipv4_addr&);

		mac_addr get_haddr(const ipv4_addr &ipaddr);
		void receive_packet(p<arp_packet> pkg);
	};
}

#endif
