#include "udp.h"
#include "udp_client_socket.h"

using namespace net;

void udp_client_socket::receive(const ipv4_addr &addr, u16 sender_port, const buffer &data) {
	if (addr != server || sender_port != server_port)
		return;

	incoming_data.push(data.copy());
}

udp_client_socket::udp_client_socket(p<transport_layer> tl) : client_socket(tl) { }

udp_client_socket::~udp_client_socket() {
	close();
}

void udp_client_socket::connect(const ipv4_addr &addr, int port) {
	server = addr;
	server_port = port;

	client_port = down->acquire_port();

	down.cast<udp>()->listen(client_port, udp::port_listener::method(this, &udp_client_socket::receive)); 
}

void udp_client_socket::write(const buffer &data) {
	down.cast<udp>()->send(server, server_port, client_port, data);
}

void udp_client_socket::read(buffer &data) {
	buffer buf = incoming_data.pop();
	if (data.get_address())
		data.copy_data(buf);
	else
		data = buf;
}

void udp_client_socket::close() {
	if (client_port != 0) {
		down->release_port(client_port);
		down.cast<udp>()->remove_listener(client_port, udp::port_listener::method(this, &udp_client_socket::receive));
		client_port = 0;
	}
}
