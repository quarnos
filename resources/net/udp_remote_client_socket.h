#include "udp_server_socket.h"
#include "client_socket.h"
#include "libs/fifo.h"
#include "libs/buffer.h"

namespace net {
	class udp_remote_client_socket : public remote_client_socket {
	private:
		int server_port;

		int client_port;
		ipv4_addr client;

		p<udp_server_socket> udp_server;

		fifo<buffer> incoming_data;

		void receive(const ipv4_addr &addr, u16 sender_port, const buffer &data) {
			if (addr != client || sender_port != client_port)
				return;

			incoming_data.push(data.copy());
		}

	public:
		udp_remote_client_socket(p<transport_layer> tl, p<udp_server_socket> server) : remote_client_socket(tl), udp_server(server) { }

		~udp_remote_client_socket() {
			close();
		}

		void set(const ipv4_addr &addr, int port, int serv_port, const buffer &data) {
			client = addr;
			client_port = port;

			server_port = serv_port;
			
			incoming_data.push(data.copy());

			down.cast<udp>()->listen(server_port, udp::port_listener::method(this, &udp_remote_client_socket::receive)); 
		}

		ipv4_addr get_addr() const {
			return client;
		}

		void write(const buffer &data) {
			down.cast<udp>()->send(client, client_port, server_port, data);
		}

		void read(buffer &data) {
			buffer buf = incoming_data.pop();
			data.copy_data(buf);
		}

		void close() {
			if (udp_server.valid()) {
				udp_server->disconnect(this);
				down.cast<udp>()->remove_listener(server_port, udp::port_listener::method(this, &udp_remote_client_socket::receive));
				udp_server = p<udp_server_socket>::invalid;
			}
		}
	};
}
