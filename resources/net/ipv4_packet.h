#ifndef _IPV4_PACKET_H_
#define _IPV4_PACKET_H_

#include "libs/array.h"

#include "arch/low/general.h"
#include "packet_builder.h"

namespace net {
	class ipv4_packet : public packet_builder {
	private:
		enum {
			default_version_size = 0x45,
			default_service = 0,
			default_ttl = 64
		};

		struct packet {
			u8 version_size;
			u8 service;
			u16 length;
			u16 id;
			u16 flags_offset;
			u8 ttl;
			u8 prot;
			u16 checksum;
			u32 src;
			u32 dst;
		} __attribute__((packed));

		mutable p<packet> pkg;

		buffer data;
	public:
		ipv4_packet() : pkg(new packet) { }
		~ipv4_packet() {
			pkg.dispose();
		}

		buffer generate() const {
			/* Set default values */
			pkg->version_size = default_version_size;
			pkg->service = default_service;
			pkg->ttl = default_ttl;
			pkg->flags_offset = to_le16(0x40);

			/* Count packet length */
			pkg->length = to_be16(data.get_size() + sizeof(packet));
			
			/* Count checksum */
			array<u16> words = array<u16>::from_buffer(buffer::to_mem(pkg));
			u32 sum = 0;
			for (unsigned int i = 0; i < sizeof(packet) / 2; i++)
				sum += words[i];

			sum = (sum & 0xffff) + (sum >> 16);
			sum += (sum >> 16);

			pkg->checksum = to_le16(~sum);

			buffer buf = buffer::to_mem(pkg);
			buf += data;

			return buf;
		}

		void add_data(const buffer &x) {
			data = x;
		}

		enum protocol {
			p_icmp = 1,
			p_tcp = 6,
			p_udp = 17
		};

		void set_ptype(protocol pt) {
			pkg->prot = pt;
		}

		void set_source_paddr(const ipv4_addr &addr) {
			pkg->src = addr.to_be();
		}

		void set_target_paddr(const ipv4_addr &addr) {
			pkg->dst = addr.to_be();
		}

		ipv4_packet(const buffer &x) : pkg(x.cast<packet>()), data(x) { }

		buffer get_data() const {
			return data.get_next(from_be16(pkg->length)).cut_first(sizeof(packet));
		}

		protocol get_ptype() const {
			return (protocol)pkg->prot;
		}

		ipv4_addr get_source_paddr() const {
			return ipv4_addr::from_be(pkg->src);
		}

		ipv4_addr get_target_paddr() const {
			return ipv4_addr::from_be(pkg->dst);
		}
	};
}

#endif
