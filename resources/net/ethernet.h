#ifndef _ETHERNET_H_
#define _ETHERNET_H_

#include "mac_addr.h"
#include "link_layer.h"

namespace net {
	class ethernet : public link_layer {
	protected:
		void receive_data(const buffer &x);

	public:
		virtual void set_nic(p<nic>);
		virtual void send_data(const mac_addr &addr, u16 prot, const buffer &x) ;
	};
}

#endif
