#include "ethernet.h"

using namespace net;

struct ethernet2_frame {
	u32 dst0;
	u16 dst1;
	u32 src0;
	u16 src1;
	u16 type;
} __attribute__((packed));

void ethernet::set_nic(p<nic> x) {
	link_layer::set_nic(x);

	x->set_receiver(delegate<void, const buffer &>::method(this, &ethernet::receive_data));
}

void ethernet::send_data(const mac_addr &addr, u16 prot, const buffer &x) {
	p<ethernet2_frame> frame = new ethernet2_frame;

	frame->dst0 = addr.first_be32();
	frame->dst1 = addr.last_be16();

	frame->src0 = get_haddr().first_be32();
	frame->src1 = get_haddr().last_be16();

	frame->type = to_be16(prot);

	buffer buf = buffer::to_mem(frame);
	buf += x;

	down->send_data(buf);

//	frame.dispose();
}

void ethernet::receive_data(const buffer &x) {
	p<ethernet2_frame> frm = x.cast<ethernet2_frame>();
	mac_addr addr = mac_addr::from_be(frm->dst0, frm->dst1);
	mac_addr broadcast = mac_addr::from_be(0xffffffff, 0xffff);

	if (get_haddr() != addr && addr != broadcast)
		return;

	int type = from_be16(frm->type);

	for (int i = 0; i < type_id.get_count(); i++)
		if (type_id[i] == type)
			handlers[i](buffer(&frm[1], x.get_size() - sizeof(ethernet2_frame)));
}
