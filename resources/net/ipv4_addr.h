#ifndef _IPV4_ADDR_H_
#define _IPV4_ADDR_H_

#include "address.h"
#include "arch/low/general.h"

namespace net {
	class ipv4_addr : public address {
	private:
		u32 addr;

	public:
		ipv4_addr();
		ipv4_addr(const ipv4_addr &x);
		ipv4_addr &operator=(const ipv4_addr &x);

		//static ipv4_addr from_string(const string&);

		static ipv4_addr from_be(u32);
		static ipv4_addr from_le(u32);

		u32 to_be() const;
		u32 to_le() const;

		ipv4_addr network(const ipv4_addr &mask) const;

		static int in_bytes() {
			return 4;
		}

		virtual bool operator==(const ipv4_addr &x) const;
		virtual bool operator!=(const ipv4_addr &x) const {
			return !operator==(x);
		}
	};
}

#endif
