#ifndef _PACKET_BUILDER_
#define _PACKET_BUILDER_

namespace net {
	class packet_builder {
	public:
		virtual ~packet_builder() { }

		virtual buffer generate() const = 0;
		virtual void add_data(const buffer&) = 0;
	};
}

#endif
