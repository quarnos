#include "arp.h"
#include "mac_addr.h"
#include "arp_packet.h"

using namespace net;

void poll(volatile bool *value) {
	while (!*value) asm volatile("nop");
}

mac_addr arp::get_haddr(const ipv4_addr &ipaddr) {
	p<arp_table::arp_record> rec = table.get(ipaddr);
	if (rec.valid())
		return rec->haddr;
	
	p<arp_packet> pkg = new arp_packet;
	pkg->set_ptype(arp_packet::p_ipv4);
	pkg->set_htype(arp_packet::h_ethernet);

	pkg->set_type(arp_packet::arp_request);

	pkg->set_source_haddr(my_haddr);
	pkg->set_source_paddr(my_paddr);

	pkg->set_target_paddr(ipaddr);

	mac_addr broadcast = mac_addr::from_be(0xffffffff, 0xffff);
	down->send_data(broadcast, et_arp, pkg->generate());

	pkg.dispose();

      	rec = p<arp_table::arp_record>::invalid;

	do {
		poll(&received);
		received = false;

		rec = table.get(ipaddr);
	} while (!rec.valid());

	return rec->haddr;
}

void arp::receive_packet(p<arp_packet> pkg) {
	if (pkg->get_type() == arp_packet::arp_request) {
		if (pkg->get_target_paddr() != my_paddr)
			return;

		/* Add sender to ARP table */
		arp_table::arp_record rec;
		rec.prot = pkg->get_ptype();
		rec.paddr = pkg->get_source_paddr();
		rec.haddr = pkg->get_source_haddr();

		table.add(rec);

		p<arp_packet> rpl = new arp_packet;

		rpl->set_htype(pkg->get_htype());
		rpl->set_ptype(pkg->get_ptype());

		rpl->set_type(arp_packet::arp_reply);

		rpl->set_source_haddr(my_haddr);
		rpl->set_source_paddr(my_paddr);

		rpl->set_target_haddr(pkg->get_source_haddr());
		rpl->set_target_paddr(pkg->get_source_paddr());

		down->send_data(pkg->get_source_haddr(), et_arp, rpl->generate());

		rpl.dispose();	

	} else if (pkg->get_type() == arp_packet::arp_reply) {
		arp_table::arp_record rec;
		rec.prot = pkg->get_ptype();
		rec.paddr = pkg->get_source_paddr();
		rec.haddr = pkg->get_source_haddr();

		table.add(rec);
		received = true;
	}
}

void arp::receive_data(const buffer &x) {
	receive_packet(new arp_packet(x));
}
