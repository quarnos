#ifndef _TCP_H_
#define _TCP_H_

#include "transport_layer.h"
#include "ipv4_addr.h"

namespace net {
	class tcp : public transport_layer {
	public:
		enum tcp_flags {
			tcp_none = 0,
			tcp_fin = 1,
			tcp_syn = 2,
			tcp_rst = 4,
			tcp_psh = 8,
			tcp_ack = 0x10,
			tcp_urg = 0x20,
			tcp_ecn = 0x40,
			tcp_cwr = 0x80
		};

		typedef delegate<void, const ipv4_addr &, u16, const buffer &, tcp_flags, int, int> port_listener;

	private:
		struct pseudo_header {
			u32 source;
			u32 destination;
			u8 zero;
			u8 protocol;
			u16 length;
		} __attribute__((packed));

		struct tcp_packet {
			u16 sender_port;
			u16 receiver_port;
			u32 seq;
			u32 ack;
			u8 header_size;
			u8 flags;
			u16 window_size;
			u16 checksum;
			u16 piority;
		} __attribute__((packed));

		list<int> ports;
		list<port_listener > listeners;

		int last_port;

		void receive(const ipv4_addr &, const buffer&);

	public:
		tcp() : last_port(49152) { }

		void set_internet_layer(p<internet_layer> x) {
			down = x;
			down->listen(6, delegate<void, const ipv4_addr&, const buffer&>::method(this, &tcp::receive));
		}

		void send(const ipv4_addr&, u16, u16, const buffer &, tcp_flags, int, int);
		void listen(u16, port_listener);
		void remove_listener(u16, port_listener);

		int acquire_port() {
			return last_port++;
		}

		void release_port(int) { }

		p<client_socket> create_client();
		p<server_socket> create_server(int port);
	};
}

#endif
