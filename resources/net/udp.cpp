/* Based on RFC768 */

#include "udp.h"
#include "ipv4.h"
#include "udp_client_socket.h"
#include "udp_server_socket.h"

#include "arch/low/general.h"

#include "libs/array.h"
#include "udp_packet.h"

using namespace net;

void udp::send(const ipv4_addr &addr, u16 dst_port, u16 src_port, const buffer &data) {
	p<udp_packet> pkg = new udp_packet;

	pkg->set_source_port(src_port);
	pkg->set_source_paddr(down.cast<ipv4>()->get_my_ip());

	pkg->set_target_port(dst_port);
	pkg->set_target_paddr(addr);

	pkg->add_data(data);

	down->send_data(addr, 17, pkg->generate());
}

void udp::receive(const ipv4_addr &sender, const buffer &data) {
	p<udp_packet> pkg = new udp_packet(data);

	for (int i = 0; i < ports.get_count(); i++)
		if (ports[i] == pkg->get_target_port())
			listeners[i](sender, pkg->get_source_port(), pkg->get_data());
}

void udp::listen(u16 port, delegate<void, const ipv4_addr &, u16, const buffer &> listener) {
	for (int i = 0; i < ports.get_count();) {
		if (ports[i] == port) {
			ports.remove(i);
			listeners.remove(i);
		} else
			i++;
	}

	ports.add(port);
	listeners.add(listener);
}

void udp::remove_listener(u16 port, port_listener listener) {}

p<client_socket> udp::create_client() {
	return (client_socket*)new udp_client_socket(this);
}

p<server_socket> udp::create_server(int port) {
	return (server_socket*)new udp_server_socket(this, port);
}
