#ifndef _CLIENT_SOCKET_H_
#define _CLIENT_SOCKET_H_

#include "ipv4_addr.h"

#include "libs/buffer.h"
#include "libs/pointer.h"
#include "libs/stream.h"

namespace net {
	class transport_layer;

	class client_socket : public resources::stream {
	protected:
		p<transport_layer> down;

	public:
		client_socket(p<transport_layer> tl) {
			down = tl;
		}

		virtual void connect(const ipv4_addr&, int) = 0;
		virtual void close() = 0;
	};

	class remote_client_socket : public client_socket {
	public:
		remote_client_socket(p<transport_layer> tl) : client_socket(tl) { }

		void connect(const ipv4_addr&,int) { }

		virtual void set(const ipv4_addr&, int, int, const buffer&) = 0;
		virtual ipv4_addr get_addr() const = 0;
	};
}

#endif
