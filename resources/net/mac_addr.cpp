#include "mac_addr.h"

#include "libs/string.h"

using namespace net;

mac_addr::mac_addr() {}

mac_addr::mac_addr(const mac_addr &x) {
	memcpy(addr, x.addr, 6);
}

mac_addr &mac_addr::operator=(const mac_addr &x) {
	memcpy(addr, x.addr, 6);

	return *this;
}

mac_addr mac_addr::from_be(u32 x, u16 y) {
	mac_addr a;
	((u16*)a.addr)[0] = from_be16(y);
	*(u32*)((u32)a.addr + 2) = from_be32(x);
	return a;
}

mac_addr mac_addr::from_be_r(u16 x, u32 y) {
	mac_addr a;
	((u32*)a.addr)[0] = from_be32(y);
	((u16*)a.addr)[2] = from_be16(x);
	return a;
}

u32 mac_addr::first_be32() const {
	return from_be32(*(u32*)((u32)addr + 2));
}

u16 mac_addr::first_be16() const {
	return from_be16(*(u16*)((u32)addr + 4));
}

u32 mac_addr::last_be32() const {
	return from_be32(*(u32*)addr);
}

u16 mac_addr::last_be16() const {
	return from_be16(*(u16*)addr);
}

bool mac_addr::operator==(mac_addr &x) {
	return first_be32() == x.first_be32() && x.last_be16() == last_be16();
}
