#include "udp.h"
#include "udp_client_socket.h"
#include "udp_remote_client_socket.h"
#include "udp_server_socket.h"

using namespace net;

void udp_server_socket::receive(const ipv4_addr &addr, u16 sender_port, const buffer &data) {
	for (int i = 0; i < clients.get_count(); i++)
		if (clients[i]->get_addr() == addr)
			return;

	p<remote_client_socket> client = (remote_client_socket*)new udp_remote_client_socket(down, this);
	client->set(addr, sender_port, server_port, data);
	waiting_clients.push(client);
}

udp_server_socket::udp_server_socket(p<transport_layer> tl, int port) : server_socket(tl), server_port(port) { }

p<remote_client_socket> udp_server_socket::accept() {
	down.cast<udp>()->listen(server_port, udp::port_listener::method(this, &udp_server_socket::receive));
	p<client_socket> client = waiting_clients.pop();
	clients.add(client);
	down.cast<udp>()->remove_listener(server_port, udp::port_listener::method(this, &udp_server_socket::receive));

	return client;
}

void udp_server_socket::disconnect(p<remote_client_socket> connection) {
	for (int i = 0; i < clients.get_count();)
		if (clients[i] == connection)
			clients.remove(i);
		else
			i++;
}
