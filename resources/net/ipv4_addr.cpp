#include "ipv4_addr.h"

using namespace net;

ipv4_addr::ipv4_addr() : addr(0) { }

ipv4_addr::ipv4_addr(const ipv4_addr &x) : addr(x.addr) { }

ipv4_addr &ipv4_addr::operator=(const ipv4_addr &x) {
	addr = x.addr;
	return *this;
}

ipv4_addr ipv4_addr::from_be(u32 x) {
	ipv4_addr ar;
	ar.addr = from_be32(x);
	return ar;
}

ipv4_addr ipv4_addr::from_le(u32 x) {
	ipv4_addr ar;
	ar.addr = from_le32(x);
	return ar;
}

u32 ipv4_addr::to_be() const {
	return to_be32(addr);
}

u32 ipv4_addr::to_le() const {
	return to_le32(addr);
}

ipv4_addr ipv4_addr::network(const ipv4_addr &mask) const {
	ipv4_addr network;
	network.addr = addr & mask.addr;
	return network;
}

bool ipv4_addr::operator==(const ipv4_addr &x) const {
	return addr == x.addr;
}
