#include "link_layer.h"

using namespace net;

void link_layer::set_nic(p<nic> x) {
	down = x;
}

mac_addr link_layer::get_haddr() {
	return down->get_haddr();
}

void link_layer::add_handler(int id, delegate<void, const buffer &> handler) {
	type_id.add(id);
	handlers.add(handler);
}
