#ifndef _SERVER_SOCKET_H_
#define _SERVER_SOCKET_H_

namespace net {
	class server_socket {
	protected:
		p<transport_layer> down;

	public:
		server_socket(p<transport_layer> tl) {
			down = tl;
		}

		virtual p<remote_client_socket> accept() = 0;
		virtual void disconnect(p<remote_client_socket>) = 0;
	};
}

#endif
