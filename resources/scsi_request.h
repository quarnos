#include "arch/low/general.h"

#include "libs/buffer.h"
#include "libs/pointer.h"

namespace resources {
	class scsi_request /* : public block_io_request */ {
	private:
		struct command_6 {
			u8 operation;
			u8 flags;
			u16 lba;
			u8 transfer_length;
			u8 control;
		} __attribute__((packed));

		struct command_10 {
			u8 operation;
			u8 flags;
			u32 lba;
			u8 reserved;
			u16 transfer_length;
			u8 control;
		} __attribute__((packed));

		struct command_12 {
			u8 operation;
			u8 flags;
			u32 lba;
			u32 transfer_length;
			u8 reserved;
			u8 control;
		} __attribute__((packed));

		p<command_10> cmd;
	public:
		scsi_request() : cmd(new command_10) { }
		~scsi_request() {
			cmd.dispose();
		}

		void set_address(int addr) {
			cmd->lba = addr;
		}

		void set_size(int size) {
			cmd->transfer_length = size;
		}

		void read() {
			cmd->control = 0;
			cmd->reserved = 0;
			cmd->flags = 0;

			cmd->operation = 0x28;
		}

		buffer generate() const {
			return buffer::to_mem(cmd);
		}
	};
}
