/* Quarn OS
 *
 * FAT filesystem
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "fat.h"

#include "manes/manec.h"

using namespace manes;
using namespace resources;

fat::fat() : mounted(false) {}

bool fat::mount(p<block> _dep) {
	dep = _dep;

	/* Load MBR */
	mbr = new unsigned char[512];
	dep->read_block(0, 1, (void*)mbr);

	/* Check if filesystem is FAT12 */
	if (strncmp((char*)&(mbr[mbr_fsid]),"FAT12",5)) {
		debug("fat: fs is not fat12");
		return false;
	}
	if (mbr[mbr_dev_desc] != 0xF0) {
		debug("fat: fs is not fat12");
		return false;
	}

	/* Load FAT */
	main_fat = new unsigned char[(unsigned short)mbr[mbr_sectors_per_fat] << 9];
	dep->read_block(mbr[mbr_reserved_sectors], ((unsigned short)mbr[mbr_sectors_per_fat]), (void*)main_fat);

	/* Load root dir */
	root_dir = new dir_entry[(unsigned short)mbr[mbr_root_entries] * 32];
	dep->read_block(mbr[mbr_fat_count] * mbr[mbr_sectors_per_fat] + mbr[mbr_reserved_sectors], ((unsigned short)mbr[mbr_root_entries])* 32 / 512, (void*)root_dir);

	mounted = true;

	return true;
}
typedef unsigned int cpu_word;
void fat::load_file(const string &filename, buffer &addr) {
	assert("fat: filesystem is not mounted", !mounted);

	int index = find_file(filename);

	unsigned int cluster = root_dir[index].cluster;
	int count = 0;
	
	while (cluster < 0xFF0 && cluster > 1) {
		index = ((unsigned char)mbr[mbr_fat_count])*((unsigned short)mbr[mbr_sectors_per_fat])+((unsigned short)mbr[mbr_reserved_sectors])+((unsigned short)mbr[mbr_root_entries])*32/512;

		index += cluster - 2;

		dep->read_block(index, 1, (void*)((cpu_word)addr.get_address() + count));

		cluster = get_cluster_val(cluster);	

		count += 512;
	}
}

file::file_meta fat::load_meta(const string &filename) {
	assert("fat: filesystem is not mounted", !mounted);

	int index = find_file(filename);

	file::file_meta meta;
	meta.creation_d = date(root_dir[index].date & 0x1f, root_dir[index].date & 0x1e0 >> 5, (root_dir[index].date & 0xf800 >> 9) + 1980);
	meta.creation_t = time(root_dir[index].time & 0x1f, root_dir[index].time & 0x7e0 >> 5, root_dir[index].time & 0xf800 >> 0xb);

	if ((root_dir[index].attr & fat_hidden) == fat_hidden)	
		meta.attributes = file::attr_hidden;
	else
		meta.attributes = (file::attrib)0;

	meta.directory = (root_dir[index].attr & fat_directory) == fat_directory;

	if ((root_dir[index].attr & fat_read_only) == fat_read_only)
		meta.access = prvl(0, 555);
	else
		meta.access = prvl(0, 777);

	meta.size = root_dir[index].size;

	return meta;
}

short fat::get_cluster_val(int n) {
	int cluster = n;
	int index = cluster + (cluster >> 1);
	cluster = *((unsigned short*)((int)main_fat+index));

	if (n % 2 == 0)
		cluster &= 0xFFF;
	else
		cluster >>= 4;

	return cluster;
}

void fat::set_cluster_val(int n, int val) {
	int cluster = n;
	int index = cluster + (cluster >> 1);
	

	if (n % 2 == 0) {
		*((unsigned short*)((int)main_fat+index)) &= 0xF000;
		val &= 0xFFF;
		*((unsigned short*)((int)main_fat+index)) |= val;
	} else {
		*((unsigned short*)((int)main_fat+index)) &= 0x000F;
		val <<= 4;
		*((unsigned short*)((int)main_fat+index)) |= val;
	}
}

void fat::filename_to_fatname(const char *filename, char *fatname) {
	bool dot = false;
	for (int i = 0, j = 0; i < 11; j++, i++) {
		if (i > 7 && !dot) {
			dot = true;
			for (; filename[j] != '.' && filename[j]; j++);
			fatname[7] = '1';
			fatname[6] = '~';

		}
		if (filename[j] >= 'a' && filename[j] <= 'z') fatname[i] = filename[j] - 'a' + 'A';
		else if (filename[j] == '.') {
			dot = true;
			for (; i < 8; i++)
				fatname[i] = ' ';
				i--;
		}
		else if (!filename[j])
			for (; i < 11; i++)
				fatname[i] = ' ';
		else fatname[i] = filename[j];
	}
}

void fat::fatname_to_filename(const char *fatname, char *filename) {
	int i, j;
	for (i = 0, j = 0; i < 11; i++,j++) {
		if (i == 8 && fatname[9] != ' ') {
			filename[j] = '.';
			j++;
		}

		if (fatname[i] >= 'A' && fatname[i] <= 'Z')
			filename[j] = fatname[i] - 'A' + 'a';
		else if (fatname[i] != ' ')
			filename[j] = fatname[i];

		if (fatname[i] == ' ') {
			for (; fatname[i]==' ' && i < 11; i++);
			if (i == 11) break;
			/*filename[j] = '.';*/
			i--;
			j--;
		}
	}
	filename[j]=0;
}

bool valid_fatname(const char *fatname) {
	for (int i = 0; i < 11; i++)
		if (fatname[i] < ' ' || fatname[i] > '~')
			return false;
	return true;
}

void fat::save_file(const string &filename, buffer &addr) {
	assert("fat: filesystem is not mounted", !mounted);

	char fatname[11];
	filename_to_fatname(filename, fatname);

	int pos = 0;
	for (int i = 0; i < 50; i++)
		if (root_dir[i].name[0] == '\0' && root_dir[i].name[1] == '\0')
			pos = i;
	assert("fat: no free entry in root dir", pos == 0);

	strcpy((char*)root_dir[pos].name, fatname);
	root_dir[pos].size = addr.get_size();

	int first_cluster = 0, n_cluster, cluster, saved = 0;

	for (first_cluster = 3; get_cluster_val(first_cluster) != 0; first_cluster++);

	n_cluster = first_cluster;
	do {
		cluster = n_cluster;

		int index = ((unsigned char)mbr[mbr_fat_count])*((unsigned short)mbr[mbr_sectors_per_fat])+((unsigned short)mbr[mbr_reserved_sectors])+((unsigned short)mbr[mbr_root_entries])*32/512;
		index += cluster - 2;

		dep->write_block(index, 1, (void*)((int)addr.get_address() + saved));
		
		for (n_cluster = cluster + 1; get_cluster_val(n_cluster) != 0; n_cluster++);
		set_cluster_val(cluster, n_cluster);

		saved += 512;
	} while (saved < addr.get_size());

	set_cluster_val(cluster, 0xFFF);

	root_dir[pos].cluster = first_cluster;

	/* Save FAT */
	dep->write_block(mbr[mbr_reserved_sectors], ((unsigned short)mbr[mbr_sectors_per_fat]), (void*)main_fat);

	/* Save root dir */
	dep->write_block(mbr[mbr_fat_count] * mbr[mbr_sectors_per_fat] + mbr[mbr_reserved_sectors], ((unsigned short)mbr[mbr_root_entries])* 32 / 512, (void*)root_dir);
	
}

list<p<file> > fat::list_directory(const string &dirname) {
	list<p<file> > dir;

	for (int i = 0; i < 50; i++) {
		if (valid_fatname((char*)root_dir[i].name)) {
			char filename[20];
			p<file> fp = new file();
			fatname_to_filename((char*)root_dir[i].name, filename);
			fp->set(filename, (fs*)this);
			dir.add(fp);
		}
	}

	return dir;
}

int fat::find_file(const char *filename) {
	char fatname[11];

	filename_to_fatname(filename, fatname);

	for (int i = 0; i < 50; i++)
		if (!strncmp(fatname, (const char*)root_dir[i].name,11))
			return i;

	debug((string)"fat: file not found: " + filename);
	return -1;
}

void fat::register_type() {
	manec::get()->register_type<fat>("fat", "manec");
}

