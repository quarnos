/* Quarn OS
 *
 * Keyboard and Screen
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _KEYBSCR_H_
#define _KEYBSCR_H_

#include "resources/stream.h"
#include "resources/device.h"

#include "libs/buffer.h"
#include "libs/fifo.h"
#include "libs/colors.h"

namespace resources {
	class x86_keybscr;

	class keybscr : public dev_stream, public color_output {
	private:
		int x, y;
		fifo<char> buf;
		volatile bool enter;

		delegate<void> on_data_received;

	protected:
		virtual void received(const char a);

		virtual void screen_print(char, int, int, int) = 0;
		virtual void scr_scroll() = 0;

	public:
		typedef x86_keybscr arch_keybscr;

		keybscr();

		virtual void read(buffer &);
		virtual void write(const buffer&);

		virtual bool init_device(p<did> iadr) = 0;

		virtual void set_ondatareceived(delegate<void>);

		virtual int get_width() = 0;
		virtual int get_height() = 0;

		static void register_type();
	};
}

#endif
