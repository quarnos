/* Quarn OS
 *
 * USB bus
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _USB_H_
#define _USB_H_

#include "pci.h"
#include "bus.h"
#include "usb_hc.h"
#include "device.h"
#include "did.h"

namespace resources {
	class uhci;

	class usb : public bus {
	private:
		int dev_count;

       		pci_did pciaddr;

	        p<usb_hc> host;
	public:
		struct usb_setup_data {
			u8 bmRequestType;
			u8 bRequest;
			u16 wValue;
			u16 wIndex;
			u16 wLength;
		};
	public:
		enum usb_pid {
			pid_setup = 0x2d,
			pid_in = 0x69,
			pid_out = 0xe1
		};
	private:
		struct device_descriptor {
			u8 bLength;
			u8 bDescriptorType;
			u16 bcdUSB;
			u8 bDeviceClass;
			u8 bDeviceSubClass;
			u8 bDeviceProtocol;
			u8 bMaxPacketSize0;
			u16 idVendor;
			u16 idProduct;
			u16 bcdDevice;
			u8 iManufacturer;
			u8 iProduct;
			u8 iSerialNumber;
			u8 bNumConfigurations;
		} __attribute__((packed));

		struct configuration_descriptor {
			u8 bLength;
			u8 bDescriptorType;
			u16 wTotalLength;
			u8 bNumInterfaces;
			u8 bConfigurationValue;
			u8 iConfiguration;
			u8 bmAttrubutes;
			u8 bMaxPower;
		} __attribute__((packed));
	public:
		struct interface_descriptor {
			u8 bLength;
			u8 bDescriptorType;
			u8 bInterfaceNumber;
			u8 bAlternateSetting;
			u8 bNumEndpoints;
			u8 bInterfaceClass;
			u8 bInterfaceSubClass;
			u8 bInterfaceProtocol;
			u8 iInterface;
		} __attribute__((packed));
	private:
		struct endpoint_descriptor {
			u8 bLength;
			u8 bDescriptorType;
			u8 bEndpointAddress;
			u8 bmAttributes;
			u16 wMaxPacketSize;
			u8 bInterval;
		} __attribute__((packed));

		void device_connected(int);
		string get_dev_string(int, int);
	public:
//		bool initialize();
	//	bool type_added(manes::type_name);
		p<usb_hc> get_host() {
			return host;
		}

		void set_conf(int, int);

		int get_count();

		bool init_bus(p<device>);

		static void register_type();
	};

	class usb_did : public did {
	public:
		usb_did() {}
		virtual ~usb_did() {}

	private:
		int address;
		p<usb::interface_descriptor> intf;

		int control_endp;
		int read_endp;
		int write_endp;

		int configuration;
	public:
	        usb_did(p<bus> b, int addr, p<usb::interface_descriptor> i) : did(b), address(addr), intf(i) { }

		virtual void set_endps(int c, int r, int w) {
			control_endp = c;
			read_endp = r;
			write_endp = w;
		}

		virtual void set_conf(int c) {
			configuration = c;
		}

		virtual int get_class() const {
			return intf->bInterfaceClass;
		}

		virtual int get_subclass() const {
			return intf->bInterfaceSubClass;
		}

		virtual int get_protocol() const {
			return intf->bInterfaceProtocol;
		}
		void accepted() {
			bus_id.cast<usb>()->set_conf(address, configuration);
		}

		virtual void bulk_read(buffer &buf) {
			bus_id.cast<usb>()->get_host()->bulk_transfer(usb::pid_in, buf.get_address(), buf.get_size(), address, read_endp);
		}

		virtual void bulk_write(const buffer &buf) {
			bus_id.cast<usb>()->get_host()->bulk_transfer(usb::pid_out, buf.get_address(), buf.get_size(), address, write_endp);
		}

		virtual void control(const buffer &buf) {
			bus_id.cast<usb>()->get_host()->control_transfer(usb::pid_setup, buf.get_address(), buf.get_size(), 0, address);
		}

		virtual buffer control(const buffer &buf, int size) {
			void *addr = bus_id.cast<usb>()->get_host()->control_transfer(usb::pid_setup, buf.get_address(), buf.get_size(), size, address);
			return buffer(addr, size);
		}
	};
}

#endif
