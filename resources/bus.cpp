/* Quarn OS
 *
 * Bus
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "bus.h"
#include "manes/cds/driver.h"
#include "manes/manec.h"
#include "unknown.h"

using namespace resources;
using namespace manes;
using namespace manes::cds;

bool bus::type_added(const component_name &tname) {
	if (get_count() == 0)
		return false;

	p<driver> drv;
	p<unknown> ukn;
	if ((drv = manec::get()->get<type>((string)"/type," + tname.get_name()).cast<driver>()).valid()) {
		for (int i = 0; i < components.get_count(); i++) {
			if ((ukn = components[i].cast<unknown>()).valid()) {
				if (drv->check_device(ukn->get_did())) {
					components.remove(i);
					p<component> c = new_component(tname);
					c.cast<device>()->init_device(ukn->get_did());
				//	delete ukn;
				}
			}
		}

	}
	return true;
}

p<component> bus::get_component(const component_name &obj) {
	for (unsigned int i = 0, j = 0; (int)i < components.get_count(); i++) {
		if (components[i]->get_type()->is(obj.get_type())) {
			if (j == obj.get_index()) {
				return components[i];
			} else {
				j++;
			}
		}
	}

	return p<component>::invalid;
}

void bus::create_device(p<did> id) {
	p<manes::cds::type> drv = manes::manec::get()->get_driver(id);

	if (!drv.valid()) {
		p<manes::cds::component> c = new_component(manes::cds::component_name::from_path("/type,unknown"));
		c.cast<unknown>()->init_device(id.cast<did>());
	} else {
		id->accepted();
		p<manes::cds::component> c = new_component(drv->get_name());
		p<device> cdev = c.cast<device>();

		if (cdev.valid())
			cdev->init_device(id);
	//	else if (bdev = c->get<bus>())
		//	bdev->init_device(id);
	}
}
