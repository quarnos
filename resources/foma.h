/* Quarn OS
 *
 * Fixed-size Objects Memory Allocator
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _FOMA_H_
#define _FOMA_H_

#include "memm.h"
#include "libs/list.h"
#include "arch/low/general.h"
#include "physmem.h"

namespace resources {
	class foma : public memm {
	private:
		p<physmem> physical;

		unsigned int memory_size;
		void *start_heap;

		unsigned int *first_block[PAGE_SHIFT + 1];

		void *get_page(int index);

		void *allocate(unsigned int fixed_size, unsigned int size);
		void deallocate(void *ptr, unsigned int fixed_size);
	public:
//#if DEBUG_MODE == CONF_YES
		static int used_memory;
//#endif
	public:
		foma();

		bool initialize();

		virtual void *allocate_space(unsigned int size);
		virtual void deallocate_space(void *ptr);

		virtual unsigned int get_size(void *ptr) const;

		static void register_type();
	};
}

#endif
