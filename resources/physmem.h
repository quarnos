/* Quarn OS
 *
 * Physical Memory Manager
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _PHYSMEM_H_
#define _PHYSMEM_H_

#include "memm.h"
#include "libs/vector.h"

namespace resources {
	typedef unsigned int vaddr;
	typedef unsigned int paddr;

	class physmem : public memm {
	public:
		struct region {
			vaddr start;
			unsigned int size;

			vaddr last;

			enum type {
				kernel,
				kern2usr,
				iomem,
				user
			} rtype;
		};

	private:
		paddr last_p;
		unsigned int available_memory;

		vector<region> regions;

		/* Speed up physical mem allocation */
		const int default_region;

	protected:
		void map_page(paddr ppage, vaddr vpage);

	public:
		physmem();

		void *allocate_space(unsigned int);
		vaddr get_page(region::type);

		void deallocate_space(void *page);

		virtual unsigned int get_size(void *ptr) const {
			return 0x1000;
		}

		static void register_type();
	};
}

#endif
