/* Quarn OS
 *
 * Realtek 8139 ethernet card
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "net/nic.h"
#include "pci.h"

namespace resources {
	/* socket */
	class rtl8139 : public net::nic, public device {
	protected:
		enum reg {
			tsd0 = 0x10,
			tsad0 = 0x20,
			rbstart = 0x30,
			cmd = 0x37,
			capr = 0x38,
			imr = 0x3c,
			isr = 0x3e,
			rec_conf = 0x44,
			config1 = 0x52
		};

		enum rcr {
			rcr_apa = 1, /* accept all packets */
			rcr_apm = 2, /* accept physical match */
			rcr_am = 4, /* accept multicast */
			rcr_ab = 8, /* accept broadcast */
			rcr_aer = 0x20 /* accept error packet */
		};

		struct packet {
			u16 status;
			u16 size;
		};

		pci_did id;

		int send_index;
		int received_index;

		volatile char *rec_buffer;

		void init();
		void send_packet(const buffer &x);
		void receive_packet();
		void get_mac_addr(u8 *addr) const;

		void irq_handler();

	public:
		bool init_device(p<did>);
		void send_data(const buffer &x);
		net::mac_addr get_haddr() const;

		static bool check_device(p<did>);
		static void register_type();
	};
}
