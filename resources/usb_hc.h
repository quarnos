/* Quarn OS
 *
 * USB host controller
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _USB_HC_H_
#define _USB_HC_H_

#include "device.h"

namespace resources {
	class usb_hc : public device {
	public:
		virtual void *control_transfer(int, void*, int, int, unsigned char) = 0;

		virtual void bulk_transfer(int, void *, int, int, int) = 0;

		virtual void scan_bus(delegate<void, int>) = 0;

		virtual int get_free_address() = 0;
	};
}

#endif
