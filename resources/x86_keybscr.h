#include "did.h"
#include "keybscr.h"

#include "libs/pointer.h"

namespace resources {
	class x86_keybscr : public keybscr {
	private:
		int last_x, last_y;
		bool shift;

		void keyboard_received();

		static const char scan_to_ascii[];
		static const char scan_to_bigascii[];
	protected:
		virtual void screen_print(char, int, int, int);
		virtual void scr_scroll();

	public:
		virtual bool init_device(p<did> iadr);

		virtual int get_width() {
			return 80;
		}

		virtual int get_height() {
			return 25;
		}
	};
}
