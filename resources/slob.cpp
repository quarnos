/* Simple List of Blocks allocator
 * Based on the SLOB allocator introduced to Linux kernel by M. Mackall.
 *
 * M. Mackall: slob: introduce the SLOB allocator, Linux Kernel Mailing List,
 * 2005, http://lkml.org/lkml/2005/11/1/230
 */

#include "slob.h"
#include "manes/manec.h"

using namespace resources;

slob::slob() {
	physical = manes::manec::get()->get<physmem>("/physmem");
	first_block = (unit*)physical->allocate_space(0x1000);
	first_block->size = 0x1000;
	first_block->next = (unit*)0;
}

void *slob::allocate_space(unsigned int size) {
	unit *before = 0;
	unit *current = first_block;

	if (size < sizeof(unit))
		size = sizeof(unit);

	size += sizeof(unit);
	size = (size / sizeof(unit) + 1) * sizeof(unit);

	if (size >= 0x1000) {
		void *first_page = physical->allocate_space(PAGE_SHIFT + 1);
		for (int i = size - PAGE_SIZE; i > 0; i -= PAGE_SIZE)
			physical->allocate_space(PAGE_SHIFT + 1);
		return first_page;
	}

	while (current->size < size) {
		if (!current->next)  {
			current->next = (unit*)physical->allocate_space(0x1000);
			current->next->size = 0x1000;
			current->next->next = (unit*)0;
		}

		before = current;
		current = current->next;
	} 

	unit *new_unit;

	if (current->size > size) {
		new_unit = (unit*)((int)current + size);

		new_unit->size = current->size - size;
		new_unit->next = current->next;

	} else
		new_unit = current->next;

	if (before)
		before->next = new_unit;
	else
		first_block = new_unit;

	current->size = size;
	current->next = 0;

	return (void*)&current[1];
}

void slob::deallocate_space(void *ptr) {
	if (((unsigned int)ptr & 0xffff) == 0)
		return;

	unit *dealloc = (unit*)((int)ptr - sizeof(unit));

	if (!merge(dealloc)) {
		dealloc->next = first_block;
		first_block = dealloc;
	}
}

unsigned int slob::get_size(void *ptr) const {
	unit *dealloc = (unit*)((int)ptr - sizeof(unit));

	return dealloc->size;
}

bool slob::merge(unit *ptr) {
	unit *current = first_block;
	unit *before = (unit*)0;

	bool action = false;

	do {
		if ((unsigned int)current + current->size == (unsigned int)ptr) {
			current->size += ptr->size;

			ptr = current;
			action = true;

		} else if ((unsigned int)ptr + ptr->size == (unsigned int)current) {
			before->next = ptr;
			ptr->next = current->next;
			ptr->size += current->size;

			current = ptr;
			if (!before)
				first_block = ptr;

			action = true;
		}

		before = current;
		current = current->next;
	} while (current);

	return action;
}


void slob::register_type() {
	manes::manec::get()->register_type<slob>("slob", "allocator");
}
