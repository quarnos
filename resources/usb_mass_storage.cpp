#include "scsi_request.h"
#include "usb.h"
#include "usb_mass_storage.h"

#include "manes/manec.h"


using namespace resources;

int usb_mass_storage_count = 0;

bool usb_mass_storage::init_device(p<did> _id) {
	p<usb_did> uid = _id.cast<usb_did>();

	if (!uid.valid())
		return false;

	id = uid;

	usb::usb_setup_data req;
	req.bmRequestType = 0x21;
	req.bRequest = 0xff;
	req.wValue = 0;
	req.wIndex = 0;
	req.wLength = 0;
	uid->control(buffer::to_mem(req));

	req.bmRequestType = 0xa1;
	req.bRequest = 0xfe;
	req.wLength = 1;
//	buffer luns = uid->control(buffer::to_mem(req), 1);

	p<cbw> cmd = new cbw;
	cmd->dCBWSignature = 0x43425355;
	cmd->dCBWTag = 0xbeef + tag++;
	cmd->dCBWDataTransferLength = 512;
	cmd->bmCBWFlags = 1 << 7;
	cmd->bCBWLUN = 0;
	cmd->bCBWCBLength = 10;
	
	scsi_request scsi_cmd;
	scsi_cmd.set_address(1);
	scsi_cmd.set_size(1);
	scsi_cmd.read();
	
	buffer bufc = buffer::to_mem(cmd);
	bufc += scsi_cmd.generate();
	buffer fill(6);
	bufc += fill;
	bufc.align(0x1000);
	uid->bulk_write(bufc);
	buffer buf(new (0x1000)char[512], 512);
	uid->bulk_read(buf);

	buffer bufs(new (0x1000) char[13], 13);
	uid->bulk_read(bufs);
	asm("cli\nhlt"::"a"(bufs.get_address()), "b"(buf.get_address()), "c"(bufc.get_address()));

	return true;
}


bool usb_mass_storage::check_device(p<did> id) {
	p<usb_did> uid = id.cast<usb_did>();

	if (!uid.valid())
		return false;

	/*
	 * Class: Mass Storage
	 * Subclass: SCSI
	 * Protocol: bulk-only
	 */
	if (uid->get_class() == 8
	    && uid->get_subclass() == 6
	    && uid->get_protocol() == 0x50)
		return true;

	return false;
}

void usb_mass_storage::register_type() {
	manes::manec::get()->register_driver<usb_mass_storage>("usb_mass_storage", "pci", delegate<bool, p<did> >::function(&usb_mass_storage::check_device));
}
