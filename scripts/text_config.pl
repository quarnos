#!/usr/bin/perl

# Quarn OS
#
# Configuration tool
#
# Copyright (C) 2008 Pawel Dziepak
#
# This tool loads file that consist of configuration possibilities and asks
# user questions about each option. The purpose of the tool is the same as
# the nc_config, but this script does not need ncurses installed.
#

# Open configuration file
open (CONF_IN, "scripts/config_options");
@confin = <CONF_IN>;

if ($ARGV[0] !~ /\Adef/) {
	print "Configuration tool for Quarn OS $ARGV[0] (text version)\nCopyright (C) 2008 Pawel Dziepak\n\n";
}

# go through config config and ask questions
$count = 0;
for $line (@confin) {
	if ($line =~ /\Aname (.*)/) {
		$name = $1;
		$count++;
	} elsif ($line =~ /\Alongname (.*)/) {
		$descr =$1;
		$count++;
	} elsif ($line =~ /\Aopts (.*)/) {
		$opts =$1;
		$count++;
	} elsif ($line =~ /\Ahelp (.*)/) {
		$help =$1;
		$count++;
	} else {
		$help = $name = $descr = $opts = "";
		$count = 0;
	}
	$usera = "";
	$def = "";

	if ($opts =~ /(\A|\s)yes/) {
		$usera .= "y/";
	}
	if ($opts =~ /(\A|\s)dyes/) {
		$usera .= "Y/";
		$def = "y";
	}
	if ($opts =~ /(\A|\s)module/) {
		$usera .= "m/";
	}
	if ($opts =~ /(\A|\s)dmodule/) {
		$usera .= "M/";
		$def = "m";
	}
	if ($opts =~ /(\A|\s)no/) {
		$usera .= "n/";
	}
	if ($opts =~ /(\A|\s)dno/) {
		$usera .= "N/";
		$def = "n";
	}
	$usera .= "?";

	while ($count == 4) {
		$count = 0;
		if ($ARGV[0] !~ /\Adef/) {
			print "Do you want to enable \"$descr\" [$usera]: ";
			$ans = <STDIN>;
		}
		if ($ans !~ /(\w|\?)/ || $ARGV[0] =~ /\Adef/) {
			$ans = $def;
		}
		if ($ans =~ /\Ay/ && $opts =~ /yes/) {
			push(@confout, "#define $name\t\t2\n");
		} elsif ($ans =~ /\Am/ && $opts =~ /module/) {
			push(@confout, "#define $name\t\t1\n");
		} elsif ($ans =~ /\An/ && $opts =~ /no/) {
			push(@confout, "#define $name\t\t0\n");
		} elsif ($ans =~ /\A\?/) {
			print $help . "\n";
			$count = 4;
		} else {
			print "Unknown answer. ";
			$count = 4;
		}
	}
}

push(@confout, "\n#define CONF_NO\t\t\t0\n");
push(@confout, "#define CONF_MODULE\t\t1\n");
push(@confout, "#define CONF_YES\t\t2\n");

open (CONF_OUT, ">quarnconf.h");
print CONF_OUT @confout;
