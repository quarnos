/* Quarn OS
 *
 * Configuration tool
 *
 * Copyright (C) 2008 Pawel Dziepak
 *
 * This is a tool that generate configuration file before compilation of Quarn
 * OS. Possible configuration options are read from external file and parsed.
 * Then there are shown menus in which user can adjust his own compilation of
 * the system. This tool uses ncurses, for computers without this library, there
 * is a less user-friendly shell script.
 */

#include <ncurses.h>
#include <menu.h>

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))

char *descrs[] = {
	"Choice 1",
	"Choice 2",
	"Choice 3",
};

char *names[] = {
	"NAME1",
	"NAME2",
	"NAME3",
};

int main() {
	ITEM **config_items;
	MENU *main_menu;
	ITEM *cur_item;
	int n_descrs, i;
	int c;
	
	/* Initialize curses */	
	initscr();
	start_color();
	cbreak();
	noecho();
	keypad(stdscr, TRUE);

	/* Initialize and set colors */
	init_pair(1, COLOR_RED, COLOR_WHITE);
	init_pair(2, COLOR_GREEN, COLOR_WHITE);
	init_pair(3, COLOR_YELLOW, COLOR_WHITE);
	attron(COLOR_PAIR(3));
	
	/* Initialize items */
	n_descrs = ARRAY_SIZE(descrs);
	config_items = (ITEM **)calloc(n_descrs + 1, sizeof(ITEM *));
	for(i = 0; i < n_descrs; ++i)
		config_items[i] = new_item(descrs[i],"");
	config_items[n_descrs] = (ITEM *)NULL;

	/* Create menu */
	main_menu = new_menu((ITEM **)config_items);

	/* Set foreground and background */
	set_menu_fore(main_menu, COLOR_PAIR(1) | A_REVERSE);
	set_menu_back(main_menu, COLOR_PAIR(1));
	set_menu_grey(main_menu, COLOR_PAIR(2));

	/* Show the menu */
	post_menu(main_menu);
	refresh();

	/* Main loop */
	while((c = getch()) != 'q') {
	       switch(c) {
			case KEY_DOWN:
				menu_driver(main_menu, REQ_DOWN_ITEM);
				break;
			case KEY_UP:
				menu_driver(main_menu, REQ_UP_ITEM);
				break;
			case 10: /* Enter */
				cur_item = current_item(main_menu);
				if (item_opts(cur_item) & O_SELECTABLE == O_SELECTABLE) {
					item_opts_off(cur_item, O_SELECTABLE);
				} else {
					item_opts_on(cur_item, O_SELECTABLE);
				}
				break;
		}
	}	

	/* Clean everything */
	unpost_menu(main_menu);
	for(i = 0; i < n_descrs; ++i)
		free_item(config_items[i]);
	free_menu(main_menu);
	endwin();
}
	
