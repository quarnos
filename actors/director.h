/* Quarn OS
 *
 * Director
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _DIRECTOR_H_
#define _DIRECTOR_H_

#include "manes/cds/creator.h"
#include "actor.h"

#include "libs/fifo.h"
#include "libs/pointer.h"
#include "libs/delegate.h"

namespace actors {
	class director : public manes::cds::creator {
	protected:
		fifo<p<actor> > execution_list;
		p<actor> current;

		virtual void execution_flow();

	public:
		bool initialize();
		bool type_added(manes::cds::component_name);

		virtual void start(p<actor>);
		virtual void launch(p<actor>);

		virtual p<actor> get_current() const;

		static void register_type();
	};
}

#endif
