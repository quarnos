/* Quarn OS
 *
 * Roundrobind scheduler
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "roundrobin.h"
#include "services/kernel_state.h"
#include "manes/manec.h"

using namespace actors;

/* Simple round-robin scheduling algorithm */
void roundrobin::on_time_tick() {
	time_left--;

	if (time_left > 0 || execution_list.empty())
		return;

	p<actor> old = current;

	current = execution_list.pop();
	execution_list.push(old);

	time_left = SCHED_QUANT;
	current->execute();
}

void roundrobin::launch(p<actor> process) {
	manes::manec::get()->state()->add_time_tick_call(delegate<void>::method(this, &roundrobin::on_time_tick));
	time_left = SCHED_QUANT;

	director::launch(process);
}

void roundrobin::register_type() {
	manes::manec::get()->register_type<roundrobin>("director", "manec");
}

