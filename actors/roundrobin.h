/* Quarn OS
 *
 * Roundrobin scheduler
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _ROUNDROBIN_H_
#define _ROUNDROBIN_H_

#include "director.h"

#define SCHED_QUANT	10

namespace actors {
	class roundrobin : public director {
	protected:
		volatile int time_left;

		void on_time_tick();

	public:
		void launch(p<actor> process);

		static void register_type();
	};
}

#endif
