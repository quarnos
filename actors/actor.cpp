/* Quarn OS
 *
 * Actor
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "actor.h"
#include "manes/error.h"
#include "manes/manec.h"

#include "arch/low/memory.h"
#include "arch/low/taskswitch.h"

using namespace actors;

actor::actor() : tss_selector(0) {}

void actor::set(delegate<void> _start) {
	assert("actor: unknown entry point", _start.null());

	start = _start;

	tss_selector = arch::create_tss(_start, arch::create_vm());
}

void actor::execute() {
	assert("actor: unknown entry point", start.null());

	arch::jump_tss(tss_selector);
}

void actor::register_type() {
	manes::manec::get()->register_type<actor>("actor", "none");
}

unsigned int actor::get_tss() {
	return tss_selector;
}

