/* Quarn OS
 *
 * Director
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "director.h"
#include "arch/low/taskswitch.h"
#include "arch/low/memory.h"
#include "manes/manec.h"

using namespace actors;

bool director::initialize() {
	delegate<void> del_start;
	arch::create_tss(del_start, arch::create_vm());
	return true;
}

bool director::type_added(manes::cds::component_name) {
	return true;
}

void director::execution_flow() {
	current = execution_list.pop();
	current->execute();

	debug("director: nothing to execute, system is halted");
	while(1) asm("cli\nhlt");
}

void director::start(p<actor> process) {
	execution_list.push(process);
}

void director::launch(p<actor> process) {
	start(process);

	execution_flow();
}

p<actor> director::get_current() const {
	return current;
}

void director::register_type() {
	manes::manec::get()->register_type<director>("director", "manec");
}

