#ifndef _ELF_H_
#define _ELF_H_

#include "binary.h"
#include "loader.h"

extern "C" int get_symbol_size(void *symbol);

namespace modules {
	class elf : public binary {
	private:
	public:
		virtual unsigned int get_size(unsigned int ptr) const {
			return get_symbol_size((void*)ptr);
		}

		virtual string get_name(unsigned int ptr) const {

		}

		virtual void load(const string &prog);

		static void register_type();
	};
}

#endif
