/* Quarn OS
 *
 * ELF files loader and dynamic linker
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

/**
 * @file elf.cpp
 * ELF files loader
 */

#include "loader.h"
#include "manes/manec.h"
#include "elf.h"

#include "resources/fat.h"

#define	KERNEL_ELF		0x200000

/* Types defined in ELF sepcification */
typedef unsigned int Elf32_Addr;
typedef unsigned short Elf32_Half;
typedef unsigned int Elf32_Off;
typedef signed int Elf32_Sword;
typedef unsigned int Elf32_Word;

#define PT_DYNAMIC		2

#define SHT_SYMTAB		2
#define SHT_STRTAB		3
#define SHT_X86_UNWIND		0

#define DT_PLTRELSZ		2
#define DT_STRTAB		5
#define DT_SYMTAB		6
#define DT_REL			17
#define DT_JMPREL		23
#define DT_RELSZ		18
#define DT_RELENT		19

#define STB_GLOBAL		1

/**
 * Program header
 */
typedef struct {
	Elf32_Word p_type;
	Elf32_Off  p_offset;
	Elf32_Addr p_vaddr;
	Elf32_Addr p_paddr;
	Elf32_Word p_filesz;
	Elf32_Word p_memsz;
	Elf32_Word p_flags;
	Elf32_Word p_align;
} Elf32_Phdr;

/**
 * Section header
 */
typedef struct {
	Elf32_Word sh_name;
	Elf32_Word sh_type;
	Elf32_Word sh_flags;
	Elf32_Addr sh_addr;
	Elf32_Off  sh_offset;
	Elf32_Word sh_size;
	Elf32_Word sh_link;
	Elf32_Word sh_info;
	Elf32_Word sh_addralign;
	Elf32_Word sh_entsize;
} Elf32_Shdr;

/**
 * Dynamic table record
 */
typedef struct {
	Elf32_Sword   d_tag;
	union {
		Elf32_Word   d_val;
		Elf32_Addr   d_ptr;
	} d_un;
} Elf32_Dyn;

/**
 * Relocation information structure
 */
typedef struct {
        Elf32_Addr r_offset;
        Elf32_Word r_info;
} Elf32_Rel;

#define R_386_32	1
#define R_386_PC32	2
#define R_386_GLOB_DAT	6
#define R_386_JMP_SLOT	7
#define R_386_RELATIVE	8

/**
 * Dynamic symbol structure
 */
typedef struct {
          Elf32_Word    st_name;
          Elf32_Addr    st_value;
          Elf32_Word    st_size;
          unsigned char st_info;
          unsigned char st_other;
          Elf32_Half    st_shndx;
} Elf32_Sym;

#define ELF32_ST_BIND(i)	((i)>>4)

#define ELF32_R_SYM(i)    ((i)>>8)
#define ELF32_R_TYPE(i)   ((unsigned char)(i))
#define ELF32_R_INFO(s,t) (((s)<<8)+(unsigned char)(t))

struct Elf32_DynLinker {
	void* AT_PHDR;
	void* AT_PHENT;
	void* AT_PHNUM;
	void* AT_ENTRY;
	void* AT_BASE;
};

#define ELF_NIDENT 16
/**
 * ELF program header
 */
typedef struct {
	unsigned char e_ident[ELF_NIDENT];	/**< magic number */
	Elf32_Half e_type;			/**< type of elf file */
	Elf32_Half e_machine;			/**< hardware architecture */
	Elf32_Word e_version;			/**< version of elf */
	Elf32_Addr e_entry;			/**< entry point */
	Elf32_Off  e_phoff;			/**< offset of first program header */
	Elf32_Off  e_shoff;			/**< offset of first section header */
	Elf32_Word e_flags;			/**< flags */
	Elf32_Half e_ehsize;
	Elf32_Half e_phentsize;
	Elf32_Half e_phnum;
	Elf32_Half e_shentsize;
	Elf32_Half e_shnum;
	Elf32_Half e_shstrndx;
} Elf32_Ehdr;

void *get_eh_frame() {
	extern void *_eh_frame;
	void *eh_frame = (void*)&_eh_frame;
	return eh_frame;
}

Elf32_Sym *quarn_symbols() {
	Elf32_Ehdr *ehdr = (Elf32_Ehdr*)KERNEL_ELF;
	Elf32_Shdr *sections = (Elf32_Shdr*)(ehdr->e_shoff + KERNEL_ELF);

	for (short i = 0; i <= ehdr->e_shnum; i++)
		if (sections[i].sh_type == SHT_SYMTAB)
			return (Elf32_Sym*)(sections[i].sh_offset + KERNEL_ELF);

	critical("elf: kernel symbols table not found");
	return (Elf32_Sym*)0;
}

int quarn_symnum() {
	Elf32_Ehdr *ehdr = (Elf32_Ehdr*)KERNEL_ELF;
	Elf32_Shdr *sections = (Elf32_Shdr*)(ehdr->e_shoff + KERNEL_ELF);

	for (short i = 0; i <= ehdr->e_shnum; i++)
		if (sections[i].sh_type == SHT_SYMTAB)
			return sections[i].sh_size / sizeof(Elf32_Sym);

	critical("elf: kernel symbols table not found");
	return 0;
}

char *quarn_strings() {
	Elf32_Ehdr *ehdr = (Elf32_Ehdr*)KERNEL_ELF;
	Elf32_Shdr str = ((Elf32_Shdr*)(ehdr->e_shoff + KERNEL_ELF))[ehdr->e_shstrndx + 2];
	return (char*)(str.sh_offset + KERNEL_ELF);
}

extern "C" int get_symbol_size(void *symbol) {
	int kern_symnum = quarn_symnum();
	Elf32_Sym *kern_sym = quarn_symbols();

	for (int i = 0; i < kern_symnum; i++)
		if (kern_sym[i].st_value == (Elf32_Word)symbol && kern_sym[i].st_size)
			return kern_sym[i].st_size;

	return 0;
}
typedef unsigned int cpu_word;
modules::loader::module_entry load_dynamic_elf(const string &prog) {
	p<resources::file> fp = manes::manec::get()->get<resources::file>((string)"/file," + prog);
	char *buf = reinterpret_cast<char*>(fp->get_buffer().get_address());
	Elf32_Ehdr *header = (Elf32_Ehdr*)buf;
	cpu_word offset = reinterpret_cast<cpu_word>(buf);

	/* RELPTL (relocation table) [0x17] has to be filled with names addresses */
	/* They are stored in DYNSYM (symbol table) [6] */
	/* RELPTL and DYNSYM addresses are in dynamic section */

	Elf32_Phdr *segments = (Elf32_Phdr*)((int)header->e_phoff + offset);

	/* Find dynamic segment in program */
	Elf32_Dyn *dynamic_s = (Elf32_Dyn*)0;
	for (int i = 0; i < header->e_phnum; i++)
		if (segments[i].p_type == PT_DYNAMIC) 
			dynamic_s = (Elf32_Dyn*)(segments[i].p_offset + offset);

	/* Find Quarn symbol table */
	int kern_symnum = quarn_symnum();
	Elf32_Sym *kern_sym = quarn_symbols();
	char *kern_str = quarn_strings();

	/* Find relocation and symbol tables (sections) */
	/* Checking for broken ELF files */
	Elf32_Rel *reldyn_t = 0, *relplt_t = 0;
	int reldyn_size = 0, relplt_size = 0, rel_esize = 0, reldyn_ent = 0, relplt_ent = 0;
	Elf32_Sym *symbol_t = 0;
	char *mod_str = 0;
	for (int i = 0; dynamic_s[i].d_tag; i++) {
		switch (dynamic_s[i].d_tag) {
			case DT_STRTAB : mod_str = (char*)(dynamic_s[i].d_un.d_val + offset); break;
			case DT_SYMTAB : symbol_t = (Elf32_Sym*)(dynamic_s[i].d_un.d_val + offset); break;
			case DT_REL : reldyn_t = (Elf32_Rel*)(dynamic_s[i].d_un.d_val + offset); break;
			case DT_JMPREL : relplt_t = (Elf32_Rel*)(dynamic_s[i].d_un.d_val + offset); break;
			case DT_RELENT : rel_esize = dynamic_s[i].d_un.d_val; break;
			case DT_RELSZ : reldyn_size = dynamic_s[i].d_un.d_val; break;
			case DT_PLTRELSZ : relplt_size = dynamic_s[i].d_un.d_val; break;
		}
	}

	/*
	 * FIXME: code reuse!
	 */

	/* Fill offsets in REL (global variables) */
	reldyn_ent = reldyn_size / rel_esize;
	for (int i = 0; i < reldyn_ent; i++) {
		u32 *record = (u32*)(reldyn_t[i].r_offset + offset);
		if (ELF32_R_TYPE(reldyn_t[i].r_info) == R_386_RELATIVE) {
			*record += offset;
		} else 	if (symbol_t[ELF32_R_SYM(reldyn_t[i].r_info)].st_shndx) {
			switch (ELF32_R_TYPE(reldyn_t[i].r_info)) {
				case R_386_PC32:
					*record += symbol_t[ELF32_R_SYM(reldyn_t[i].r_info)].st_value + offset - (u32)record;
					break;
				case R_386_32:
					*record += symbol_t[ELF32_R_SYM(reldyn_t[i].r_info)].st_value + offset;
					break;
				case R_386_GLOB_DAT:
					*record = symbol_t[ELF32_R_SYM(reldyn_t[i].r_info)].st_value + offset;
					break;
				case R_386_RELATIVE:
					*record += offset;
					break;
				default:
					debug("elf: unknown relocation type");
			}
		} else {
			for (int j = 0; j < kern_symnum; j++) {
				if (!strcmp(&mod_str[symbol_t[ELF32_R_SYM(reldyn_t[i].r_info)].st_name], &kern_str[kern_sym[j].st_name])
				    && mod_str[symbol_t[ELF32_R_SYM(reldyn_t[i].r_info)].st_name]) {
					switch (ELF32_R_TYPE(reldyn_t[i].r_info)) {
						case R_386_PC32:
							*record += kern_sym[j].st_value - (u32)record;
							break;
						case R_386_32:
							*record += kern_sym[j].st_value;
							break;
						case R_386_GLOB_DAT:
							*record = kern_sym[j].st_value;
							break;
						default:
							debug("elf: unknown relocation type");
					}
				}
			}
		}
	}

	/* Fill offsets in RELPLT (procedures and functions) */
	relplt_ent = relplt_size / rel_esize;
	for (int i = 0; i < relplt_ent; i++) {
		u32 *record = (u32*)(relplt_t[i].r_offset + offset);
		if (symbol_t[ELF32_R_SYM(relplt_t[i].r_info)].st_shndx) {
				if (record != 0)
					__asm__("cli\nhlt"::"a"(record), "b"(symbol_t[ELF32_R_SYM(relplt_t[i].r_info)].st_value));
			switch (ELF32_R_TYPE(relplt_t[i].r_info)) {
				case R_386_PC32:
					*record += symbol_t[ELF32_R_SYM(relplt_t[i].r_info)].st_value + offset - (u32)record;
					break;
				case R_386_32:
					*record += symbol_t[ELF32_R_SYM(relplt_t[i].r_info)].st_value + offset;
					break;
				case R_386_JMP_SLOT:
					*record = symbol_t[ELF32_R_SYM(relplt_t[i].r_info)].st_value + offset;
					break;
				default:
					debug("elf: unknown relocation type");
			}
		} else {
			for (int j = 0; j < kern_symnum; j++) {
				if (!strcmp(&mod_str[symbol_t[ELF32_R_SYM(relplt_t[i].r_info)].st_name], &kern_str[kern_sym[j].st_name])
				    && mod_str[symbol_t[ELF32_R_SYM(relplt_t[i].r_info)].st_name]) {
					u32 *record = (u32*)(relplt_t[i].r_offset + offset);
					switch (ELF32_R_TYPE(relplt_t[i].r_info)) {
						case R_386_PC32:
							*record += kern_sym[j].st_value - (u32)record;
							break;
						case R_386_32:
							*record += kern_sym[j].st_value;
							break;
						case R_386_JMP_SLOT:
							*record = kern_sym[j].st_value;
							break;
						default:
							debug("elf: unknown relocation type");
					}
				}
		       	}
		}
	}

	/* Return entry point */
	return (modules::loader::module_entry)(header->e_entry + offset);
}

void modules::elf::load(const string &prog) {
	enter_module = load_dynamic_elf(prog);
}


void modules::elf::register_type() {
	manes::manec::get()->register_type<modules::elf>("elf", "module");
}

