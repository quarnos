/* Quarn OS
 *
 * Loader
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _LOADER_H_
#define _LOADER_H_

#include "manes/cds/creator.h"
#include "manes/manec.h"

#include "libs/list.h"
#include "libs/delegate.h"
#include "libs/pointer.h"

namespace modules {
	class loader : public manes::cds::creator {
	public:
		typedef void (*module_entry)(/*manes::component**/);

		virtual p<manes::cds::component> get_component(const manes::cds::component_name&);

 		static void register_type();
	};
}

#endif
