#ifndef _BINARY_H_
#define _BINARY_H_

#include "module.h"

namespace modules {
	class binary : public module {
	public:
		virtual unsigned int get_size(unsigned int ptr) const = 0;
		virtual string get_name(unsigned int ptr) const = 0;

		virtual void load(const string &prog) = 0;
	};
}

#endif
