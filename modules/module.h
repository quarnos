/* Quarn OS
 *
 * Module
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _MODULE_H_
#define _MODULE_H_

#include "manes/manec.h"

namespace modules {
	class module : public manes::cds::component {
	public:
		typedef enum {
			module_kernel_space,
			module_user_space,
			module_network
		} module_type;
	private:
		string *mname;

		module_type mtype;
		struct {
			void *code_start;
			int code_size;

			void *data_start;
			int data_size;

			void *bss_start;
			int bss_size;
		} kernel_space;

	protected:
		void (*enter_module)(/*manes::component**/);

	public:
		virtual void load(const string &prog) = 0;
		void place(module_type);

		void enter();

		delegate<void> get_delegate();

		static void register_type();
	};
}

#endif
