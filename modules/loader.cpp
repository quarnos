/* Quarn OS
 *
 * Loader
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "loader.h"
#include "module.h"
#include "manes/manec.h"

using namespace modules;

/* Load module */

p<manes::cds::component> loader::get_component(const manes::cds::component_name &mname) {
	if (mname.get_type().get_name() != string("module"))
		return p<manes::cds::component>::invalid;

	p<manes::cds::component> module_comp = new_component(manes::cds::component_name::from_path("/type,elf"));
	p<module> mp = module_comp.cast<module>();

	mp->load(mname.get_name());
	
	return module_comp;
}

void loader::register_type() {
	manes::manec::get()->register_type<loader>("loader", "manec");
}

