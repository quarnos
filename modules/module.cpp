/* Quarn OS
 *
 * Module
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "module.h"
#include "libs/string.h"
#include "arch/low/taskswitch.h"

#include "elf.h"

using namespace modules;

void module::place(module_type mt) {
	mtype = mt;
}

void module::enter() {
	if (mtype == module_kernel_space)
		enter_module(/*manes::manec::get().cast<manes::component>()*/);
	else if (mtype == module_user_space)
		arch::jump_user(enter_module);
	else
		critical("unknown or unsupported module type");
}

delegate<void> module::get_delegate() {
	return delegate<void>::method(this, &module::enter);
}

void module::register_type() {
	manes::manec::get()->register_type<modules::elf>("module", "none");
}

