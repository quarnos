/* Quarn OS
 *
 * Startup code for modules
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "manes/manec.h"
#include "services/logger.h"
#include "resources/memm.h"
#include "services/kernel_state.h"

using namespace manes;

extern p<resources::memm> allocator;

extern "C" void module_init();

extern "C" void start_module(/*component *mgr,*/ ) {
	manec::connect_manes();

	p<services::logger> log = manec::get()->get<services::logger>("/tty_logger");
	log->print("Module successfully loaded.\n",services::logger::log_critical);

	module_init();
}

void *operator new(unsigned int size) {
	return allocator->allocate_space(size);
}

void *operator new[](unsigned int size) {
	return operator new(size);
}

void operator delete(void *ptr) {
	allocator->deallocate_space(ptr);
}

void operator delete[](void *ptr) {
	operator delete(ptr);
}


/* Code that should be executed when program exit, which won't ever 
 * happen to Manes
 */
void *__dso_handle __attribute__((weak));
extern "C" void __cxa_atexit(void (*)(void *), void *, void *) {
	/* manes::error er("runtime: attempt to terminate kernel process");
	er.critical(); */
}

namespace __cxxabiv1 {
	/* Function shows error message when a call to pure virtual function 
	 * has happened */
	extern "C" void __cxa_pure_virtual(void) {
		critical("runtime: call to pure virtual function");
		while(1);
	}
}
