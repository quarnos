/* Quarn OS
 *
 * String
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _STRING_H_
#define _STRING_H_

#include "libs/delegate.h"
#include "buffer.h"
/*#include "manes/obj_val.h"
#include "manes/object_stream.h"*/
#include "list.h"

class string /*: public manes::ods::obj_val */{
protected:
	char *data;
	char error;
	int used;

	void save(const char*);
public:
	string(const char*);
	string(const string &);
	
	string(const unsigned int);
	string(const unsigned int, bool);

	string(int);

	string();
	~string();

	int length() const;
	void reverse();

	char &operator[](const int);
	const char &operator[](const int) const;

	bool operator==(const char*) const;
	bool operator==(const string&) const;
	bool operator!=(const string&x) const {
		return !operator==(x);
	}

	operator const char*() const;
	const char *to_ascii() const;

	static string from_utf16(char*);

	bool null() const;

	string operator+(const int) const;
	string operator+(const string&) const;

	void operator+=(const string&);

	void operator=(const string&);
	static bool is_digit(char);

	list<string> split(char) const;

	buffer to_mem() const;

//	void serialize(manes::ods::object_stream &ostr) { }
	//void deserialize(manes::ods::object_stream &ostr) { }
};


/* C-style functions */
int strlen(const char *);
int strcmp(const char *, const char*);
int strncmp(const char *, const char*, int);
char *strcpy(char *, const char *);
char *strncpy(char *, const char *, int);
char *strcat(char *, const char *);

void *memcpy(void *, const void *, int);
void *memset(void *, int, int);

#endif
