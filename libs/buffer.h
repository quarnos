/* Quarn OS
 *
 * Buffer
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _BUFFER_H_
#define _BUFFER_H_

#include "pointer.h"
#include "math.h"

void *memcpy(void *,const void*,int);
void *operator new[](unsigned int, int);

class buffer {
private:
	char *address;
	unsigned int size;

public:
	static const buffer empty;

	buffer() : address((char*)0), size(0) { }

	buffer(unsigned int bytes) : address(new char[bytes]), size(bytes) { }

	buffer(void *addr, unsigned int siz) : address((char*)addr), size(siz) { }

	buffer(const buffer &x) : address(x.address), size(x.size) { }

	buffer &operator=(const buffer &x) {
		address = x.address;
		size = x.size;

		return *this;
	}

	operator void*() {
		return address;
	}

	char &operator[](int i) {
		if (i >= 0 && i < size)
			return address[i];
	}

	const char &operator[](int i) const {
		if (i >= 0 && i < size)
			return address[i];
	}

	buffer &operator+=(const buffer &x) {
		if (!x.size)
			return *this;

		if (!size) {
			copy_data(x);
			return *this;
		}

		char *buf = new char[x.size + size];
		memcpy(buf, address, size);
		memcpy((void*)((unsigned int)buf + size), x.address, x.size);

		delete address;
		address = buf;
		size += x.size;

		return *this;
	}

	template<typename T>
	p<T> cast() const {
		if (size >= sizeof(T))
			return p<T>(reinterpret_cast<T*>(address));
	}

	int get_size() const {
		return size;
	}

	void *get_address() const {
		return address;
	}

	buffer cut_first(unsigned int cut_size) const {
		if (cut_size >= size)
			return empty;

		return buffer(address + cut_size, size - cut_size);
	}

	void align(int al) {
		void *naddr = (void*)new (al) char[size];
		memcpy(naddr, (void*)address, size);
		address = (char*)naddr;
	}

	buffer get_next(unsigned int bytes) const {
		bytes = math::min(bytes, size);

		if (!bytes)
			return empty;

		return buffer(address, bytes);
	}

	buffer copy() const {
		buffer buf(new char[size], size);
		memcpy(buf.address, address, size);
		return buf;
	}

	buffer copy_data(const buffer &buf) {
		memcpy(address, buf.address, math::min(size, buf.size));
	}

	template<typename T>
	static buffer to_mem(p<T> addr) {
		return buffer((void*)addr, sizeof(T));
	}

	template<typename T>
	static buffer to_mem(T &addr) {
		return buffer((void*)&addr, sizeof(T));
	}
};

#endif
