#ifndef _COLOR_H_
#define _COLOR_H_

namespace color {
	typedef enum {
		black,
		blue,
		green,
		cyan,
		red,
		magenta,
		brown,
		gray,
		darkgray,
		lightblue,
		lightgreen,
		lightcyan,
		pink,
		lightmagenta,
		yellow,
		white
	} color;
}

class color_output {
protected:
	color::color foreground;
	color::color background;
public:
	color_output() {
		default_color();
	}

	virtual void set_forecolor(color::color c) {
		foreground = c;
	}

	virtual color::color get_forecolor() const {
		return foreground;
	}

	virtual void set_backcolor(color::color c) {
		background = c;
	}

	virtual color::color get_backcolor() const {
		return background;
	}

	virtual void default_color() {
		foreground = color::gray;
		background = color::black;
	}
};

#endif
