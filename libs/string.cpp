/* Quarn OS
 *
 * String
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "arch/low/general.h"

#include "string.h"
#include "buffer.h"

#include "list.h"

const buffer buffer::empty = buffer((void*)0, 0);

#define assert(x,y)

string::string(const char *text) : error('\0') {
	data = new char[strlen(text) + 1];
	strcpy(data, text);
	used = 1;
}

void string::save(const char *text) {
	data = new char[strlen(text) + 1];
	strcpy(data, text);
}

/* Consider copy on write strategy */
string::string(const string &x) : error('\0') {
	assert("string: copying null string", x.null());
	data = new char[x.length() + 1];
	if (x.data)
		strcpy(data, x.data);
}

string::string() : data((char*)0) {}

string::string(int _val) {
	unsigned int val = _val;
	data = new char[20];

	for (int i = 0; i < 20; i++)
		data[i] = 0;

	int i = 0;
	do {
		data[i++] = val % 10 + '0';
	} while ((val /= 10) > 0);

	data[i] = 0;

	reverse();
}

string::string(const unsigned int _val) {
	unsigned int val = _val;
	data = new char[20];

	for (int i = 0; i < 20; i++)
		data[i] = 0;

	int i = 0;
	do {
		data[i++] = val % 10 + '0';
	} while ((val /= 10) > 0);

	data[i] = 0;

	reverse();
}


string::string(const unsigned int _val, bool) {
	const char translate[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
	unsigned int val = _val;

	data = new char[20];

	for (int i = 0; i < 20; i++)
		data[i] = 0;

	int i = 0;
	do {
		data[i++] = translate[val & 0xf];
	} while ((val >>= 4) != 0);

	data[i] = 0;

	reverse();
}


string::~string() {
	if (data)
		delete data;
}

int string::length() const {
	if (!data)
		return 0;

	return strlen(data);
}

void string::reverse() {
	assert("string: operations on a null string", null());
	char temp;
	int len = length();

	for (int i = 0, j = length() - 1; i < j; i++, j--) {
		temp = data[i];
		data[i] = data[j];
		data[j] = temp;
	}

	data[len] = 0;
}

char &string::operator[](const int index) {
	assert("string: operations on a null string", null());
	if (index < length() + 1)
		return data[index];
	else
		return error;
}

const char &string::operator[](const int index) const {
	assert("string: operations on a null string", null());
	if (index < length() + 1)
		return data[index];
	else
		return error;
}

list<string> string::split(char sign) const {
	char *text = new char[strlen(data) + 1];
	strcpy(text, data);
	
	list<string> subs;

	int i = 0, j = 0;
	while (true) {
		for (; text[i] && text[i] != sign; i++);
		if (!text[i]) {
			subs.add(&text[j]);
			break;
		} else {
			text[i] = 0;
			subs.add(&text[j]);
			i++;
			j = i;
		}
	}

	delete []text;

	return subs;
}

bool string::operator==(const char *x) const {
	assert("string: operations on a null string", null());
	assert("string: operations on a null string", x == 0 || strlen(x) == 0);

	if (!strcmp(data, x))
		return true;
	else
		return false;
}

bool string::operator==(const string &x) const {
	assert("string: operations on a null string", null());
//	assert("string: operations on a null string", x.null());

	if (!strcmp(data, x.data))
		return true;
	else
		return false;
}

string string::operator+(const int x) const {
	assert("string: operations on a null string", null());
	string str(x);
	return operator+(str);
}

string string::operator+(const string &x) const {
	assert("string: operations on a null string", null());

	char *temp = new char[length() + x.length() + 1];
	if (data)
		strcpy(temp, data);
	if (x && data)
		strcat(temp, x);
	else if (x)
		strcpy(temp, x);

	temp[length() + x.length()] = 0;

	string val;
	val.data = temp;
	return val;
}

void string::operator+=(const string &x) {
	char *temp = new char[length() + x.length() + 1];

	if (data)
		strcpy(temp, data);
	if (x && data)
		strcat(temp, x);
	else if (x)
		strcpy(temp, x);

	temp[length() + x.length()] = 0;

	if (data)
		delete []data;

	data = temp;	
}

string::operator const char*() const {
	assert("string: operations on a null string", null());
	return data;
}

const char *string::to_ascii() const {
	assert("string: operations on a null string", null());
	return data;
}

string string::from_utf16(char *_data) {
	u16 *utf_data = (u16*)_data;
	int size;
	for (size = 0; utf_data[size]; size++);

	u8 *data = new u8[size];
	for (int i = 0; i < size; i++)
		data[i] = utf_data[i];

	return string((char*)data);
}

bool string::null() const {
	return length() == 0;
}

void string::operator=(const string &x) {
	if (data)
		delete data;

	assert("string: copying null string", x.null());
	data = new char[x.length() + 1];

	strcpy(data, x.data);
}

buffer string::to_mem() const {
	return buffer(data, strlen(data) + 1);
}

bool string::is_digit(char x) {
	return (x >= '0' && x <= '9');
}

int strlen(const char *a) {
	int i;
	for (i = 0; a[i]; i++);
	return i;
}

int strcmp(const char *a,const char *b) {
	int i;
	for (i = 0; a[i] && b[i] && a[i] == b[i]; i++);
	if (a[i] != b[i]) return 1;
	return 0;
}

int strncmp(const char *a,const char *b, int n) {
	int i;
	for (i = 0; a[i] && b[i] && a[i] == b[i] && i < n; i++);
	if (a[i] != b[i] && i != n) return 1;
	return 0;
}

char *strcat(char *a, const char *b) {
	return strcpy(&(a[strlen(a)]), b);
}

char *strcpy(char *a, const char *b) {
	return (char*)memcpy((void*)a, (const void *)b, strlen(b) + 1);
}

char *strncpy(char *a, const char *b, int n) {
	return (char*)memcpy((void*)a, (const void *)b, n);
}

void *memcpy(void * a, const void * b, int count) {
	for (int i = 0; i < count; i++)
		((char *)a)[i] = ((const char *)b)[i];
	return a;
}

void *memset(void *a, int sign, int count) {
	for (int i = 0; i < count; i++)
		((unsigned char *)a)[i] = (unsigned char)sign;
	return a;
}

