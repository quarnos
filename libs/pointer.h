/* Quarn OS
 *
 * Pointer
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _POINTER_H_
#define _POINTER_H_

#include "manes/exception.h"

template<typename T>
class p {
private:
	T *pointer;

	/* Pointers can not be placed on a heap */
//	static void *operator new(unsigned int);
//	static void operator delete(void *);
public:
	static const p<T> invalid;

	p() : pointer(0) {}

	p(T *ptr) : pointer(ptr) { }

	~p() { }

	p(const p<T> &cpy) : pointer(cpy.pointer) { }

	template<typename U>
	p(const p<U> &cpy) : pointer(const_cast<T*>(static_cast<const T*>((const U*)cpy))) { }

	template<typename U>
	p<U> cast() const {
		p<U> pt(dynamic_cast<U*>(pointer));
		return pt;
	}

        p<T> &operator=(const p<T> mve) {
		pointer = mve.pointer;

		return *this;
	}

	bool operator==(unsigned int x) const {
		return (unsigned int)pointer == x;
	}

	bool operator!=(unsigned int x) const {
		return (unsigned int)pointer != x;
	}

	template<typename U>
	U *cast(bool unsafe) const {
		return (U*)pointer;
	}

	operator T*() const {
//		if (valid())
			return pointer;
	//	else
	//		throw new null_pointer_exception("Attempt to perform unsafe cast on invalid pointer.");
	}

	T &operator*() {
		if (valid())
			return *pointer;
		else
			throw new null_pointer_exception("Attempt to dereference invalid pointer.");
	}

	T *operator->() {
		if (valid())
			return pointer;
		else
			throw new null_pointer_exception("Attempt to dereference invalid pointer.");
	}

	const T *operator->() const {
		return pointer;
	}

	bool valid() const {
		return (pointer != (T*)0);
	}

	template<typename U>
	bool is() {
		if (dynamic_cast<U*>(pointer))
			return true;
		return false;
	}

	void dispose() {
		if (!valid())
			return;

		delete pointer;
		pointer = (T*)0;
	}

	template<typename U>
	bool operator==(const p<U> _x) const {
		p<T> x = _x.cast<T>();
		return x.pointer == pointer;
	}

        bool operator==(const p<T> x) const {
		return x.pointer == pointer;
	}
};

template<typename T>
const p<T> p<T>::invalid = 0;

#endif
