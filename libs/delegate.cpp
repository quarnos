/* Quarn OS
 *
 * Delegates
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _DELEGATE_H_
#error This file can not be compiled directly.
#endif

template<typename in, typename out>
union _type_transform {
	in a;
	out b;
};

template<typename in, typename out>
out type_transform(in trans) {
	_type_transform<in, out> t;
	t.a = trans;
	return t.b;
}

template<typename ret_type, typename arg_type1, typename arg_type2,
	typename arg_type3, typename arg_type4, typename arg_type5,
	typename arg_type6, typename arg_type7 >
delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7>::~delegate() {}

template<typename ret_type, typename arg_type1, typename arg_type2,
	typename arg_type3, typename arg_type4, typename arg_type5,
	typename arg_type6, typename arg_type7 >
delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7>::delegate() : owner((A*)0), type(del_uninit) { }

template<typename ret_type, typename arg_type1, typename arg_type2,
	typename arg_type3, typename arg_type4, typename arg_type5,
	typename arg_type6, typename arg_type7 >
delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7>::delegate(const delegate<ret_type, arg_type1, arg_type2, arg_type3,arg_type4, arg_type5, arg_type6, arg_type7> &x) {
	ptr = x.ptr;
	owner = x.owner;
	type = x.type;
}

template<typename ret_type, typename arg_type1, typename arg_type2,
	typename arg_type3, typename arg_type4, typename arg_type5,
	typename arg_type6, typename arg_type7 >
delegate<ret_type, arg_type1, arg_type2, arg_type3,arg_type4, arg_type5, arg_type6, arg_type7> &delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7>::operator=(const delegate<ret_type, arg_type1, arg_type2, arg_type3,arg_type4, arg_type5, arg_type6, arg_type7> &x) {
	ptr = x.ptr;
	owner = x.owner;
	type = x.type;

	return *this;
}

/* Save method pointer for different number of arguments */
template<typename ret_type, typename arg_type1 , typename arg_type2 ,
	typename arg_type3 , typename arg_type4 , typename arg_type5 ,
	typename arg_type6 , typename arg_type7>
template<typename T>
delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7>::method(T *obj, ret_type (T::*_ptr)()) {
	this_delegate del;
	del.type = del_method;
	del.ptr.meth_a0 = type_transform<ret_type (T::*)(),ret_type (A::*)()>(_ptr);
	del.owner = type_transform<T*,A*>(obj);
	return del;
}

template<typename ret_type, typename arg_type1 , typename arg_type2 ,
	typename arg_type3 , typename arg_type4 , typename arg_type5 ,
	typename arg_type6 , typename arg_type7 >
template<typename T>
delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7>::method(T *obj, ret_type (T::*_ptr)(arg_type1)) {
	this_delegate del;
	del.type=del_method;
	del.ptr.meth_a1 = type_transform<ret_type (T::*)(arg_type1),ret_type (A::*)(arg_type1)>(_ptr);
	del.owner = type_transform<T*,A*>(obj);
	return del;
}

template<typename ret_type, typename arg_type1 , typename arg_type2 ,
	typename arg_type3 , typename arg_type4 , typename arg_type5 ,
	typename arg_type6 , typename arg_type7 >
template<typename T>
delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7>::method(T *obj,ret_type (T::*_ptr)(arg_type1, arg_type2)) {
	this_delegate del;
	del.type = del_method;
	del.ptr.meth_a2 = type_transform<ret_type (T::*)(arg_type1, arg_type2),
				ret_type (A::*)(arg_type1, arg_type2)>(_ptr);
	del.owner = type_transform<T*,A*>(obj);
	return del;
}

template<typename ret_type, typename arg_type1 , typename arg_type2 ,
	typename arg_type3 , typename arg_type4 , typename arg_type5 ,
	typename arg_type6 , typename arg_type7 >
template<typename T>
delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7>::method(T *obj,ret_type (T::*_ptr)(arg_type1, arg_type2, arg_type3)) {
	this_delegate del;
	del.type = del_method;
	del.ptr.meth_a3 = type_transform<ret_type (T::*)(arg_type1, arg_type2, arg_type3),
				ret_type (A::*)(arg_type1, arg_type2, arg_type3)>(_ptr);
	del.owner = type_transform<T*,A*>(obj);
	return del;
}

template<typename ret_type, typename arg_type1 , typename arg_type2 ,
	typename arg_type3 , typename arg_type4 , typename arg_type5 ,
	typename arg_type6 , typename arg_type7 >
template<typename T>
delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7>::method(T *obj,ret_type (T::*_ptr)(arg_type1, arg_type2, arg_type3, arg_type4)) {
	this_delegate del;
	del.type = del_method;
	del.ptr.meth_a4 = type_transform<ret_type (T::*)(arg_type1, arg_type2, arg_type3, arg_type4),
				ret_type (A::*)(arg_type1, arg_type2, arg_type3, arg_type4)>(_ptr);
	del.owner = type_transform<T*,A*>(obj);
	return del;
}

template<typename ret_type, typename arg_type1 , typename arg_type2 ,
	typename arg_type3 , typename arg_type4 , typename arg_type5 ,
	typename arg_type6 , typename arg_type7 >
template<typename T>
delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7>::method(T *obj,ret_type (T::*_ptr)(arg_type1, arg_type2, arg_type3, arg_type4, arg_type5)) {
	this_delegate del;
	del.type = del_method;
	del.ptr.meth_a5 = type_transform<ret_type (T::*)(arg_type1, arg_type2, arg_type3, arg_type4, arg_type5),
				ret_type (A::*)(arg_type1, arg_type2, arg_type3, arg_type4, arg_type5)>(_ptr);
	del.owner = type_transform<T*,A*>(obj);
	return del;
}

template<typename ret_type, typename arg_type1 , typename arg_type2 ,
	typename arg_type3 , typename arg_type4 , typename arg_type5 ,
	typename arg_type6 , typename arg_type7 >
template<typename T>
delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7>::method(T *obj,ret_type (T::*_ptr)(arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6)) {
	this_delegate del;
	del.type = del_method;
	del.ptr.meth_a6 = type_transform<ret_type (T::*)(arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6),
				ret_type (A::*)(arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6)>(_ptr);
	del.owner = type_transform<T*,A*>(obj);
	return del;
}

template<typename ret_type, typename arg_type1 , typename arg_type2 ,
	typename arg_type3 , typename arg_type4 , typename arg_type5 ,
	typename arg_type6 , typename arg_type7 >
template<typename T>
delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7>::method(T *obj,ret_type (T::*_ptr)(arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7)) {
	this_delegate del;
	del.type = del_method;
	del.ptr.meth_a7 = type_transform<ret_type (T::*)(arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7),
		ret_type (A::*)(arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7)>(_ptr);
	del.owner = type_transform<T*,A*>(obj);
	return del;
}

/* Save function pointer for different number of arguments */
template<typename ret_type, typename arg_type1 , typename arg_type2 ,
	typename arg_type3 , typename arg_type4 , typename arg_type5 ,
	typename arg_type6 , typename arg_type7 >
delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7>::function(ret_type (*_ptr)()) {
	this_delegate del;
	del.type = del_function;
	del.ptr.func_a0 = _ptr;
	return del;
}		

template<typename ret_type, typename arg_type1 , typename arg_type2 ,
	typename arg_type3 , typename arg_type4 , typename arg_type5 ,
	typename arg_type6 , typename arg_type7 >
delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7>::function(ret_type (*_ptr)(arg_type1)) {
	this_delegate del;
	del.type = del_function;
	del.ptr.func_a1 = _ptr;
	return del;
}

template<typename ret_type, typename arg_type1 , typename arg_type2 ,
	typename arg_type3 , typename arg_type4 , typename arg_type5 ,
	typename arg_type6 , typename arg_type7 >
delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7>::function(ret_type (*_ptr)(arg_type1, arg_type2)) {
	this_delegate del;
	del.type = del_function;
	del.ptr.func_a2 = _ptr;
	return del;
}

template<typename ret_type, typename arg_type1 , typename arg_type2 ,
	typename arg_type3 , typename arg_type4 , typename arg_type5 ,
	typename arg_type6 , typename arg_type7 >
delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7>::function(ret_type (*_ptr)(arg_type1, arg_type2, arg_type3)) {
	this_delegate del;
	del.type = del_function;
	del.ptr.func_a3=_ptr;
	return del;
}

template<typename ret_type, typename arg_type1 , typename arg_type2 ,
	typename arg_type3 , typename arg_type4 , typename arg_type5 ,
	typename arg_type6 , typename arg_type7 >
delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7>::function(ret_type (*_ptr)(arg_type1, arg_type2, arg_type3, arg_type4)) {
	this_delegate del;
	del.type = del_function;
	del.ptr.func_a4 = _ptr;
	return del;
}

template<typename ret_type, typename arg_type1 , typename arg_type2 ,
	typename arg_type3 , typename arg_type4 , typename arg_type5 ,
	typename arg_type6 , typename arg_type7 >
delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7>::function(ret_type (*_ptr)(arg_type1, arg_type2, arg_type3, arg_type4, arg_type5)) {
	this_delegate del;
	del.type = del_function;
	del.ptr.func_a5 = _ptr;
	return del;
}

template<typename ret_type, typename arg_type1 , typename arg_type2 ,
	typename arg_type3 , typename arg_type4 , typename arg_type5 ,
	typename arg_type6 , typename arg_type7 >
delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7>::function(ret_type (*_ptr)(arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6)) {
	this_delegate del;
	del.type = del_function;
	del.ptr.func_a6 = _ptr;
	return del;
}

template<typename ret_type, typename arg_type1 , typename arg_type2 ,
	typename arg_type3 , typename arg_type4 , typename arg_type5 ,
	typename arg_type6 , typename arg_type7 >
delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7>::function(ret_type (*_ptr)(arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7)) {
	this_delegate del;
	del.type = del_function;
	del.ptr.func_a7 = _ptr;
	return del;
}

/* Calling function/method with different number of arguments */
template<typename ret_type, typename arg_type1 , typename arg_type2 ,
	typename arg_type3 , typename arg_type4 , typename arg_type5 ,
	typename arg_type6 , typename arg_type7 >
ret_type delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7>::call(arg_type1 arg1, arg_type2 arg2, arg_type3 arg3, arg_type4 arg4, arg_type5 arg5, arg_type6 arg6, arg_type7 arg7) const {
	if (type == del_method)
		return (owner->*(ptr.meth_a7))(arg1, arg2, arg3, arg4, arg5, arg6, arg7);
	else if (type == del_function)
		return ptr.func_a7(arg1, arg2, arg3, arg4, arg5, arg6, arg7);

	return (ret_type)0;
}

/* Overloaded operators () for different numbers of arguments */
template<typename ret_type, typename arg_type1 , typename arg_type2 ,
	typename arg_type3 , typename arg_type4 , typename arg_type5 ,
	typename arg_type6 , typename arg_type7 >
ret_type delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7>::operator()() const {
	return call(0, 0, 0, 0, 0, 0, 0);
}

template<typename ret_type, typename arg_type1 , typename arg_type2 ,
	typename arg_type3 , typename arg_type4 , typename arg_type5 ,
	typename arg_type6 , typename arg_type7 >
ret_type delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7>::operator()(arg_type1 arg1) const {
	return call(arg1, 0, 0, 0, 0, 0, 0);
}	   

template<typename ret_type, typename arg_type1 , typename arg_type2 ,
	typename arg_type3 , typename arg_type4 , typename arg_type5 ,
	typename arg_type6 , typename arg_type7 >
ret_type delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7>::operator()(arg_type1 arg1, arg_type2 arg2) const {
	return call(arg1, arg2, 0, 0, 0, 0, 0);
}	   

template<typename ret_type, typename arg_type1 , typename arg_type2 ,
	typename arg_type3 , typename arg_type4 , typename arg_type5 ,
	typename arg_type6 , typename arg_type7 >
ret_type delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7>::operator()(arg_type1 arg1, arg_type2 arg2, arg_type3 arg3) const {
	return call(arg1, arg2, arg3, 0, 0, 0, 0);
}	   

template<typename ret_type, typename arg_type1 , typename arg_type2 ,
	typename arg_type3 , typename arg_type4 , typename arg_type5 ,
	typename arg_type6 , typename arg_type7 >
ret_type delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7>::operator()(arg_type1 arg1, arg_type2 arg2, arg_type3 arg3, arg_type4 arg4) const {
	return call(arg1, arg2, arg3, arg4, 0, 0, 0);
}	   

template<typename ret_type, typename arg_type1 , typename arg_type2 ,
	typename arg_type3 , typename arg_type4 , typename arg_type5 ,
	typename arg_type6 , typename arg_type7 >
ret_type delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7>::operator()(arg_type1 arg1, arg_type2 arg2, arg_type3 arg3, arg_type4 arg4, arg_type5 arg5) const {
	return call(arg1, arg2, arg3, arg4, arg5, 0, 0);
}	   

template<typename ret_type, typename arg_type1 , typename arg_type2 ,
	typename arg_type3 , typename arg_type4 , typename arg_type5 ,
	typename arg_type6 , typename arg_type7 >
ret_type delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7>::operator()(arg_type1 arg1, arg_type2 arg2, arg_type3 arg3, arg_type4 arg4, arg_type5 arg5, arg_type6 arg6) const {
	return call(arg1, arg2, arg3, arg4, arg5, arg6, 0);
}

template<typename ret_type, typename arg_type1 , typename arg_type2 ,
	typename arg_type3 , typename arg_type4 , typename arg_type5 ,
	typename arg_type6 , typename arg_type7 >
ret_type delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7>::operator()(arg_type1 arg1, arg_type2 arg2, arg_type3 arg3, arg_type4 arg4, arg_type5 arg5, arg_type6 arg6, arg_type7 arg7) const {
	return call(arg1, arg2, arg3, arg4, arg5, arg6, arg7);
}

template<typename ret_type, typename arg_type1 , typename arg_type2,
	typename arg_type3, typename arg_type4, typename arg_type5,
	typename arg_type6, typename arg_type7>
bool delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7>::operator==(const delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> &x) const {
	if (x.owner == owner && ptr.meth_a0 == x.ptr.meth_a0)
		return true;
	return false;
}

template<typename ret_type, typename arg_type1 , typename arg_type2,
	typename arg_type3, typename arg_type4, typename arg_type5,
	typename arg_type6, typename arg_type7>
bool delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7>::null() const {
	return type == del_uninit;
}
