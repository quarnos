/* Quarn OS
 *
 * List
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _LIST_H_
#error This file can not be compiled directly.
#endif

#include "libs/string.h"
//#include "manes/error.h"

template<typename T>
list_element<T>::list_element() : value((T*)0) {}

template<typename T>
list_element<T>::~list_element() {
	if (value)
		delete value;
}

template<typename T>
void list_element<T>::set_value(const T &val) {
	value = new T(val);
}

template<typename T>
T &list_element<T>::get_value() {
	if (value)
		return *value;
	//manes::error *er = manes::manec::err_msg("list: element is not present");
	//er->critical();
	while(1);
}

/* class list<T> */

template<typename T>
void list<T>::insert(const int i, const T &obj) {
/*	list_element<T> *element = new list_element<T>();
	element->set_value(obj);
	list_element<T> &index = iter[i];
	element->next = &index;
	element->prev = index.prev;
	index.prev->next=element;
	index.prev=element;
	if (i==0) start=element;
	if (i==count-1) end=element;
	count++;
	iter=start;*/
}

template<typename T>
void list<T>::add(const T &obj) {
	list_element<T> *element = new list_element<T>();
	if (count==0) { end=element; start=element; }
	element->set_value(obj);
	element->next=start;
	element->prev=end;
	end->next=element;
	end=element;
	count++;
	start->prev=element;
}

template<typename T>
void list<T>::remove(const int i) {
	list_element<T> *_element = start;

	for (int in = 0; in < i; in++)
		_element = _element->next;

	list_element<T> &element = *_element; 

	if (start==&element) start=element.next;
	if (end==&element) end=element.prev;
	element.next->prev = element.prev;
	element.prev->next = element.next;
	count--;

	delete _element;
}

template<typename T>
int list<T>::get_count() const {
	return count;
}

template<typename T>
T &list<T>::operator[](int index) {
	list_element<T> *element = start;

	for (int i = 0; i < index; i++)
		element = element->next;

	return element->get_value();
}

template<typename T>
const T &list<T>::operator[](const int index) const {
	list_element<T> *element = start;

	for (int i = 0; i < index; i++)
		element = element->next;

	return element->get_value();
}

template<typename T>
list<T>::list() : count(0) {}

template<typename T>
list<T>::~list() {
	while (get_count())
		remove(0);
}

