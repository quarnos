/* Quarn OS
 *
 * setjmp and longjmp functions
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

.global __setjmp
__setjmp:
	/* Get structure from pointer */
	movl	4(%esp), %edx

	/* Save return address */
	movl	(%esp), %eax
	movl	%eax, 12(%edx)
	
	/* Save stack pointer */
	movl	%esp, (%edx)

	/* Save base pointer */
	movl	%ebp, 4(%edx)

	/* Save flags */
	pushf
	popl	%eax
	movl	%eax, 8(%edx)

	/* Clear eax */
	xorl	%eax, %eax
	
	ret
		
.global __longjmp
__longjmp:
	movl	%esp, %edx
	
	/* Restore stack pointer */
	movl	4(%edx), %esp

	/* Restore base pointer */
	movl	8(%edx), %ebp

	/* Restore flags */
	movl	12(%edx), %eax
	pushl	%eax
	popf

	/* Set eax */
	movl	20(%edx), %eax

	/* Return */
	movl	16(%edx), %edx
	pushl	%edx

	ret
	