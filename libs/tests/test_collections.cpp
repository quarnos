/* Quarn OS
 *
 * Testing tool for collections
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include COLLECTION_H
#include <iostream>
#include <cstdlib>
#include <vector>

static int first[]	= { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
static int second[]	= { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };
static int third[]	= { 1, 2, 3, 5, 7, 8, 9 };
static int deleted[]	= { 0, 4, 6 };

int used_memory = 0;
bool error = false;
int addresses[2000];
int sizes[2000];
int last = 0;

void *operator new(unsigned int size) {
	used_memory += size;
	void *addr = malloc(size);
	addresses[last] = (int)addr;
	sizes[last++] = size;
	return addr;
}

void *operator new[](unsigned int size) {
	return operator new(size);
}

void operator delete(void *ptr) {
	for (int i =  0; i < last; i++) {
		if (addresses[i] == (int)ptr) {
			used_memory -= sizes[i];
			addresses[i] = 0;
		}
	}
	free(ptr);
}

void operator delete[](void *ptr) {
	return operator delete(ptr);
}

int test() {
	COLLECTION<int> data;// = new COLLECTION<int>;

	std::cout << "Test #1: Basic Operations\n";

	std::cout << "\t* adding data\n";
	for (int i = 0; i < 10; i++)
		data.add(first[i]);

	std::cout << "\t* reading data\n";
	for (int i = 0; i < data.get_count(); i++) {
		if (data[i]!=first[i]) {
			std::cout << "\t  -> failed\n";
			break;
		}
	}

	std::cout << "\t* reading random data\n";
	for (int i = 0; i < data.get_count(); i++) {
		int b = rand() % data.get_count();
	if (data[b]!=first[b]) {
			std::cout << "\t  -> failed\n";
			break;
		}
	}		

	std::cout << "\t* reverse reading data\n";
	for (int i = 0; i < data.get_count(); i++) {
		if (data[data.get_count() - i - 1]!=second[i]) {
			std::cout << "\t  -> failed\n";
			break;
		}
	}

	std::cout << "\t* removing random data\n";
	data.remove(0);
	data.remove(3);
	data.remove(4);
	for (int i = 0; i < data.get_count(); i++) {
		if (data[i]!=third[i]) {
			std::cout << "\t  -> failed\n";
			break;
		}
	} 
/*	
	std::cout << "\t* inserting random data\n";
	data.insert(0,deleted[0]);
	data.insert(4,deleted[1]);
	data.insert(6,deleted[2]);
	for (int i = 0; i < data.get_count(); i++) {
		if (data[i]!=first[i]) {
			std::cout << "\t  -> failed\n";
			break;
		}
	}
*/
}

int main() {
	test();

	std::cout << "\t* checking for memory leaks\n";
	if (used_memory != 0)
		std::cout << "\t  -> failed\n";
}

void *memcpy(void *a, void const * b, int size) {
	for (int i = 0; i < size; i++)
		((char*)a)[i] = ((char * const)b)[i];

	return a;
}
