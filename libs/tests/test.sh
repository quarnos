#!/bin/sh

# Quarn OS
#
# Testing script
#
# Copyright (C) 2008-2009 Pawel Dziepak
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#


data_struct=( list vector )

if [ "$1" == "" ]; then
    echo -e "Data structures examiner. Usage:\n\t./test.sh [collection type]\n"
    echo -e "Data structures, collections and containers available to exam:"
    for element in ${data_struct[*]}
    do
        echo -e "\t$element";
    done
        echo -ne "\n";
    exit
fi

if [ "`echo ${data_struct[*]} | grep $1`" == "" ]; then
    echo -e "Can not find data structure called \`$1'."
    exit
fi

echo -n "Testing data structure: "
echo -e $1
g++ -o test_$1 test_collections.cpp -DCOLLECTION_H=\"../$1.h\" -DCOLLECTION=$1 -I../ -I../../

./test_$1
rm test_$1

