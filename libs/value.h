/* Quarn OS
 *
 * Safe arythmetic
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _CHECKED_H_
#define _CHECKED_H_

template<typename T>
class checked {
private:
	T val;
public:
	checked() : val(0) {}
	checked(T x) : val(x) {}

	template<typename U>
	checked(const checked<U> &x) : val((T)x.val) {}

	checked<T> &operator=(T &x) {
		val = x;
		return *this;
	}

	operator T() { return val; }

	template<typename U>
	operator U() {
		if (sizeof(U) >= sizeof(T))
			return (U)val;
		if (val >> (sizeof(U) * 8) == 0)
			return (U)val;

		return 0;
	}

	void set(T v) { val = v; }

	checked<T> operator++() {
//		if (val < limit<T>::max)
		++val;
	}

	checked<T> operator++(int) {
		return val++;
	}

	checked<T> operator--() {
//		if (val < limit<T>::max)
		--val;
	}

	checked<T> operator--(int) {
		return val--;
	}

	checked<T> operator+(T x) {
		/* Dangerous. Standard states that there are no overflows! */
		/* Use volatile to disallow optimizations */
		volatile T r = val + x;

		/*
		 * First case: val and x.val are both positive =>
		 * => val + x.val > max(val, x.val)
		 *
		 * Second case: val and/or x.val is negative =>
		 * => val + x.val < max(val, x.val)
		 */
		if ((val > 0 && x > 0 && r < val)
	       	|| ((val < 0 || x < 0) && r > (x > val ? x : val)))
			return 0;

		return checked<T>(r);
	}

	checked<T> operator-(T x) {
		/* Dangerous. Standard states that there are no overflows! */
		volatile T r = val - x;

		/*
		 * First case: val and x.val are both positive =>
		 * => val - x.val < val
		 *
		 * Second case: x.val is negative =>
		 * => val - x.val > val;
		 */
		if ((x > 0 && r > val) || (x < 0 && r < val))
			return 0;

		return checked<T>(r);
	}

	checked<T> operator*(T x) {
		/* Dangerous. Standard states that there are no overflows! */
		if (val * x < val)
			return 0;

		return checked<T>(val * x);
	}

	checked<T> operator/(T x) {
		if (x == 0)
			return 0;

		/* No need to check for overflows */
		return checked<T>(val / x);
	}
};

#endif
