/* Quarn OS
 *
 * Vector
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _VECTOR_H_
#define _VECTOR_H_

template<typename T>
class vector_element {
private:
	T *value;
	
public:
	vector_element();
	~vector_element();

	T &get_value();
	void set_value(const T &val);
};

template<typename T>
class vector {
protected:
	vector_element<T> **table;

	volatile int count;

	int size;
public:
	vector();
	~vector();

	typedef T element_type;

	void add(const T obj);
	void insert(const int i, const T &obj);
	void remove(const int i);
	
	int get_count() const;

	T &operator[](const int index);
	const T &operator[](const int index) const;
};

/* Anyone thinks that this is ugly? */
#include "vector.cpp"

#endif
