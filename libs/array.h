/* Quarn OS
 *
 * Pointer
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _ARRAY_H_
#define _ARRAY_H_

#include "buffer.h"
#include "manes/exception.h"

template<typename T>
class array {
private:
	T *data;
	int size;
public:
	array(int count) : data(new T[count]), size(count * sizeof(T)) { }
	array(T* _data, int _size) : data(_data), size(_size) { }
	~array() {
	       	delete data;
	}

	T &operator[](int index) {
		if (index >= 0 && index < size / sizeof(T))
			return data[index];

		throw new index_out_of_range_exception();
	}

	int length() const {
		return size / sizeof(T);
	}

	template<typename U>
	array<U> cast() {
		return array<U>(reinterpret_cast<U*>(data), size);
	}

	static array<T> from_buffer(const buffer &x) {
		return array<T>((T*)x.get_address(), x.get_size());
	}

	buffer to_buffer() const {
		return buffer(data, size);
	}
};


template<typename T, int X>
class array_stack {
private:
	T data[X];
	const int size;
public:
	array_stack() : size(X) { }

	T &operator[](int index) {
		if (index >= 0 && index < size)
			return data[index];

		throw new index_out_of_range_exception();
	}
};
#endif
