/* Quarn OS
 *
 * setjmp and longjmp functions
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

/**
 * @file setjmp.h
 * setjmp and longjmp functions
 */

#undef no_mangling
#ifdef __cplusplus
#define no_mangling extern "C"
#else
#define no_mangling
#endif

/* Standard wants jmp_buf to be an array. */
struct jmp_buf {
	long buf[4];
};

no_mangling int __setjmp(jmp_buf *env);
no_mangling void __longjmp(jmp_buf env, int value);

/* Standard wants set/longjmp not to get a pointer */
#define setjmp(env) __setjmp(&env)
#define longjmp(env, value) __longjmp(env, value)
