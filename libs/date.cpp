/* Quarn OS
 *
 * Date
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "date.h"

date::date() : timestamp(-1) {}

int months[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31, 0 };

date::date(int ts) {
	timestamp = ts / (24 * 60 * 60);
}

date::date(int day, int month, int year) {
	timestamp = (year - 1970) * 365;
	for (; month - 1 > 0; month--)
		timestamp += months[month - 1];
	timestamp += day + (year - 1970) / 4;
}

date::operator int() {
	return timestamp * 24 * 60 * 60;
}

int date::get_year() const {
	return 1970 + (timestamp + timestamp / 365 / 4)/ 365;
}

int date::get_month() const {
        int month = 0;
	if (get_year() % 4 == 0)
		months[1]++;

	for (int i = 0, ts = (timestamp - timestamp / 365 / 4) % 365; ts >= months[i]; i++) {
		month++;
		ts -= months[i];
	}

	if (get_year() % 4 == 0)
		months[1]--;

	return month + 1;
}

int date::get_day() const {
        int month = 0;
	if (get_year() % 4 == 0)
		months[1]++;
	int ts = (timestamp  - (timestamp / 365 + 2) / 4) % 365;
	for (int i = 0; ts >= months[i]; i++) {
		month++;
		ts -= months[i];
	}

	if (get_year() % 4 == 0)
		months[1]--;
	_ts = ts;

	return ts + 1;
}

time::time() : timestamp(-1) {}

time::time(int ts) {
	timestamp = ts % (24 * 60 * 60);
}

time::time(int hour, int minute, int second) {
	timestamp = hour * 3600 + minute * 60 + second;
}

time::operator int() {
	return timestamp;
}

int time::get_second() const {
	return timestamp % 60;
}

int time::get_minute() const {
	return timestamp / 60 % 60;
}

int time::get_hour() const {
	return timestamp / 3600;
}
