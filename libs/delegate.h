/* Quarn OS
 *
 * Delegates
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _DELEGATE_H_
#define _DELEGATE_H_

template<typename ret_type, typename arg_type1 = int, typename arg_type2 = int,
	typename arg_type3 = int, typename arg_type4 = int,
	typename arg_type5 = int, typename arg_type6 = int,
	typename arg_type7 = int>
class delegate {
private:

	class A {};

	/* Pointer to object (for methods only) */
	A *owner;

	/* Union of different types of pointer to method/function */
	union {
		/* Direct call method */
		ret_type (A::*meth_a0)();
		ret_type (A::*meth_a1)(arg_type1);
		ret_type (A::*meth_a2)(arg_type1, arg_type2);
		ret_type (A::*meth_a3)(arg_type1, arg_type2, arg_type3);
		ret_type (A::*meth_a4)(arg_type1, arg_type2, arg_type3, arg_type4);
		ret_type (A::*meth_a5)(arg_type1, arg_type2, arg_type3, arg_type4, arg_type5);
		ret_type (A::*meth_a6)(arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6);
		ret_type (A::*meth_a7)(arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7);

		/* Direct call function */
		ret_type (*func_a0)();
		ret_type (*func_a1)(arg_type1);
		ret_type (*func_a2)(arg_type1, arg_type2);
		ret_type (*func_a3)(arg_type1, arg_type2, arg_type3);
		ret_type (*func_a4)(arg_type1, arg_type2, arg_type3, arg_type4);
		ret_type (*func_a5)(arg_type1, arg_type2, arg_type3, arg_type4, arg_type5);
		ret_type (*func_a6)(arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6);
		ret_type (*func_a7)(arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7);
	} ptr;

	/* Current state of the delegate */
	enum del_type { del_uninit, del_method, del_function } type;

	typedef delegate<ret_type, arg_type1, arg_type2, arg_type3,arg_type4, arg_type5, arg_type6, arg_type7> this_delegate;

public:
	delegate();
	~delegate();

	delegate(const delegate<ret_type, arg_type1, arg_type2, arg_type3,arg_type4, arg_type5, arg_type6, arg_type7>&);
	delegate<ret_type, arg_type1, arg_type2, arg_type3,arg_type4, arg_type5, arg_type6, arg_type7> &operator=(const delegate<ret_type, arg_type1, arg_type2, arg_type3,arg_type4, arg_type5, arg_type6, arg_type7> &);
	
	
	/* Save method pointer for different number of arguments */
	template<class T> static delegate<ret_type, arg_type1, arg_type2, arg_type3,arg_type4, arg_type5, arg_type6, arg_type7> method(T *obj,ret_type (T::*_ptr)());
	template<class T> static delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> method(T *obj,ret_type (T::*_ptr)(arg_type1));
	template<class T> static delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> method(T *obj,ret_type (T::*_ptr)(arg_type1, arg_type2));
	template<class T> static delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> method(T *obj,ret_type (T::*_ptr)(arg_type1, arg_type2, arg_type3));
	template<class T> static delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> method(T *obj,ret_type (T::*_ptr)(arg_type1, arg_type2, arg_type3, arg_type4));
	template<class T> static delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> method(T *obj,ret_type (T::*_ptr)(arg_type1, arg_type2, arg_type3, arg_type4, arg_type5));
	template<class T> static delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> method(T *obj,ret_type (T::*_ptr)(arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6));
	template<class T> static delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> method(T *obj,ret_type (T::*_ptr)(arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7));

	/* Save function pointer for different number of arguments */
	static delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> function(ret_type (*_ptr)());
	static delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> function(ret_type (*_ptr)(arg_type1));
	static delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> function(ret_type (*_ptr)(arg_type1, arg_type2));
	static delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> function(ret_type (*_ptr)(arg_type1, arg_type2, arg_type3));
	static delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> function(ret_type (*_ptr)(arg_type1, arg_type2, arg_type3, arg_type4));
	static delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> function(ret_type (*_ptr)(arg_type1, arg_type2, arg_type3, arg_type4, arg_type5));
	static delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> function(ret_type (*_ptr)(arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6));
	static delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> function(ret_type (*_ptr)(arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7));

	/* Calling function/method with different number of arguments */
	ret_type call(arg_type1 arg1, arg_type2 arg2, arg_type3 arg3, arg_type4 arg4, arg_type5 arg5, arg_type6 arg6, arg_type7 arg7) const;

	/* Overloaded operators () for different numbers of arguments */
	ret_type operator()() const;
	ret_type operator()(arg_type1 arg1) const;
	ret_type operator()(arg_type1 arg1, arg_type2 arg2) const;
	ret_type operator()(arg_type1 arg1, arg_type2 arg2, arg_type3 arg3) const;
	ret_type operator()(arg_type1 arg1, arg_type2 arg2, arg_type3 arg3, arg_type4 arg4) const;
	ret_type operator()(arg_type1 arg1, arg_type2 arg2, arg_type3 arg3, arg_type4 arg4, arg_type5 arg5) const;
	ret_type operator()(arg_type1 arg1, arg_type2 arg2, arg_type3 arg3, arg_type4 arg4, arg_type5 arg5, arg_type6 arg6) const;
	ret_type operator()(arg_type1 arg1, arg_type2 arg2, arg_type3 arg3, arg_type4 arg4, arg_type5 arg5, arg_type6 arg6, arg_type7 arg7) const;

	bool operator==(const delegate<ret_type, arg_type1, arg_type2, arg_type3, arg_type4, arg_type5, arg_type6, arg_type7> &) const;

	bool null() const;
};

#include "delegate.cpp"

#endif

