/* Quarn OS
 *
 * Date
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _DATE_H_
#define _DATE_H_

class date {
private:
	int timestamp;
	mutable int _ts;
public:
	date();
	date(int);
	date(int, int, int);

	operator int();

	int get_day() const;
	int get_month() const;
	int get_year() const;
};

class time {
private:
	int timestamp;
public:
	time();
	time(int);
	time(int, int, int);

	operator int();

	int get_second() const;
	int get_minute() const;
	int get_hour() const;
};

#endif
