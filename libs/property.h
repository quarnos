/* Quarn OS
 *
 * Lambda operator and properties
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#define glue(a, b)

#define __LAMBDA glue(__lambda, __COUNTER__)
#define __PROPERTY glue(__property, __COUNTER__)

#define lambda(t, a, x) struct __LAMBDA { t operator() a { x } }

#define property(T, set, get)	class __PROPERTY {			\
				private:				\
					set _set;			\
					get _get;			\
				public: 				\
					void operator=(T val) { 	\
						_set(val); 		\
					}				\
					operator T() { 			\
						return _get(); 		\
					} 				\
				}

/*
 Usage:

property(int,
	lambda(void, (int x), printf("%d\n", x);),
	lambda(int, (void), return 5;)) value;

 */
