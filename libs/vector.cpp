/* Quarn OS
 *
 * Vector
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _VECTOR_H_
#error This file can not be compiled directly.
#endif

//#include "libs/string.h"
//#include "manes/error.h"

template<typename T>
vector_element<T>::vector_element() : value((T*)0) {}

template<typename T>
vector_element<T>::~vector_element() {
	if (value)
		delete value;
}

template<typename T>
void vector_element<T>::set_value(const T &val) {
	value = new T(val);
}

template<typename T>
T &vector_element<T>::get_value() {
	if (value)
		return *value;
//	manes::error er("vector: element is not present");
//	er.critical();
	while(1);
}

/* class vector<T> */
void memrcpy(char *a, char *b, int size) __attribute__((weak));
void memrcpy(char *a, char *b, int size) {
	for (int i = size - 1; i >= 0; i--) a[i]=b[i];
}

template<typename T>
void vector<T>::insert(const int i, const T &obj) {
/*	if (size < count + 1) {
		vector_element<T> **temp = new vector_element<T>*[size * 2];
		memcpy(temp, table, size * sizeof(vector_element<T>*));
		delete [] table;
		table = temp;
	}

	memrcpy((char*)&(table[i+1]),(char*)&(table[i]), (size-i+1) * sizeof(vector_element<T>*));
	vector_element<T> *element = new vector_element<T>();
	element->set_value(obj);
	table[i] = element;
	count++;*/
}

template<typename T>
void vector<T>::add(const T obj) {
	vector_element<T> *element = new vector_element<T>();
	if (size < count + 1) {
		vector_element<T> **temp = new vector_element<T>*[size * 2];
		memcpy(temp, table, size * sizeof(vector_element<T>*));
		delete [] table;
		table = temp;
		size *= 2;
	}
	element->set_value(obj);
	table[count] = element;
	count++;
}

template<typename T>
void vector<T>::remove(const int i) {
	vector_element<T> *element = table[i];
	delete element;

	memcpy(&(table[i]), &(table[i + 1]), (count - i + 1) * sizeof(vector_element<T>*));	
	count--;
}

template<typename T>
int vector<T>::get_count() const {
	return count;
}

template<typename T>
T &vector<T>::operator[](const int index) {
	return table[index]->get_value();
}

template<typename T>
const T &vector<T>::operator[](const int index) const {
	return table[index]->get_value();
}


template<typename T>
vector<T>::vector() : count(0), size(32) {
	table = new vector_element<T>*[size];	
}

template<typename T>
vector<T>::~vector() {
	while (get_count())
		remove(0);

	delete [] table;
}

