#ifndef _MATH_H_
#define _MATH_H_

namespace math {

template<typename T>
static inline T min(T x, T y) {
	return x < y ? x : y;
}

template<typename T>
static inline T max(T x, T y) {
	return x > y ? x : y;
}

template<typename T>
static inline T abs(T x) {
	return x > 0 ? x : -x;
}

}

#endif
