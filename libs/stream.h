#ifndef _LSTREAM_H_
#define _LSTREAM_H_

#include "buffer.h"
#include "resources/device.h"
#include "delegate.h"

namespace resources {
	class file;
}
namespace resources {
class stream {
public:
	virtual void read(buffer &) = 0;
	virtual void write(const buffer&) = 0;
};

class memory_stream : public stream {
private:
	buffer buf;
	int ptr;

public:
	memory_stream(buffer);

	virtual void read(buffer &);
	virtual void write(const buffer&);
};

class file_stream : public stream {
private:
	p<resources::file> fp;
public:
	file_stream(p<resources::file>);

	virtual void seek(int);
	virtual void read(buffer &);
	virtual void write(const buffer&);
};

}
#endif
