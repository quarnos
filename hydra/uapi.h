/* Quarn OS / Hydra
 *
 * Universal Application Programming Interface
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _UAPI_H_
#define _UAPI_H_

#include "libs/string.h"
#include "libs/list.h"

namespace hydra {
	/**
	 * @brief Universal Application Programming Interface
	 * @details This is the layer that separates high level Hydra interface
	 * with low level operating system specific APIs.
	 */
	namespace uapi {
		class _output {
		public:	
			void write(const char*);
		};

		extern _output *output;

		class _input {
		public:
			void read(char *, int);
		};

		extern _input *input;

		/**
		 * @brief File operations
		 * @details This class provides implementation of all actions
		 * that can be done on files (including directories). This inteface
		 * is independent from both os and file system.
		 */
		class _file {
		private:
			void *data;
		public:
			/**
			 * Opens files
			 * @param name file name
			 */
			void open(const char *name);
			void close();

			/**
			 * Reads file (unrecommended)
			 * @return All data present in the file
			 */
			const char *readall();

			/**
			 * Returns list of files present in directory.
			 */
			const list<string> subfiles();
		};
	}
}

#endif
