/* Quarn OS / Hydra
 *
 * UI class implementation
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "ui.h"
#include "console.h"

using namespace hydra;

console::console() : ui(ui_console) {}

ui::ui(ui_type uit) : uitype(uit) {}

void ui::add(p<ui_element> el) {
	elements.push(el);
}

void ui::render() {
	while (!elements.empty()) {
		p<ui_element> uie = elements.pop();
		uie->render(get_ui_type());
		if (uie->dispose())
			delete uie;
	}
}

ui::ui_type ui::get_ui_type() {
	return uitype;
}

ui_element::ui_element(string name) : element_name(name) {
}

bool ui_element::dispose() {
	return true;
}
