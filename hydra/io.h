/* Quarn OS / Hydra
 *
 * IO class
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _IO_H_
#define _IO_H_

#include "uapi.h"

namespace hydra {
	class io {
	protected:
		string io_name;

		uapi::_file internals;
	public:
		~io() { close(); }

		void close();

		string name() { return io_name; }

		const list<io*> list_dir();
	        const char *readall() { return internals.readall(); }

		static io *open_file(string name);
		static io *enter_dir(string name){ return new io;}
	};
}

#endif
