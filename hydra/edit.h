/* Quarn OS / Hydra
 *
 * Edit class
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "ui.h"
#include "hydra.h"
#include "form.h"
#include "libs/delegate.h"

namespace hydra {
	class edit : public ui_element {
	protected:
		/* FIXME: use dynamic table */
		char buffer[100];
		int index;

		delegate<void, string> ui_event;

		volatile bool filled;

		void on_keyb();
	public:
		delegate<string> value;

		edit(p<form> frm, delegate<void, string>, string);
		edit(p<form> frm, string);
		void render(ui::ui_type);

		string get_value();
	};
}
