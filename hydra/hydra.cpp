/* Quarn OS / Hydra
 *
 * Application and Runtime classes implementations
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "hydra.h"
#include "console.h"
#include "uapi.h"

using namespace hydra;

void __hydra_start();

void hydra::start() {
	uapi::output = new uapi::_output;

	runtime::create();

	__hydra_start();
}

void application::set_ui(ui::ui_type) {
	app_ui = new console;
}

ui *application::get_ui() {
	return app_ui;
}

void application::exit() {
while(1);
}

namespace hydra {
	runtime *runtime::instance = (runtime*)0;
}

runtime::runtime() {}

void runtime::create() {
	instance = new runtime;
}

runtime *runtime::get() {
	return instance;
}

void runtime::start_app(application *newapp) {
	app = newapp;
	app->init();
	app->get_ui()->render();
}
