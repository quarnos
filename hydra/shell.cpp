/* Quarn OS
 *
 * Basic shell for Quarn
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "hydra.h"
#include "ui.h"
#include "label.h"
#include "edit.h"
#include "io.h"

class shell : public hydra::application {
public:
	void init();
	void command(p<hydra::form>);
};

hydra_app_class(shell)

void shell::init() {
	set_ui(hydra::ui::ui_console);
	delegate<void, p<hydra::form> > onFilled;
	onFilled.method(this, &shell::command);

	hydra::form *welc = new hydra::form(this);
	new hydra::label(welc, "Welcome to Quarn OS\n\n","welcome");

	hydra::form *main = new hydra::form(this, onFilled);
	new hydra::label(main, "quarnos> ", "cmd_prompt");
	new hydra::edit(main, "cmd");
}

void shell::command(p<hydra::form> query) {
	string cmd = query.cast<hydra::form>()->get_element("cmd").cast<hydra::edit>()->get_value();

	if (!strcmp(cmd,"exit"))
		exit();
	else if (!strcmp(cmd,"info")) {
		hydra::form *msg = new hydra::form(this);
		new hydra::label(msg, "Copyright (C) 2008-2009 Pawel Dziepak\n","copy");
	} else if (!strncmp(cmd, "cat ", 4)) {
		string name(&((const char*)cmd)[4]);
		hydra::io *fp = hydra::io::open_file(name);
		hydra::form *msg = new hydra::form(this);
		new hydra::label(msg, fp->readall(), "file");
	} else if (!strcmp(cmd, "ls")) {
		hydra::io *dir = hydra::io::enter_dir("/");
		hydra::form *msg = new hydra::form(this);
		list<hydra::io*> ldir = dir->list_dir();
		for (int i = 0; i < ldir.get_count(); i++)
			new hydra::label(msg, ldir[i]->name() + "\n", "file");
	} else if (!strcmp(cmd, "help")) {
		hydra::form *msg = new hydra::form(this);
		new hydra::label(msg, "Command list:\n\tcat\t\tshows content of the file\n\tinfo\t\tshows copyright information\n\thelp\t\tshows list of commands\n\tls\t\tshows files in current directory\n\texit\t\thalts shell\n","help");
	} else {
		hydra::form *msg = new hydra::form(this);
		new hydra::label(msg, "No such command, type `help' for more information\n","nocmd");
	}

	query->keep();
}
