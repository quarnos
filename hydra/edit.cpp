/* Quarn OS / Hydra
 *
 * Edit class implementation
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "ui.h"
#include "console.h"
#include "edit.h"
#include "uapi.h"

#include "libs/delegate.h"

using namespace hydra;

edit::edit(p<form> frm, delegate<void, string> uievent, string name) : ui_element(name), filled(false) {
	ui_event = uievent;
	index = 0;
	frm->add(this);

	value.method(this, &edit::get_value);
}


edit::edit(p<form> frm, string name) : ui_element(name), filled(false) {
	index = 0;
	frm->add(this);

	value.method(this, &edit::get_value);
}

void edit::on_keyb() {/*
	uapi::input->read(&(buffer[index++]),1);
	if (buffer[index-1] == '\n') {
		delegate<void> del;
		uapi::input->set_ondatareceived(del);
		buffer[index-1] = 0;
		filled = true;
		ui_event(buffer);
	}*/
}

string edit::get_value() {
	string s((char*)buffer);
	return s;
}

void edit::render(ui::ui_type) {
	index = 0;

	for (char last = '\0';  last != '\n'; index++) {
		uapi::input->read(&buffer[index],1);
		last = buffer[index];
	}
	buffer[index-1] = 0;
}
