/* Quarn OS / Hydra
 *
 * Application and Runtime classes
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _HYDRA_H_
#define _HYDRA_H_

#include "ui.h"

#define hydra_app_class(x) \
void __hydra_start() { \
	hydra::application *app = new x(); \
	hydra::runtime::get()->start_app(app); \
}

namespace hydra {
	/**
	 * @brief Base for main class of each Hydra program.
	 * @details Each Hydra program has to define its main class which inherits
	 * from this base. There is one instance of such class for each process
	 * executing the same Hydra program. Subclass has to provide at least
	 * method init() which is called at the program startup.
	 */
	class application {
	protected:
		/**
		 * User interface used by the program.
		 */
		ui *app_ui;

	public:
		/**
		 * Entry point of each Hydra program.
		 * After initialization Hydra runtime calls this function which has
		 * to be provided by subclass of application. The main task of
		 * this method is to prepare ui used by program.
		 */
		virtual void init() = 0;
		//virtual void release() = 0;

		void set_ui(ui::ui_type);
		ui *get_ui();

		void exit();
	};

	/**
	 * @brief Runtime support for Hydra applications.
	 * @details When Hydra program is run, functions provided by this class
	 * are to be first to execute. This class manages execution flow for
	 * whole lifetime of the process. Runtime class is a singleton, hence
	 * there is only one instance for all processes running the same program.
	 */
	class runtime {
	private:
		//list<application *> apps;
		application *app;

		static runtime *instance;
		runtime();

	public:
		static void create();
		static runtime *get();

		/**
		 * Start application and manages execution flow.
		 * This function initializes Hydra framework, executes
		 *  method hydra::init() and then takes responsibility of
		 * providing user interface and reacting on events.
		 * @param app An instance of main class of Hydra program.
		 */
		void start_app(application *app);
	};

	void start();
}

#endif
