/* Quarn OS / Hydra
 *
 * UAPI implementation for Quarn
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "manes/manec.h"
#include "services/logger.h"
#include "libs/stream.h"
#include "resources/stream.h"
#include "resources/fs.h"
#include "resources/file.h"
#include "../uapi.h"

using namespace hydra::uapi;
namespace hydra {
	namespace uapi {
		_output *output;
		_input *input;
	}
}

void _output::write(const char *x) {
	resources::stream *tty = manes::manec::get()->get<resources::stream>("/keybscr");
	tty->write(buffer((void*)x, strlen(x)));
}

void _input::read(char *x, int size) {
	resources::stream *tty = manes::manec::get()->get<resources::stream>("/keybscr");

	for (int i = 0; i < size; i++) {
		buffer buf = buffer::to_mem(x[i]);
		tty->read(buf);
	}
}

void _file::open(const char *filename) {
	data = new string(filename);
}

const char * _file::readall() {
	p<resources::file> fp = manes::manec::get()->get<resources::file>((string)"/file," + *(string*)data);
	return (const char*)(const string)*fp;
}

const list<string> _file::subfiles() {
	p<resources::fs> fs_floppy = manes::manec::get()->get<resources::fs>("/fat");
	list<p<resources::file> > dir = fs_floppy->list_directory("");
	list<string> ls;
	for (int i = 0; i < dir.get_count(); i++)
		ls.add(dir[i]->get_name());
	return ls;
}

void close() {

}
