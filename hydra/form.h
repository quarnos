/* Quarn OS / Hydra
 *
 * Form class
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _FORM_H_
#define _FORM_H_

#include "ui.h"
#include "hydra.h"
#include "libs/delegate.h"

namespace hydra {
	class form : public ui_element {
	protected:
		p<application> owner;

		delegate<void, p<form> > onfilled;

		list<p<ui_element> > elements;

		int used;
	public:
		form(p<application> app, delegate<void, p<form> >);
		form(p<application> app);

		p<ui_element> get_element(string name);

		void add(p<ui_element>);

		void render(ui::ui_type);

		void keep();

		bool dispose();
	};
}

#endif
