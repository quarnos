/* Quarn OS / Hydra
 *
 * UI class
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _UI_H_
#define _UI_H_

#include "libs/fifo.h"
#include "libs/list.h"
#include "libs/pointer.h"

namespace hydra {
	class ui_element;
	class ui {
	public:
		enum ui_type {
			ui_console
		};
	protected:
		/* Elements to be rendered */
		fifo<p<ui_element> > elements;

		ui_type uitype;
	public:
		ui(ui_type);

		virtual void add(p<ui_element>);

		virtual void render();

		ui_type get_ui_type();
	};

	class ui_element {
	protected:
	public:
		string element_name;

	public:
		ui_element(string name);

		virtual void render(ui::ui_type) = 0;

		virtual bool dispose();
	};
}

#endif
