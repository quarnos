#include "hydra.h"
#include "ui.h"
#include "form.h"
#include "label.h"
#include "edit.h"

class test : public hydra::application {
public:
	void init();
	void onedit(hydra::form*);
};

hydra_app_class(test)

void test::onedit(hydra::form* a) {
	delegate<void,hydra::form*>bl;
	hydra::form *fr = new hydra::form(this, bl);
	new hydra::label(fr, ((hydra::edit*)a->get_element("edit1"))->get_value(), "label3");
}

void test::init() {
	set_ui(hydra::ui::ui_console);
	delegate<void,hydra::form*>bl;
	bl.method(this, &test::onedit);

	hydra::form *fr = new hydra::form(this, bl);
	new hydra::label(fr, "Hydra Sample #1:\nHello World\nEnter data: ", "label1");
	hydra::edit *ed = new hydra::edit(fr, "edit1");
	new hydra::label(fr, "OK, data received: ", "label2");
	new hydra::label(fr, ed->value, "label4");
}
