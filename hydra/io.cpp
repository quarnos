/* Quarn OS / Hydra
 *
 * IO class implementation
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "io.h"

using namespace hydra;

void io::close() {
	internals.close();
}

const list<io*> io::list_dir() { 
	list<string> names = internals.subfiles();
	list<io*> ls;
	for (int i = 0; i < names.get_count(); i++)
		ls.add(open_file(names[i]));
	return ls;
}

io *io::open_file(string name) {
	io *fp = new io();
	fp->io_name = name;
	fp->internals.open((const char*)name);
	return fp;
}

