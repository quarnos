/* Quarn OS / Hydra
 *
 * Form class implementation
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "form.h"
#include "manes/error.h"

using namespace hydra;

form::form(p<application> app, delegate<void, p<form> > on_filled) : ui_element("form"), owner(app), used(1) {
	onfilled = on_filled;
	app->get_ui()->add(this);
}

form::form(p<application> app) : ui_element("form"), owner(app), used(1) {
	app->get_ui()->add(this);
}

void form::add(p<ui_element> el) {
	elements.add(el);
}

p<ui_element> form::get_element(string name) {
	for (int i = 0; i < elements.get_count(); i++)
		if (!strcmp(elements[i]->element_name, name)) return elements[i];

	debug("ui: required element not found");

	/* throw */
	while(1);
}

void form::render(ui::ui_type) {
	for (int i = 0; i < elements.get_count(); i++)
		elements[i]->render(ui::ui_console);

	onfilled(this);
}

void form::keep() {
	used++;
	owner->get_ui()->add(this);
}

bool form::dispose() {
	used--;

	return used < 1;
}
