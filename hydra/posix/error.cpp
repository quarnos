/* Quarn OS / Hydra
 *
 * Error messages suport for POSIX
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "stdio.h"
#include "stdlib.h"

#include "manes/error.h"
#include "libs/string.h"

using namespace manes;

error::error(const string &x) : base_err(0), er_message(x) {
}

void error::debug() {
	fprintf(stderr, "(");
	for (p<error> er = this; er.valid(); er = er->base_err)
		fprintf(stderr, er->er_message);
	fprintf(stderr, ")");
}

void error::assertion() {
	fprintf(stderr, "Assertion failed!\n");
	for (p<error> er = this; er.valid(); er = er->base_err)
		fprintf(stderr, er->er_message);
	fprintf(stderr, "\n");
}

void error::critical() {
	fprintf(stderr, "Critical Quarn OS error has occurred!\n");
	for (p<error> er = this; er.valid(); er = er->base_err)
		fprintf(stderr, er->er_message);
	fprintf(stderr, "\nAborting.");
	exit(-1);
}

