/* Quarn OS / Hydra
 *
 * UAPI implementation for POSIX
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>

#include "../uapi.h"

using namespace hydra::uapi;
namespace hydra {
	namespace uapi {
		_output *output;
		_input *input;
	}
}

void _output::write(const char*x) {
	printf("%s", x);
}

void _input::read(char *x, int size) {
	for (int i = 0; i < size; i++)
		x[i] = getchar();
}

void _file::open(const char *name) {
	FILE *fp = fopen(name, "r+");
	data = (void*)fp;
}

void _file::close() {
	fclose((FILE*)data);
}

const list<string> _file::subfiles() {
	DIR *dir;
	dirent *dire;

	dir = opendir(".");

	list<string> ls;

	while ((dire = readdir(dir)))
		ls.add(dire->d_name);

	closedir(dir);

	return ls;
}

const char *_file::readall() {
	FILE *fp = (FILE*)data;

	if (!fp) return new char[2];

	fseek(fp, 0, SEEK_END);
	long size = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	char *buffer = new char[size];

	for (int i = 0; (buffer[i] = fgetc(fp)) != EOF; i++);
	buffer[size] = 0;

	return buffer;
}
