#ifndef _TIME_SOURCE_H_
#define _TIME_SOURCE_H_

namespace services {
	class time_source {
	public:
		typedef unsigned int time_stamp;
		virtual time_stamp get_timestamp() = 0;
	};
}

#endif
