/* Quarn OS
 *
 * Server
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "server.h"
#include "manes/cds/creator.h"
#include "manes/cds/type.h"
#include "manes/manec.h"

#include "libs/delegate.h"

using namespace manes;
using namespace manes::cds;
using namespace services;

p<component> server::get_component(const component_name &_name) {
	if (!manec::get()->get_type(_name.get_type())->is(component_name::from_path("/type,service")))
		return p<component>::invalid;

	p<component> get = creator::get_component(_name);
	if (!get.valid())
		get = new_component(_name.get_type());

	return get;
}

void server::register_type() {
	manec::get()->register_type<server>("server", "manec");
}
