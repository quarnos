/* Quarn OS
 *
 * Timer
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _TIMER_H_
#define _TIMER_H_

#include "manes/cds/component.h"
#include "kernel_state.h"
#include "libs/delegate.h"

namespace services {
	class timer : public manes::cds::component {
	public:
		typedef delegate<void> timer_tick;
	
	private:
		p<kernel_state> ks;

		int start, end;
		timer_tick call;

		void time_tick();
	public:
		virtual void set_timer(int, timer_tick);

		static void register_type();
	};
}

#endif
