#ifndef _NTP_CLIENT_H_
#define _NTP_CLIENT_H_

#include "resources/net/client_socket.h"
#include "resources/net/udp.h"

#include "time_source.h"

namespace services {
	class ntp_client : time_source {
	private:
		typedef int u64[2];
		struct ntp_packet {
			u8 flags;
			u8 stratum;
			u8 poll_int;
			u8 precision;
			u32 root_delay;
			u32 root_dispersion;
			u32 ref_clock_id;
			u64 ref_clock_update;
			u64 orig_time_stamp;
			u64 rec_time_stamp;
			u64 trans_time_stamp;
		};

		enum ntp_flags {
			ntp_cli = 3,
			ntp_srv = 4,
			ntp_ver4 = 0x20,
			ntp_alarm = 0xc0
		};

		enum {
			ntp_port = 123
		};

		p<net::client_socket> client;

		p<ntp_packet> pkg;
	public:
		~ntp_client() {
			if (client.valid())
				client->close();
		}

		void set(p<net::udp> u) {
			client = u->create_client();
			client->connect(net::ipv4_addr::from_le(213 << 24 | 134 << 16 | 184 << 8 | 12), ntp_port);

			pkg = new ntp_packet;
		}

		time_source::time_stamp get_timestamp() {
			pkg->flags = ntp_cli | ntp_ver4 | ntp_alarm;
			pkg->poll_int = 6;
			pkg->ref_clock_id = to_be32(0x494e4954);

			client->write(buffer::to_mem(pkg));

			buffer buf = buffer::to_mem(pkg);
			client->read(buf);

			time_source::time_stamp timestamp = from_be32(buf.cast<ntp_packet>()->rec_time_stamp[0]);

			/* NTP uses Jan 01 1900 as a base, we need Jan 01 1970 */
			timestamp -= 2208988800;

			return timestamp;
		}
	};
}

#endif
