/* Quarn OS
 *
 * Unmangler
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "manes/manec.h"
#include "unmangler.h"

using namespace services;

/* Main unmangling function */
string unmangler::unmangle(const string &name) const {

	if (strncmp(name, "_Z", 2)) {
		int index = 0;
		return get_argument(name, index);
	}

	string symbol;
	string qualify;
	int index = 2;
	int arguments = 0;

	while (index < name.length()) {
		if (arguments > 1)
			symbol += ", ";
		else if (arguments == 1)
			symbol += "(";
		if (index == 2) {
			if (name[index] == 'K') {
				qualify += "const";
				index++;
			}
		}
		if (name[index] == 'N') {
			symbol += decode_name(name, index);
			arguments++;
		} else if (index != 2) {
			symbol += get_argument(name, index);
			arguments++;
		}

		index++;
	}

	symbol += (string)") " + qualify;
	return symbol;
}

/* Unmangle name */
string unmangler::decode_name(const string &name, int &index) const {
	if (name[index] != 'N')
		return (string)"??";

	string symbol;
	index++;
	       
	do {
		int length = 0;
		for (;string::is_digit(name[index]); index++)
			length = length * 10 + name[index] - '0';
		
		char *scope = new char[length + 1];
		strncpy(scope, &name[index], length);
		scope[length] = 0;
		if (symbol.length())
			symbol += (string)"::";

		symbol += (string)scope;

		index += length;
	} while (name[index] != 'E');
	return symbol;
}

/* Fundamental types */
const int type_number = 14;
const char type_short[] = { 'v', 'b', 'c', 'a', 'h', 's', 't', 'i', 'j', 'l', 'm', 'x', 'y', 'f', 'd' };
const char* type_long[] = {
	"void",
	"bool",
	"char",
	"signed char",
	"unsigned char",
	"short",
	"unsigned short",
	"int",
	"unsigned int",
	"long",
	"unsigned long",
	"long long",
	"unsigned long long",
	"float",
	"double"
 };

/* Unmangle argument */
string unmangler::get_argument(const string &name, int &index) const {
	string addons;
	/* FIXME: Probably wrong order */
	if (name[index] == 'K') {
		addons += (string)" const";
		index++;
	}
	if (name[index] == 'V') {
		addons += (string)" volatile";
		index++;
	}
	if (name[index] == 'R') {
		addons += (string)" &";
		index++;
	} else if (name[index] == 'P') {
		addons += (string)" *";
		index++;
	}
	if (name[index] == 'N')
		return (string)decode_name(name, index) + addons;
	else if (name[index] == 'F') {
		index++;
		string func(get_argument(name,index));
		func += (string)" (*)(";
		bool first = true;
		while (name[index] != 'E') {
			if (!first)
				func += (string)", ";
			first = false;
			func += get_argument(name,index);
			index++;
		}
		func += (string)")";
		return func;
	} else
		for (int i = 0; i < type_number; i++)
			if (type_short[i] == name[index])
				return (string)type_long[i] + addons;
	return (string)"??";

}

void unmangler::register_type() {
	manes::manec::get()->register_type<unmangler>("unmangler", "service");
}
