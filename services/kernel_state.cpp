/* Quarn OS
 *
 * Kernel state controller
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

/* Implementation of kernel state controller which monitors all changes in the
 * way that CPU executes code. Can be used mainly for schedulers and saving
 * energy services.
 */

#include "arch/low/e820.h"
#include "kernel_state.h"
#include "manes/error.h"
#include "manes/manec.h"

using namespace manes;
using namespace resources;
using namespace services;

kernel_state::kernel_state() : _state(starting), time(0) {}

void kernel_state::add_time_tick_call(delegate<void> call) {
	assert("kernel_state: tick call null delegate", call.null());

	time_tick_call.add(call);
}

void kernel_state::del_time_tick_call(delegate<void> call) {
	for (int i = 0; i < time_tick_call.get_count(); i++)
		if (time_tick_call[i] == call)
			time_tick_call.remove(i);
}

void kernel_state::increase_time() {
	time++;

	for (int i = 0; i < time_tick_call.get_count(); i++)
		time_tick_call[i]();
}

int kernel_state::get_time() const {
	return time;
}

error *kernel_state::new_error(const char *x) {
	return new error(x);
}

extern "C" void memcpy(char *a, char *b, int size) {
	for (int i = 0; i < size; i++)
		a[i] = b[i];
}

/* IDEA: allow to create "hooks" that will allow to call a "dynamically set"
 *       function when change from one specific type to another happens
 */
bool kernel_state::set_state(state new_state) {
	if (_state == halting) {
		debug("kernel_state: fail at changing kernel status");
		return false;
	}
	if (new_state == starting) {
		debug("kernel_state: fail at changing kernel status");
		return false;
 	}

	_state = new_state;
	return true;
}

kernel_state::state kernel_state::get_state() const {
	return _state;
}

unsigned int kernel_state::get_memory_size() const {
	return arch::get_memory_size();
}

void kernel_state::register_type() {
	manec::get()->register_type<kernel_state>("kernel_state", "service");
}
