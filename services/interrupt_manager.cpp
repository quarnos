/* Quarn OS
 *
 * Interrupt manager
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "interrupt_manager.h"
#include "manes/manec.h"
#include "manes/error.h"
#include "arch/low/pit.h"
#include "arch/low/irq.h"

using namespace services;

/* Cache Interrupt Manager */
p<interrupt_manager> im = p<interrupt_manager>::invalid;

void interrupt(int i) {
	im->interrupt_called(i);
}

interrupt_manager::interrupt_manager() {
	im = this;
}

void interrupt_manager::register_interrupt(unsigned int i, delegate<void> handler) {
	if (i == 0x32 || i == 0x33) 
		critical("Can not override 0x32 and 0x33 interrupt handlers.");

	handlers[i] = handler;

	arch::unmask_irq(i - 0x20);
}

void interrupt_manager::free_interrupt(unsigned int i) {
	arch::unlock_irqs(i - 0x20);
}

void interrupt_manager::interrupt_called(unsigned int i) {
	if (!handlers[i].null()) {
		try {
			handlers[i]();
		}
		catch (exception *ex) {
			critical("IRQ: WTF?");
		}
	} else
		/* throw */ critical((string)"Unknown interrupt occured: 0x" + string(i,true));
}

void interrupt_manager::register_type() {
	manes::manec::get()->register_type<interrupt_manager>("interrupt_manager", "service");
}
