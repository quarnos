/* Quarn OS
 *
 * Logger
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

/* Abstract interface of loggers. They are an objects that inform user about
 * situations that happened. They decide if a message is worth showing it on
 * the main data output (for example screen) or if it is better to save it to
 * a file. They are the main way for kernel to communicate with the user.
 */

#ifndef _LOGGER_H_
#define _LOGGER_H_

#include "libs/string.h"

#include "manes/cds/component.h"

namespace services {
	class logger : public manes::cds::component {
	public:
		typedef enum {
			log_critical,
			log_failure,
			log_error,
			log_warning,
			log_alert,
			log_info,
			log_debug
		} log_level;

		virtual void print(const string&, log_level) = 0;
	};
}

#endif
