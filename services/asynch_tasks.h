/* Quarn OS
 *
 * Asynchronous tasks
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _ASYNCH_TASK_H_
#define _ASYNCH_TASK_H_

#include "manes/cds/component.h"

#include "libs/fifo.h"
#include "libs/delegate.h"

namespace services {
	class asynch_tasks : public manes::cds::component {
	public:
		typedef delegate<void> asynch_task;

	private:
		fifo<asynch_task> tasks;

		bool working;

		void main_loop();
	public:
		asynch_tasks();

		virtual void add_task(asynch_task task);

		static void register_type();
	};
}

#endif
