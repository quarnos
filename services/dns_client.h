#ifndef _DNS_CLIENT_H_
#define _DNS_CLIENT_H_

#include "resources/net/client_socket.h"
#include "resources/net/udp.h"
#include "resources/net/ipv4_addr.h"

namespace services {
	class dns_client {
	private:
		struct packet {
			u16 id;
			u16 flags;
			u16 questions;
			u16 answers;
			u16 auth_rr;
			u16 add_rr;
		};

		struct dns_answer {
			u16 name;
			u16 a_type;
			u16 a_class;
			u32 ttl;
			u16 length;
			u32 answer;
		} __attribute__((packed));

		enum {
			dns_no_such_name = 3,
			dns_non_auth_ok = 0x10,
			dns_recursion = 0x100,
			dns_truncated = 0x200,
			dns_standard_query = 0,
			dns_question = 0
		};


		struct dns_query {
			u16 q_type;
			u16 q_class;
		};

		enum dns_q_types {
			dns_A = 1
		};

		enum dns_q_class {
			dns_IN = 1
		};

		p<net::udp> u;
	public:
		void set(p<net::udp> _u) {
			u = _u;
		}

		net::ipv4_addr get_ipv4(const string &host) {
			p<packet> pkg = new packet;
			pkg->id = to_be16(0xbeef);
			pkg->flags = to_be16(dns_question | dns_standard_query | dns_recursion);
			pkg->questions = to_be16(1);
			pkg->answers = 0;
			pkg->auth_rr = 0;
			pkg->add_rr = 0;

			buffer buf = buffer::to_mem(pkg);

			list<string> subs = host.split('.');
			for (int i = 0; i < subs.get_count(); i++) {
				buffer size(1);
				size[0] = subs[i].length();
				buf += size;
				buf += subs[i].to_mem().get_next(subs[i].length());
			}

			buffer nul(1);
			nul[0] = 0;
			buf += nul;
			
			p<dns_query> q = new dns_query;
			q->q_type = to_be16(dns_A);
			q->q_class = to_be16(dns_IN);

			buf += buffer::to_mem(q);

			p<net::client_socket> client = u->create_client();
			client->connect(net::ipv4_addr::from_le(62 << 24 | 148 << 16 | 87 << 8 | 180), 53);
			client->write(buf);

			buffer answer;
			client->read(answer);
			client->close();

			if (answer.cast<packet>()->flags & dns_no_such_name)
				return net::ipv4_addr::from_le(0);

			answer = answer.cut_first(sizeof(packet));
			int i;
			for (i = 0; answer[i] && i < answer.get_size(); i++);
			answer = answer.cut_first(i + 1 + sizeof(u16) * 2);

			if (from_be16(answer.cast<dns_answer>()->a_type) != dns_A)
				return net::ipv4_addr::from_le(0);

			return net::ipv4_addr::from_be(answer.cast<dns_answer>()->answer);
		}
	};
}

#endif
