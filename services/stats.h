/* Quarn OS
 *
 * Stats
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "manes/cds/component.h"
#include "libs/string.h"

namespace services {
	class stats : public manes::cds::component {
	public:
		typedef enum {
			efc_remotecalls,
			efc_localcalls,
			efc_calls,
			ks_tickfreq,
			ks_timeticks,
			mem_all,
			mem_allocated
		} stat_value;

		virtual unsigned int get_value(stat_value) const;

		virtual string full_stats() const;

		static void register_type();
	};
}

