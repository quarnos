/* Quarn OS
 *
 * Early logger
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

/* Implementation of a logger that can be used immediately after booting kernel.
 * It does not uses any high-level kernel structures and objects, so it can be
 * a good way to inform about their corruption. However it is not reccomended
 * to use it after setting up "standard" tty logger.
 */

#include "early_logger.h"

#include "server.h"
#include "manes/cds/creator.h"
#include "manes/cds/type.h"
#include "manes/manec.h"

#include "libs/colors.h"

using namespace manes;
using namespace manes::cds;
using namespace services;


early_logger::early_logger() { 
	output = manec::get()->get<resources::stream>("/console");
}

/* Print a log message. This logger prints everything on the screen regardless
 * of its level. At the stage when it is used, all information is imported.
 */
void early_logger::print(const string &message, log_level) {
	assert("early_logger: empty message provided", message.null());

	bool accept_colors = output.is<color_output>();
	if (accept_colors)
		output.cast<color_output>()->set_forecolor(color::white);
	output->write(message.to_mem());
	if (accept_colors)
		output.cast<color_output>()->default_color();
}

void early_logger::register_type() {
	manec::get()->register_type<early_logger>("early_logger", "service");
}

