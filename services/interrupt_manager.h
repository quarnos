/* Quarn OS
 *
 * Interrupt manager
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _INTERRUPT_MANAGER_H_
#define _INTERRUPT_MANAGER_H_

#include "manes/cds/component.h"
#include "libs/delegate.h"
#include "libs/array.h"

namespace services {
	class interrupt_manager : public manes::cds::component {
	private:
		array_stack<delegate<void>, 256> handlers;

	public:
		interrupt_manager();

		/* local */
	       	void interrupt_called(unsigned int i);

		virtual void register_interrupt(unsigned int i, delegate<void> handler);
		//virtual void unregister_interrupt(int i, delegate<void> handler);
		virtual void free_interrupt(unsigned int i);

		static void register_type();
	};
}

#endif
