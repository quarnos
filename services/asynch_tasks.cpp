/* Quarn OS
 *
 * Asynchronous tasks
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "asynch_tasks.h"

#include "actors/actor.h"
#include "actors/director.h"

#include "manes/manec.h"

using namespace services;
using namespace actors;

asynch_tasks::asynch_tasks() : working(false) {}

void asynch_tasks::main_loop() {
	while (1) {
		while (tasks.empty());

		tasks.pop()();
	}
}

void asynch_tasks::add_task(asynch_task task) {
	tasks.push(task);

	if (!working) {
		p<director> sched = manes::manec::get()->get<director>("/director");
		p<actor> loop = sched->new_component(manes::cds::component_name::from_path("/type,actor")).cast<actor>();
		loop->set(delegate<void>::method(this, &asynch_tasks::main_loop));

		working = true;

		sched->launch(loop);
	}
}

void asynch_tasks::register_type() {
	manes::manec::get()->register_type<asynch_tasks>("asynch_tasks", "service");
}
