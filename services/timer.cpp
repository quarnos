/* Quarn OS
 *
 * Timer
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "timer.h"
#include "services/kernel_state.h"
#include "manes/manec.h"

using namespace manes;
using namespace services;

void timer::set_timer(int wait, timer_tick _call) {
	assert("timer: incorrect wait argument", wait < 1);
	assert("timer: incorrect alarm delegate", _call.null());

	ks = manec::get()->state();

	call = _call;

	start = ks->get_time();
	end = start + wait;
	ks->add_time_tick_call(timer_tick::method(this, &timer::time_tick));

}

void timer::time_tick() {
	if (end != ks->get_time()) return;

	ks->del_time_tick_call(timer_tick::method(this, &timer::time_tick));

	call();
}

void timer::register_type() {
	manec::get()->register_type<timer>("timer", "service");
}
