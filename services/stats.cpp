/* Quarn OS
 *
 * Stats
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "stats.h"
#include "manes/manec.h"
#include "arch/low/pit.h"
#include "manes/typeinfo"
#include "resources/foma.h"
#include "services/unmangler.h"
#include "kernel_state.h"

using namespace manes;
using namespace services;

extern unsigned int allocated_memory;
extern int alloc_memory;

unsigned int stats::get_value(stat_value v) const {
	switch (v) {
		case ks_tickfreq :
			return arch::clock_freq;
		case ks_timeticks :
			return manec::get()->state()->get_time();
		case mem_all :
			return manec::get()->state()->get_memory_size();
		case mem_allocated :
			return allocated_memory;
	}

	return 0;
}
extern p<resources::memm> allocator;
string stats::full_stats() const {
	string s = "\nQuarn OS statistics service\n";
	s += "---------------------------\n\n";
	s += (string)" * Hardware\n";
	s += (string)"  -> available memory:\t" + manec::get()->state()->get_memory_size();
	s += (string)"\n  -> allocated memory:\t" + allocated_memory;
	s += (string)"\n  -> efficiency:\t" + (resources::foma::used_memory * 100 / alloc_memory) + "%";
	s += (string)"\n  -> memory allocator:\t" + manec::get()->get<unmangler>("/unmangler")->unmangle(typeid(*allocator).name());
	s += (string)"\n\n * Execution Flow Controller\n";
	s += (string)"  -> remote calls:\t" + get_value(efc_remotecalls);
	s += (string)"\n  -> local calls:\t" + get_value(efc_localcalls);
	s += (string)"\n  -> total:\t\t" + get_value(efc_calls);
	s += (string)"\n\n * Kernel state\n";
	s += (string)"  -> clock frequency:\t" + get_value(ks_tickfreq);
	s += (string)"\n  -> time ticks:\t" + get_value(ks_timeticks);
	s += (string)"\n";
	return s;
}

void stats::register_type() {
	manec::get()->register_type<stats>("stats", "service");
}
