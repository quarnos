/* Quarn OS
 *
 * TTY logger
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _TTY_LOGGER_H_
#define _TTY_LOGGER_H_

#include "logger.h"
#include "resources/stream.h"

namespace services {
	class tty_logger : public logger {
	private:
		p<resources::stream> main_tty, slave_tty;
	public:
		tty_logger();

		void print(const string&, log_level);

		static void register_type();
	};
}

#endif
