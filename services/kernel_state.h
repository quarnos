/* Quarn OS
 *
 * Kernel state controller
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

/* Interface for kernel state controller. Manes has to inform instance of this
 * class when any change of the kernel state happens. Functions that want to
 * monitor changes has to be hardcoded into set_state() procedure.
 */

#ifndef _KERNEL_STATE_H_
#define _KERNEL_STATE_H_

#include "manes/cds/component.h"

#include "libs/list.h"
#include "libs/delegate.h"

namespace manes {
class error;
}

namespace services {

	/**
	 * All information about current kernel state
	 * This class stores all information about current kernel state that
	 * might be useful for other parts of the kernel. This information
	 * should be transported by Manes. This class does not support
	 * multiprocessors machines.
	 */
	class kernel_state : public manes::cds::component {
	public:
		/** Types of execution environment */
		typedef enum {
			starting, /**< kernel is launching */
			kernel, /**< cpu operates in kernel mode */
			user, /**< cpu operates in user mode */
			iowait, /**< cpu waits for hardware */
			frozen, /**< cpu waits for any action */
			halting /**< system is being halted */
		} state;

	private:
		/**
		 * Current execution environment
		 * This variable stores information about current state of the
		 * kernel and cpu.
		 */
		state _state;

		/**
		 * Time count
		 * Number of pit ticks since starting Quarn OS.
		 */
		int time;

		/**
		 * Timers methods
		 * Delegates that have to be called on each pit tick.
		 */
		list<delegate<void> > time_tick_call;

	public:
		kernel_state();

		virtual void add_time_tick_call(delegate<void>);
		virtual void del_time_tick_call(delegate<void>);
		virtual void increase_time();
		virtual int get_time() const;

		virtual bool set_state(state new_state);
		virtual state get_state() const;

		virtual unsigned int get_memory_size() const;

		virtual manes::error *new_error(const char *x);

		static void register_type();
	};
}

#endif
