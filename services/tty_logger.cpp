/* Quarn OS
 *
 * TTY logger
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "manes/manec.h"
#include "tty_logger.h"

using namespace manes;
using namespace services;
using namespace resources;

tty_logger::tty_logger() {
	p<manec> main = manec::get();

	main_tty = main->get<stream>("/keybscr");
	slave_tty = main->get<stream>("/keybscr");
}

void tty_logger::print(const string &message, log_level level) {
	assert("tty_logger: empty log message", message.null());

	if (level <= log_warning)
		main_tty->write(message.to_mem());
	else
		slave_tty->write(message.to_mem());
}

void tty_logger::register_type() {
	manec::get()->register_type<tty_logger>("tty_logger", "service");
}

