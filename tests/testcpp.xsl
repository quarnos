<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">

<!-- Copyright (C) 2009 Pawel Dziepak -->

<!-- Check for problems with new lines -->
<xsl:output method="text" />

<xsl:template match="test">
	<xsl:text>#include &lt;stdio.h&gt;&#xA;#include &lt;string.h&gt;&#xA;</xsl:text>
	<xsl:for-each select="need">
		<xsl:text>#include "</xsl:text>
		<xsl:value-of select="." />
		<xsl:text>"&#xA;</xsl:text>
	</xsl:for-each>
	<xsl:text>&#xA;int main() {&#xA;</xsl:text>
	<xsl:apply-templates />
	<xsl:text>&#xA;}</xsl:text>
</xsl:template>

<xsl:template match="need"></xsl:template>

<xsl:template match="name">
	<xsl:text>printf("</xsl:text>
	<xsl:value-of select="." />
	<xsl:text>\n");</xsl:text>
</xsl:template>


<xsl:template match="object">
	<xsl:text>{&#xA;&#9;&#9;</xsl:text>
	<xsl:value-of select="@class" />
	<xsl:if test="@type">
		<xsl:text>&lt;</xsl:text>
		<xsl:value-of select="@type" />
		<xsl:text>&gt;</xsl:text>
	</xsl:if>
	<xsl:text> obj;</xsl:text>
	<xsl:apply-templates />
	<xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="method">
	<xsl:if test="./return">
		<xsl:text>if (</xsl:text>
	</xsl:if>
	<xsl:text>obj.</xsl:text>
	<xsl:value-of select="@name" />
	<xsl:text>(</xsl:text>
	<xsl:for-each select="arg">
		<xsl:value-of select="." />
		<xsl:if test="not(position()=last())">
			<xsl:text>,</xsl:text>
		</xsl:if>
	</xsl:for-each>
	<xsl:text>)</xsl:text>
	<xsl:if test="./return">
		<xsl:text> != </xsl:text>
		<xsl:value-of select="./return" />
		<xsl:text>) printf("\t  -> failed\n")</xsl:text>
	</xsl:if>
	<xsl:text>;</xsl:text>
</xsl:template>

<xsl:template match="descr">
	<xsl:text>printf("\t* </xsl:text>
	<xsl:value-of select="." />
	<xsl:text>\n");</xsl:text>
</xsl:template>

</xsl:stylesheet>
