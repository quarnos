/* Quarn OS
 *
 * System loader for x86
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

/* This system loader prepares system environment to run the kernel. All basic
 * protected mode features are turned on and set to the temporary values.
 * Loading of kernel also is performed including spcific operations needed
 * by the format of the executable file.
 */

#define KERNEL_ADDRESS 0x20000

#define PAGE_DIR 0x6000
#define PAGE_TABLE 0x2000

#define IDT_ADDRESS 0x4000

#if USE_MULTIBOOT == CONF_YES
#define PTR(x) (x)
#else
#define PTR(x) ((x)+0x8000)
#endif

#define USE_E820
#define KERNEL_FORMAT ELF_STATIC_EXEC

.file "systemload.S"

#if USE_MULTIBOOT != CONF_YES
.code16
#endif

.global _start
_start:
#if USE_MULTIBOOT == CONF_YES
	jmp	PTR(__start)

.align 4
m_boot:
	.long	0x1BADB002 /* magic number */
	.long	0x00003 /* flags */
	.long	-(0x1BADB002+0x00003) /* checksum */
#endif

__start:
#if USE_MULTIBOOT != CONF_YES
	/* Clear screen */
	movb	$0, %ah
	movb	$3, %al
	int	$0x10

	/* Show message */
	movw	$PTR(booting_msg), %si
	call	message

	/* Load file */
	movw	$PTR(kernel), %si
	movw	$0x2000, %ax
	movw	$0, %bx
	call	load_file
	
	/* Get all needed info from BIOS */

#ifdef USE_E820
	/* Detect available RAM using E820 (more info in docs/x86/e820.txt) */
	/* Put received data in 0x1000:0x0000 */
	movw	$0x1000, %ax
	movw	%ax, %es
	xorl	%edi, %edi

	/* Set all registers in the way that BIOS want them to */
	xorl	%ebx, %ebx
	movl	$0x0534D4150, %edx

e820_next_entry:
	movl	$0xE820, %eax
	movl	$24, %ecx

	/* Call E820 */
	int	$0x15

	/* Check if E820 exist on this machine */
	movl	$0x0534D4150, %edx
	cmpl	%eax, %edx
	jne	e820_end

	/* Flag C set, or ebx == 0 means that it is not possible to get next
	 * entry.
	 */
	jc	e820_end

	cmpl	$0, %ebx
	je	e820_end

	/* di is not incremented automatically */
	addw	$24, %di

	jmp	e820_next_entry

	/* It is not needed to analize got data, it will be done later, by
	 * the kernel.
	 */
e820_end:
	/* Clear last position */
	xorl	%eax, %eax
	stosl
	stosl
	stosl
#endif

#ifdef USE_APM	
	/* Configure APM */
#endif
#endif

#if USE_MULTIBOOT == CONF_YES
.code32
#endif
	/* Switching to protected mode (more info in docs/x86/cpu_modes.txt) */

	/* 1. Lock interrupts */
	cli

	/* 2. Load GDT */
	lgdt	PTR(gdt_descr)

	/* 3. Set PE flag */
	movl	%cr0, %eax
	orl	$1, %eax
	mov	%eax, %cr0

	/* 4. Perform a far jump */
	ljmp	$0x08,$PTR(start_code32)

/* File system functions */
#if USE_MULTIBOOT != CONF_YES
#include "fat12.S"

/* Error messages */
file_not_found:
	movw	$PTR(not_found), %si
	jmp	error_end

cannot_read:
	movw	$PTR(read_error), %si
	jmp	error_end

error_end:
	call	message
	jmp	.

/* Show message on screen using BIOS interrupts */
message:
	lodsb

	cmpb	$0, %al
	je	message_end

	movb	$0x0e, %ah
	movw	$0x0001, %bx
	int	$0x10
	jmp	message

message_end:
	ret

booting_msg: .asciz "Booting kernel..."

not_found: .asciz "kernel not found."
read_error: .asciz "read error."
ok_msg: .asciz "OK."

kernel: .ascii "QUARN       "

.code32
#endif

/* So, we are now in PMODE */
start_code32:
	/* 5. Set LDT (unused) */

	/* 6. Set current TSS */
	/* TSS will be set later */

	/* 7. Update segment registers */
	movl	$0x10, %eax
	movw	%ax, %ds
	movw	%ax, %es
	movw	%ax, %fs
	movw	%ax, %gs
	movw	%ax, %ss

	/* 8. Set IDT */
	movl	$0xFF, %ecx
	cld
	movl	$PTR(interrupt_handler), %ebp
	movl	$IDT_ADDRESS, %edi

	interrupts:
		/* Put new entry into IDT */
		/* ISR offset (lower 16bits) */
		movl	%ebp, %eax
		stosw
		/* ISR segment */
		movl	$8, %eax
		stosw
		/* Properties */
		movl	$0xEF00, %eax
		stosw
		/* ISR offset (higher 16bits) */
		movl	%ebp, %eax
		shr	$0x10, %eax
		stosw

	        loop	interrupts

	lidt	PTR(idt_descr)

	/* Set PIC in the correct way (more information in docs/x86/PIC.txt) */
	/* ICW1: initialize PIC, 2 controllers, 4 bytes */
	movb	$0x11, %al
	outb	%al, $0x20
	outb	%al, $0xEB
	outb	%al, $0xA0
	outb	%al, $0xEB

	/* ICW2: interrupts vector's offset
	 * BIOS thinks that irq0 - int 08h (position 20h in vectors table)
	 */
	movb	$0x20, %al
	outb	%al, $0x21
	outb	%al, $0xEB
	addb	$8, %al
	outb	%al, $0xA1
	outb	%al, $0xEB

	/* ICW3: 3rd IRQ is connected with slave PIC */
	movb	$4, %al
	outb	%al, $0x21
	outb	%al, $0xEB
	/* ICW3: ID of slave PIC */
	shrb	$2, %al
	outb	%al, $0xA1
	outb	%al, $0xEB

	/* ICW4: PIC cooperate with CPU */
	shrb	$1, %al
	outb	%al, $0x21
	outb	%al, $0xEB
	outb	%al, $0xA1
	outb	%al, $0xEB

	/* Wait for a while (PIC is slower than CPU) */
	cld
	movl	$0xF000, %ecx
	do_loop:
		loop	do_loop
	/* Disable all IRQ, the needed ones will be enabled by their drivers */
	movb	$0xFB, %al /* parell, fdc, hdc, com1, com2, 8259, keyboard, timer */
	outb	%al, $0x21

	movb	$0xFF, %al /* hdc2, hdc1, fpu, PS/2, n/u, n/u, n/u, rtc */
	outb	%al, $0xA1

	/* 9. Unlock interrupts */
//	sti
#if USE_MULTIBOOT != CONF_YES
	/* Enable A20 */
	call	empty_8042
	movb	$0xD1, %al
	outb	%al, $0x64
	call	empty_8042
	movb	$0xDF, %al
	outb	%al, $0x60
	call	empty_8042
#endif
	sti

	/* Set page dir and page tables */
	/* Map only first 4MiB */
	movl	$0x400, %ecx
	xorl	%eax, %eax
	cld
	movl	$PAGE_TABLE, %edi
	pagetable:
		push	%eax
		orl	$7, %eax
		stosl
		pop	%eax
		addl	$0x1000, %eax
		loop	pagetable

	/* Page directory */
	movl	$PAGE_DIR, %edi
	movl	$PAGE_TABLE, %eax
	orl	$7, %eax
	stosl

	/* Set page directory */
	movl	$PAGE_DIR, %eax
	movl	%eax, %cr3

	/* Enable paging */
	movl	%cr0, %eax
	orl	$0x80000000, %eax
	movl	%eax, %cr0

	movl	$0x100000, %edi
	xorl	%eax, %eax
	movl	$0x80000, %ecx
	rep	stosb
	
#if USE_MULTIBOOT != CONF_YES
	/* Prepare the kernel to run */
	movl	$KERNEL_ADDRESS, %eax
#if KERNEL_FORMAT == ELF_STATIC_EXEC
	movl	KERNEL_ADDRESS+24, %eax

	movl	$KERNEL_ADDRESS, %esi
	movl	$0x100000, %edi
	movl	$0x80000, %ecx
	rep movsb
#endif
#endif
	movl	$0x2FFFFF, %esp
	movl	$0, %ebp
	
	/* Execute the kernel */
#if USE_MULTIBOOT == CONF_YES
.extern start
	call	start
#else
	pushl	$0x08
	pushl	%eax
	lret
#endif

empty_8042:
	inb	$0x64, %al
	test	$2, %al
	jnz	empty_8042
	ret

interrupt_handler:
	iret

/* IDT */
idt_descr:
	.word	256*4*2
	.long	IDT_ADDRESS

gdt_descr:
	.word	gdt_end - gdt
	.long	PTR(gdt)

/* GDT */
.align 0x1000
gdt:
	/* 0 null descriptor */
	.long	0
	.long	0
	/* 1 system-code (0x08) (B: 0000, L: 0FFFF * 4kB) */
	.word	0xFFFF /* limit -| */
	.word	0x0000 /* base --| */
	.byte	0 /* base -|- */
	.byte	0x9A /* 10011010b P, Dpl, DT, type */
	.byte	0xCF /* 11001111b g,d, o, avl, limit |- */
	.byte	0 /* base |-- */
	/* 2 system-data (0x10) (B: 0000, L: 0FFFF * 4kB) */
	.word	0xFFFF /* limit -| */
	.word	0x0000 /* base --| */
	.byte	0 /* base -|- */
	.byte	0x92 /* 10010010b P, Dpl, DT, type */
	.byte	0xCF /* 11001111b g,d, o, avl, limit |- */
	.byte	0 /* base |-- */
	/* 3 user-code (0x18 + 3) (B: 000, L: 0FFFF * 4kB) */
	.word	0xFFFF /* limit -| */
	.word	0x0000 /* base --| */
	.byte	0 /* base -|- */
	.byte	0xFA /* 11111010b P, Dpl, DT, type */
	.byte	0xCF /* 11001111b g,d, o, avl, limit |- */
	.byte	0 /* base |-- */
	/* 4 user-data (0x20 + 3) (B: 0000, L: 0FFFF * 4kB) */
	.word	0xFFFF /* limit -| */
	.word	0x0000 /* base --| */
	.byte	0 /* base -|- */
	.byte	0xF2 /* 11110010b P, Dpl, DT, type */
	.byte	0xCF /* 11001111b g,d, o, avl, limit |- */
	.byte	0 /* base |-- */
.align 0x1000	
gdt_end:

