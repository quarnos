/* Quarn OS
 *
 * Fast implementations of some functions
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

/* Fast log2 */
.global log2
log2:
	addl	$4, %esp
	popl	%eax

	bsr	%eax, %eax

	subl	$8, %esp
	ret

/* Fast memcpy (bugs here) */
// .global memcpy
memcpy:
	movl	%esi, %eax
	movl	%edi, %edx
	
	addl	$4, %esp
	popl	%edi
	popl	%esi
	popl	%ecx

	cld
	rep
	movsb
	
	subl	$0x10, %esp

	movl	%eax, %esi
	movl	%edx, %edi
	ret