/* Quarn OS
 *
 * General definitions
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _GENERAL_H_
#define _GENERAL_H_

#define PAGE_SIZE	4096
#define PAGE_SHIFT	12

#define	GDT_KERNEL_CODE	8
#define GDT_KERNEL_DATA 0x10
#define GDT_USER_CODE	0x1b
#define GDT_USER_DATA	0x23

/*
 XXX: long long is not supported by C++03 (although g++ supports it)
 */

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
#ifndef __cplusplus
typedef unsigned long long u64;
#endif

typedef signed char s8;
typedef signed short s16;
typedef signed int s32;
#ifndef __cplusplus
typedef signed long long s64;
#endif

/* Limits */
#define U8_MAX		255
#define U8_MIN		0

#define S8_MAX		127
#define S8_MIN		-127

#define U16_MAX		65535
#define U16_MIN		0

#define S16_MAX		32767
#define S16_MIN		-32767

#define U32_MAX		4294967295
#define U32_MIN		0

#define S32_MAX		2147483647
#define S32_MIN		-2147483647

#define U64_MAX		18446744073709551615
#define U64_MIN		0

#define S64_MAX		9223372036854775807
#define S64_MIN		-9223372036854775807

/* In Kernel mode? */
inline int in_kernel() {
	unsigned short code_segment;
	__asm__("movw	%%cs, %%ax" : "=a" (code_segment));
	return code_segment == 8;
}

/* Little/Big-endain */

inline u16 swap16(u16 x) {
	u16 y = (x >> 8);
	y |= ((u8)x) << 8;
	return y;
}

inline u32 swap32(u32 x) {
	u32 y = (x >> 24);
	y |= ((u8)x) << 24;
	y |= ((u8)(x >> 8)) << 16;
	y |= ((u8)(x >> 16)) << 8;
	return y;
}

inline u16 to_be16(u16 x) {
	return swap16(x);
}

inline u32 to_be32(u32 x) {
	return swap32(x);
}

inline u16 to_le16(u16 x) {
	return x;
}

inline u32 to_le32(u32 x) {
	return x;
}

inline u16 from_be16(u16 x) {
	return swap16(x);
}

inline u32 from_be32(u32 x) {
	return swap32(x);
}

inline u16 from_le16(u16 x) {
	return x;
}

inline u32 from_le32(u32 x) {
	return x;
}

#endif
