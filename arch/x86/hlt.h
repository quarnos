/* Quarn OS
 *
 * Power-saving instructions
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "lowlevel.h"

static inline void halt_forever() __attribute__((noreturn));

static inline void halt_forever() {
	asm ("cli");
	while (1) asm ("hlt");
}

static inline void wait_action() {
	asm ("sti\nhlt");
}

static inline void wait_key() {
	unsigned int mask = arch::save_mask();
	arch::mask_all();
arch::unlock_irqs(1);
	arch::unmask_irq(arch::keyboard_irqn);
	asm ("sti\nhlt");
	arch::restore_mask(mask);
}

static inline void pause() {
	asm ("pause");
}

static inline void wait_loop() {
	for (int i = 0; i < 0xf000; i++)
		__asm__ __volatile__("pause");
}
