/* Quarn OS
 *
 * RS232 bus driver
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _LRS232_H_
#define _LRS232_H_

#include "resources/device.h"
#include "resources/isa.h"
#include "libs/delegate.h"

namespace arch {
	/** Parity control mode */
	typedef enum {
		pr_none, /**< no control */
		pr_odd, /**< parity bit plus number of set bits is odd */
		pr_even, /**< parity bit plus number of set bits is even */
		pr_mark, /**< parity bit always set */
		pr_space /**< parity bit always zeroed */
	} parity_check;

	typedef enum {
		rs232_simplex, /**< one way transmission */
		rs232_halfduplex, /**< one direction at one time */
		rs232_fullduplex /**< bidirectional transmission */
	} rs232_mode;
	
	void rs232_init(resources::isa_did, delegate<void, const char>);
	void rs232_configure(resources::isa_did, rs232_mode, int, int, parity_check, int);
	void rs232_send(resources::isa_did, const char);

	extern "C" void rs232_received1();
	extern "C" void rs232_received2();
}

#endif
