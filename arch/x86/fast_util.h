/* Quarn OS
 *
 * Fast utils
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _FAST_UTIL_H_
#define _FAST_UTIL_H_

#ifdef __cplusplus
#define c_symbol extern "C"
#else
#define c_symbol
#endif

c_symbol unsigned int log2(unsigned int x);

c_symbol void memcpy(char *, const char *, int);

static inline unsigned int log2up(unsigned int x) {
	return log2(x) + ((unsigned)1 << log2(x) == x ? 0 : 1);
}

#endif
