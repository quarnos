/* Quarn OS
 *
 * DMA
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "general.h"

namespace arch {
	typedef enum {
		dma_demand,
		dma_single,
		dma_block,
		dma_cascade
	} dma_mode;

	typedef enum {
		dma_v_mode,
		dma_dev_read,
		dma_dev_write
	} dma_direction;

	void dma_set_transmisson(u8 channel, void *buffer, u32 counter,  dma_mode mode, bool decr, dma_direction direction);
}
