/* Quarn OS
 *
 * Startup code
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "launch.h"
#include "general.h"
#include "lowlevel.h"
#include "libs/delegate.h"
#include "pit.h"
#include "manes/manec.h"

using namespace arch;

namespace arch {
	idt_record *idt;
	gdt_record *gdt;
}

extern "C" u16 gdt_buf[4];
u16 gdt_buf[4];

extern "C" void *get_manager() {
	return (void*)manes::manec::get();
}

extern "C" void set_normal_int(int i, void(*isr)());
extern "C" void syscall();
extern "C" void managerimpl();


void set_exceptions();

void create_tss(delegate<void*> start, void *pagetable);
void arch::launch() {
	asm("sti");

	/* Get IDT address */
	arch::idt = (idt_record *)0x4000;

	/* Get GDT address */
	__asm__("sgdt	gdt_buf");
	arch::gdt = (gdt_record *)(gdt_buf[1] + (gdt_buf[2] << 16));

	init_irqs();

	set_exceptions();

	set_normal_int(0x33, syscall);
	set_normal_int(0x32, managerimpl);

	/* Initialize PIT */
//	pit_init();
}
