/* Quarn OS
 *
 * E820
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "e820.h"

/* Uses u64 - only for C */
/* e820 record */
struct e820_record {
	u64 base_addr;
	u64 length;
	e820_type type;
	u32 acpi;
};

struct e820_record *e820data;

/* Get size of whole memory */
u32 get_memory_size() {
	int i;

	u64 mem_size = 0;

	e820data = (struct e820_record*)0x10000;
	
	/* Sum sizes of all areas described in e820 records */
	for (i = 0; e820data[i].type; i++)
		mem_size += e820data[i].length;

	/* Correct conversion from u64 to u32 */
	if (mem_size > 0xFFFFFFFF)
		mem_size = 0xFFFFFFFF;

	return (u32)mem_size;
}

/* Get size of space that is accessible */
u32 get_free_memory_size() {
	int i;

	u64 mem_size = 0;

	e820data = (struct e820_record*)0x10000;

	/* Sum sizes of all free areas */
	for (i = 0; e820data[i].type; i++)
		if (e820data[i].type == e820_free)
			mem_size += e820data[i].length;

	/* Correct conversion from u64 to u32 */
	if (mem_size > 0xFFFFFFFF)
		mem_size = 0xFFFFFFFF;

	return (u32)mem_size;
}

