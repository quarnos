/* Quarn OS
 *
 * FDC
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */


/*
 * NOTE
 * FDC orders are explained in file: docs/x86/fdc.txt
 */

#include "general.h"
#include "resources/fdc.h"
#include "lowlevel.h"
#include "dma.h"
#include "hlt.h"
#include "manes/error.h"
#include "manes/manec.h"
#include "services/interrupt_manager.h"

using namespace manes;
using namespace resources;
using namespace arch;

void fdc::fdc_isr_dma() {
	irq_occured = true;
	unlock_irqs(6);
}

void fdc::fdc_isr_normal() {
	fdc_sense_int_status();
	irq_occured = true;
	unlock_irqs(6);
}

void fdc::fdc_init() {
	irq_occured = false;

	manes::manec::get()->get<services::interrupt_manager>("/interrupt_manager")->register_interrupt(isaaddr.irq, delegate<void>::method(this, &fdc::fdc_isr_normal));

	fdc_turn_off();
}

void fdc::fdc_read_sector(void *data, int head, int cyl, int start, int stop) {
	assert("fdc: stop lesser than start", stop < start);
	assert("fdc: incorrect head number", head > 1 || head < 0);
	assert("fdc: incorrect cylinder number", cyl < 0);
	assert("fdc: incorrect sector number", start < 0);
	assert("fdc: incorrect data buffer", data == 0);

	fdc_reset();
	wait_loop();
	fdc_set_disk_type();

	wait_loop();
	//fdc_set_rating();
	isaaddr.outb(7, 0);

	fdc_wait_int();

	fdc_recalibrate();

	fdc_wait_int();

	fdc_seek(head, cyl);

	fdc_wait_int();

	/* Set waiting-for-dma-irq */
	manes::manec::get()->get<services::interrupt_manager>("/interrupt_manager")->register_interrupt(isaaddr.irq, delegate<void>::method(this, &fdc::fdc_isr_dma));

	/* Set dma transmission */
	dma_set_transmisson(isaaddr.dma_channel, (void*)0x50000, (stop - start + 1) * 512, dma_single, false, dma_dev_read);

	fdc_read_data(head, cyl, start, stop);

	fdc_wait_int();
	wait_loop();

	/* Set normal irq */
	manes::manec::get()->get<services::interrupt_manager>("/interrupt_manager")->register_interrupt(isaaddr.irq, delegate<void>::method(this, &fdc::fdc_isr_normal));

	fdc_turn_off();

	u8 *source = (u8*)0x50000;
	for (int i = 0; i < (stop - start + 1) * 512; i++)
		((u8*)data)[i] = source[i];
}

void fdc::fdc_write_sector(void *source, int head, int cyl, int start, int stop) {
	assert("fdc: stop lesser than start", stop < start);
	assert("fdc: incorrect head number", head > 1 || head < 0);
	assert("fdc: incorrect cylinder number", cyl < 0);
	assert("fdc: incorrect sector number", start < 0);
	assert("fdc: incorrect data buffer", source == 0);

	u8 *data = (u8*)0x50000;
	for (int i = 0; i < (stop - start + 1) * 512; i++)
		data[i] = ((u8*)source)[i];

	fdc_reset();
	wait_loop();
	fdc_set_disk_type();

	wait_loop();
	//fdc_set_rating();
	isaaddr.outb(7, 0);

	fdc_wait_int();

	fdc_recalibrate();

	fdc_wait_int();

	fdc_seek(head, cyl);

	fdc_wait_int();

	/* Set waiting-for-dma-irq */
	delegate<void> fdc_dma;
	fdc_dma.method(this, &fdc::fdc_isr_dma);
	manes::manec::get()->get<services::interrupt_manager>("/interrupt_manager")->register_interrupt(isaaddr.irq, fdc_dma);

	/* Set dma transmission */
	dma_set_transmisson(isaaddr.dma_channel, (void*)0x50000, (stop - start + 1) * 512, dma_single, false, dma_dev_write);

	fdc_write_data(head, cyl, start, stop);

	fdc_wait_int();
	wait_loop();
	/* Set normal irq */
	delegate<void> fdc_normal;
	fdc_normal.method(this, &fdc::fdc_isr_normal);
	manes::manec::get()->get<services::interrupt_manager>("/interrupt_manager")->register_interrupt(isaaddr.irq, fdc_normal);

	fdc_turn_off();

}

void fdc::fdc_turn_off() {
	isaaddr.outb(2, 8);
}

void fdc::fdc_reset() {
	fdc_turn_off();

	wait_loop();

	isaaddr.outb(2, 0x1c);
}

void fdc::fdc_wait_int() {
	while (!irq_occured);
	irq_occured = false;
}

void fdc::fdc_wait_command() {
	while (isaaddr.inb(4) & 0x10) __asm__("pause");
}

void fdc::fdc_wait_data() {
	while ((isaaddr.inb(4) & 0x80) != 0x80) __asm__("pause");
}

void fdc::fdc_read_data(int head, int cyl, int start, int stop) {
	fdc_wait_command();
	fdc_wait_data();

	isaaddr.outb(5, 0x46);

	fdc_wait_data();

	isaaddr.outb(5, head << 2);

	fdc_wait_data();

	isaaddr.outb(5, cyl);

	fdc_wait_data();

	isaaddr.outb(5, head);

	fdc_wait_data();

	isaaddr.outb(5, start);

	fdc_wait_data();

	isaaddr.outb(5, 2);

	fdc_wait_data();

	isaaddr.outb(5, stop);

	fdc_wait_data();

	isaaddr.outb(5, 0x46);

	fdc_wait_data();

	isaaddr.outb(5, 0xFF);
}

void fdc::fdc_write_data(int head, int cyl, int start, int stop) {
	fdc_wait_command();
	fdc_wait_data();

	isaaddr.outb(5, 0x45);

	fdc_wait_data();

	isaaddr.outb(5, head << 2);

	fdc_wait_data();

	isaaddr.outb(5, cyl);

	fdc_wait_data();

	isaaddr.outb(5, head);

	fdc_wait_data();

	isaaddr.outb(5, start);

	fdc_wait_data();

	isaaddr.outb(5, 2);

	fdc_wait_data();

	isaaddr.outb(5, stop);

	fdc_wait_data();

	isaaddr.outb(5, 0x46);

	fdc_wait_data();

	isaaddr.outb(5, 0xFF);
}

void fdc::fdc_recalibrate() {
	fdc_wait_command();
	fdc_wait_data();

	isaaddr.outb(5, 7);

	fdc_wait_data();

	isaaddr.outb(5, 0);
}

void fdc::fdc_set_disk_type() {
	fdc_wait_command();
	fdc_wait_data();

	isaaddr.outb(5, 3);

	fdc_wait_data();

	isaaddr.outb(5, 0xdf);

	fdc_wait_data();

	isaaddr.outb(5, 2);
}

void fdc::fdc_seek(int head, int cylinder) {
	fdc_wait_command();
	fdc_wait_data();

	isaaddr.outb(5, 0xf);

	fdc_wait_data();

	isaaddr.outb(5, (u8)head << 2);

	fdc_wait_data();

	isaaddr.outb(5, (u8)cylinder);
}

void fdc::fdc_sense_int_status() {
	fdc_wait_data();

	isaaddr.outb(5, 8);

	fdc_wait_data();

	isaaddr.inb(5);

	fdc_wait_data();

	isaaddr.inb(5);
}
