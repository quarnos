/* Quarn OS
 *
 * PIT
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "pit.h"
#include "general.h"
#include "lowlevel.h"
#include "manes/manec.h"
#include "services/interrupt_manager.h"
#include "services/kernel_state.h"

using namespace manes;

long global_time = 0;

void arch::pit_init() {
	assert("pit: incorrect clock frequency", clock_freq > 1193180 || clock_freq < 0);

	int freq = 1193180 / clock_freq;

	outb(0x43,0x36);
	outb(0x40, (u8)freq);
	outb(0x40, freq>>8);

	manes::manec::get()->get<services::interrupt_manager>("/interrupt_manager")->register_interrupt(0x20, delegate<void>::function(pit_handler));
}

extern "C" void arch::pit_handler() {
	unlock_irqs(0);
	global_time++;
	manec::get()->state()->increase_time();
}
