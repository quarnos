/* Quarn OS
 *
 * Task switching
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

/* This file consist of all functions needed to properly use task switch
 * segments on IA-32 machines, including: dynamicaly modified GDT, creating
 * new TSS structures, assigning new GDT selector and others.
 *
 */

#include "general.h"

#include "manes/manec.h"

namespace arch {
	/* FIXME: should be private */
	void start_tss();

	u32 get_flags();
	u32 get_cr3();

	void create_task_gate();
	unsigned int create_tss(delegate<void> start, void *pagetable);
	void jump_tss(unsigned int tss_sel);

	extern "C" void jump_user(void (*enter_module)(/*manes::component**/));
}
