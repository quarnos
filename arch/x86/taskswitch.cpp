/* Quarn OS
 *
 * Task switching
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

/* This file consist of all functions needed to properly use task switch
 * segments on IA-32 machines, including: dynamicaly modified GDT, creating
 * new TSS structures, assigning new GDT selector and others.
 *
 */

#include "lowlevel.h"
#include "taskswitch.h"

#include "manes/error.h"

using namespace arch;

/* TSS structure */
struct tss_32 {
	u32 previous;
	u32 esp0;
	u16 ss0 __attribute__ ((aligned(4)));
	u32 esp1;
	u16 ss1 __attribute__ ((aligned(4)));
	u32 esp2;
	u16 ss2 __attribute__ ((aligned(4)));
	u32 cr3;
	u32 eip;
	u32 eflags;
	u32 eax;
	u32 ecx;
	u32 edx;
	u32 ebx;
	u32 esp;
	u32 ebp;
	u32 esi;
	u32 edi;
	u16 es __attribute__ ((aligned(4)));
	u16 cs __attribute__ ((aligned(4)));
	u16 ss __attribute__ ((aligned(4)));
	u16 ds __attribute__ ((aligned(4)));
	u16 fs __attribute__ ((aligned(4)));
	u16 gs __attribute__ ((aligned(4)));
	u32 ldt;
	u32 iomap;
	u8 bitmap[100];
};

/* GDT flags */
#define GDTF_SYSTEM	(1<<4)
#define GDTF_DPL(dpl)	((dpl)<<5)
#define GDTF_PRESENT	(1<<7)

#define GDTFL_AVL	(1<<4)
#define GDTFL_LONG	(1<<5)
#define GDTFL_DB	(1<<6)
#define GDTFL_GRANUL	(1<<7)


/* Data segments */
#define GDTT_DATA_RO	0	/* 0000 */
#define GDTT_DATA_ROA	1	/* 0001 */
#define GDTT_DATA_RW	2	/* 0010	*/
#define GDTT_DATA_RWA	3	/* 0011 */
#define GDTT_DATA_ROE	4	/* 0100 */
#define GDTT_DATA_ROEA	5	/* 0101 */
#define GDTT_DATA_RWE	6	/* 0110 */
#define GDTT_DATA_RWEA	7	/* 0111 */

/* Data segments */
#define GDTT_CODE_EO	8	/* 1000 */
#define GDTT_CODE_EOA	9	/* 1001 */
#define GDTT_CODE_ER	10	/* 1010	*/
#define GDTT_CODE_ERA	11	/* 1011 */
#define GDTT_CODE_EOC	12	/* 1100 */
#define GDTT_CODE_EOCA	13	/* 1101 */
#define GDTT_CODE_ERC	14	/* 1110 */
#define GDTT_CODE_ERCA	15	/* 1111 */

/* System segments */
#define GDTT_16TSSAVLB	1	/* 0001 */
#define GDTT_LDT	2	/* 0010 */
#define GDTT_16TSSBUSY	3	/* 0011 */
#define GDTT_16CALGATE	4	/* 0100 */
#define GDTT_TASKGATE	5	/* 0101 */
#define GDTT_16INTGATE	6	/* 0110 */
#define GDTT_16TRPGATE	7	/* 0111 */
#define GDTT_32TSSAVLB	9	/* 1001 */
#define GDTT_32TSSBUSY	11	/* 1011 */
#define GDTT_32CALGATE	12	/* 1100 */
#define GDTT_32INTGATE	14	/* 1110 */
#define GDTT_32TRPGATE	15	/* 1111 */

/* EFLAGS */
#define EFLAG_IF	512

/* Kernel stack size */
namespace arch {
	static const unsigned int kernel_stack_size = 0x1000;

	extern "C" u8 tss_buf;
	u8 tss_buf;

	/* Next segment (TSS) selector */
	u8 last_gdt_sel = 6;

	/* FIXME: needs locking */
	delegate<void> start_saved;
}

void arch::start_tss() {
	start_saved();

	debug("tss: return from initial function");
	asm("cli\nhlt");
}


/* Get current value of eflags */
u32 arch::get_flags() {
	u32 flags;
	asm ("pushf\npop %%eax" : "=a" (flags));
	return flags;
}

/* Get current value of cr3 */
u32 arch::get_cr3() {
	u32 cr3;
	asm ("movl	%%cr3, %%eax" : "=a" (cr3));
	return cr3;
}

/* Jump to the certain TSS */
void arch::jump_tss(unsigned int tss_sel) {
	asm("cli");
	gdt[5].base_lo = tss_sel * 8;
	asm ("ljmp	$0x28,$0");
	asm("sti");
}

void arch::create_task_gate() {
	gdt[5].base_lo = 0x30;
	gdt[5].flags = GDTF_PRESENT | GDTT_TASKGATE;
}

/* Create new TSS */
unsigned int arch::create_tss(delegate<void> start, void *pagetable) {
	start_saved = start;

	u32 base = (u32)new tss_32;
	u32 limit = sizeof(tss_32);

	tss_32 *taskss = (tss_32*)base;

	/* Initialize TSS */
	for (unsigned int i = 0; i < limit; i++)
		((u8*)taskss)[i] = 0;

	/* Fill in TSS data */
	taskss->esp = (u32)new u8[kernel_stack_size];
	taskss->eip = (u32)start_tss;
	taskss->eflags = EFLAG_IF;
	taskss->cr3 = get_cr3();

	taskss->cs = 8;
	taskss->ds = 0x10;
	taskss->es = 0x10;
	taskss->fs = 0x10;
	taskss->gs = 0x10;
	taskss->ss = 0x10;

	taskss->ss0 = 0x10;
	taskss->esp0 = taskss->esp;

	taskss->iomap = __builtin_offsetof(tss_32,bitmap);

	/* Set GDT record */
	gdt[last_gdt_sel].limit_lo = (u16)limit;
	gdt[last_gdt_sel].base_lo = (u16)base;
	gdt[last_gdt_sel].base_mi = (u8)(base >> 16);
	gdt[last_gdt_sel].flags = GDTF_PRESENT | GDTT_32TSSAVLB;
	gdt[last_gdt_sel].flag_limit = (u8)(limit >> 16);
	gdt[last_gdt_sel].base_hi = (u8)(base >> 24);

	if (last_gdt_sel > U8_MAX / 8)
		critical("tss: couldn't create new tss entry in gdt");

	tss_buf = last_gdt_sel * 8;

	if (last_gdt_sel == 6) {
		create_task_gate();
		asm("ltr	tss_buf");
	}

	return last_gdt_sel++;
}
