/* Quarn OS
 *
 * IRQ
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "irq.h"
#include "lowlevel.h"
#include "libs/string.h"
#include "manes/error.h"

using namespace manes;
using namespace arch;

extern "C" void irq0_isr();
extern "C" void irq1_isr();
extern "C" void irq2_isr();
extern "C" void irq3_isr();
extern "C" void irq4_isr();
extern "C" void irq5_isr();
extern "C" void irq6_isr();
extern "C" void irq7_isr();
extern "C" void irq9_isr();
extern "C" void irq10_isr();
extern "C" void irq11_isr();

void interrupt(int i);

namespace arch {

void lock_int() {
	asm ("cli");
}

void unlock_int() {
	asm ("sti");
}

static const int irq_count = 16;



void init_irqs() {
	void (*isr_low)();
lock_int();
//	assert("irq: incorrect irq number to register", i >= irq_count);
	for (int i = 0; i < 12; i++) {
		if (i == 8) continue;
		switch (i) {
			case 0: isr_low = irq0_isr; break;
			case 1: isr_low = irq1_isr; break;
			case 2: isr_low = irq2_isr; break;
			case 3: isr_low = irq3_isr; break;
			case 4: isr_low = irq4_isr; break;
			case 5: isr_low = irq5_isr; break;
			case 6: isr_low = irq6_isr; break;
			case 7: isr_low = irq7_isr; break;
			case 9: isr_low = irq9_isr; break;
			case 10: isr_low = irq10_isr; break;
			case 11: isr_low = irq11_isr; break;
			default : debug((string)"irq: incorrect irq number to register: " + i); return;
		}

		/* Set irq */
		u8 ir = i + 0x20;
		idt[ir].offset_lo = (u16)(u32)isr_low;
		idt[ir].segment = 8;
		idt[ir].flags = IDTF_PRESENT | IDTF_DPL(PL_RING0) | IDTF_TYPE(IDTT_32INTGATE);
		idt[ir].offset_hi = (u16)(((u32)isr_low)>>16);
	}
	unlock_int();
}

void mask_irq(int i) {
	outb(0x21, inb(0x21) | (1 << i));
}

void unmask_irq(int i) {
	if (i < 8)
		outb(0x21, inb(0x21) & ~(1 << i));
	else
		outb(0xa1, int(0xa1) & ~(1 << (i - 8)));
}

unsigned int save_mask() {
	return inb(0x21);
}

void mask_all() {
	outb(0x21, 0xff);
}

void restore_mask(unsigned int x) {
	outb(0x21, x);
}

void unlock_irqs(int i) {
	assert("irq: incorrect irq number to unlock", i >= irq_count);

	if (i >= 8)
		outb(0xa0, 0x20);

	outb(0x20, 0x20);

}

} /* end namespace */

extern "C" void irq0_call() {
	interrupt(0x20);
}

extern "C" void irq1_call() {
	interrupt(0x21);
}

extern "C" void irq2_call() {
	interrupt(0x22);
}

extern "C" void irq3_call() {
	interrupt(0x23);
}

extern "C" void irq4_call() {
	interrupt(0x24);
}

extern "C" void irq5_call() {
	interrupt(0x25);
}

extern "C" void irq6_call() {
	interrupt(0x26);
}

extern "C" void irq7_call() {
	interrupt(0x27);
}

extern "C" void irq9_call() {
	interrupt(0x29);
}

extern "C" void irq10_call() {
	interrupt(0x2a);
}

extern "C" void irq11_call() {
	interrupt(0x2b);
}

/* irq_op */
/*
void irq_op::set_irq(int i, delegate<void>& deleg) {
//	::set_irq(i,deleg);
}*/

void irq_op::unlock_irqs(int i) {
	::unlock_irqs(i);
}

void irq_op::mask_irq(int i) {
	::mask_irq(i);
}

void irq_op::unmask_irq(int i) {
	::unmask_irq(i);
}

unsigned int irq_op::save_mask() {
	return ::save_mask();
}

void irq_op::restore_mask(unsigned int mask) {
	::restore_mask(mask);
}

void irq_op::mask_all() {
	::mask_all();
}
