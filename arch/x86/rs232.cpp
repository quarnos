/* Quarn OS
 *
 * RS232 bus driver
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */


/* More information you can find in the Quarn OS documentation. RS232 bus is
 * described in file docs/x86/rs232.txt.
 */
#include "rs232.h"
#include "arch/low/lowlevel.h"
#include "manes/manec.h"
#include "services/interrupt_manager.h"

using namespace arch;

namespace arch {
	delegate<void, const char> rs232_handler1;
	delegate<void, const char> rs232_handler2;
	rs232_mode com1, com2;

	irq_op *irqs;
}

using namespace resources;

void arch::rs232_init(resources::isa_did addr, delegate<void, const char> sp_hndl) {
	irqs = manes::manec::get()->get_low();

	/* Configure UART */
	rs232_configure(addr, rs232_fullduplex, 0, 8, pr_none, 1);

	/* Call interrupt at every received byte */
	addr.outb(1, 1);

	/* Set IRQ */
	if (addr.irq == 4) {
		manes::manec::get()->get<services::interrupt_manager>("/interrupt_manager")->register_interrupt(addr.irq, delegate<void>::function(rs232_received1));
		rs232_handler1 = sp_hndl;
	} else /*if (addr.irq == 3)*/ {
		manes::manec::get()->get<services::interrupt_manager>("/interrupt_manager")->register_interrupt(addr.irq, delegate<void>::function(rs232_received2));
		rs232_handler2 = sp_hndl;
	}
}

void arch::rs232_configure(resources::isa_did addr, rs232_mode mode, int speed,
			   int bits, parity_check parity, int stop) {
	/* Get access to the clock registers */
	addr.outb(3, addr.inb(3) | (1 << 7));

	/* Set transmission speed (the highest possible) */
	addr.outb(0, 1);
	addr.outb(1, 0);

	/* Restore default registers access */
	addr.outb(3, addr.inb(3) & ~(1 << 7));

	/* Set 8bit data format */
	addr.outb(3, 3);
}

void arch::rs232_send(resources::isa_did addr, const char sign) {
	addr.outb(0, sign);
}

/*
 * TODO: get address from isa_address
 */
extern "C" void arch::rs232_received1() {
	irqs->unlock_irqs(4);
	rs232_handler1(inb(0x3f8));
}

extern "C" void arch::rs232_received2() {
	irqs->unlock_irqs(3);
	rs232_handler2(inb(0x2f8));
}

