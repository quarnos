/* Quarn OS
 *
 * IRQ
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _IRQ_H_
#define _IRQ_H_

#include "libs/delegate.h"

#define PL_RING0	0
#define PL_RING1	1
#define PL_RING2	2
#define PL_RING3	3

#define IDTF_PRESENT	(1<<15)
#define IDTF_DPL(dpl)	((dpl)<<13)
#define IDTF_TYPE(type)	((type)<<8)

#define IDTT_TASKGATE	5	/* 0101 */
#define IDTT_16INTGATE	6	/* 0110 */
#define IDTT_16TRPGATE	7	/* 0111 */
#define IDTT_32INTGATE	14	/* 1110 */
#define IDTT_32TRPGATE	15	/* 1111 */

namespace arch {
	typedef enum {
		pit_irqn,
		keyboard_irqn
	} irq_number;

	void init_irqs();
	void unlock_irqs(int i);
	void mask_irq(int i);
	void unmask_irq(int i);
	unsigned int save_mask();
	void mask_all();
	void restore_mask(unsigned int);

	void lock_int();
	void unlock_int();

	class irq_op {
	public:
		//virtual void set_irq(int i, delegate<void>&);
		virtual void unlock_irqs(int i);

		virtual void mask_irq(int i);
		virtual void unmask_irq(int i);
		virtual unsigned int save_mask();
		virtual void restore_mask(unsigned int);
		virtual void mask_all();
	};
}

#endif
