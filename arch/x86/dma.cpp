/* Quarn OS
 *
 * DMA
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "dma.h"
#include "lowlevel.h"

#include "manes/error.h"

namespace arch {
	const u32 dma_page_reg[] = { 0x87, 0x83, 0x81, 0x82, 0x8d, 0x8b, 0x89, 0x8a };
	static const u8 dma_channel_count = 8;
}

void arch::dma_set_transmisson(u8 channel, void *buffer, u32 counter, dma_mode mode, bool decr, dma_direction direction) {
	assert("dma: incorrect channel", channel >= dma_channel_count);
	assert("dma: incorrect buffer", !buffer);

	/* Lock channel */
	outb(0xa, (channel % 4) | 4);

	/* Set mode */
	outb(0xb, (mode << 6) | decr << 5 | 1 << 4 | direction << 2  | (channel % 4));

	/* Send page */
	outb(dma_page_reg[channel], ((u32)buffer & 0xf0000) >> 16);

	/* Be sure that order of bytes is correct */
	outb(0xc, 0xff);

	/* Send offset */
	if (channel < 4) {
		outb(channel * 2, ((u32)buffer & 0xff));
		outb(channel * 2, (((u32)buffer >> 8) & 0xff));
	} else {
		outb((channel - 4) * 2 + 0xc0, ((u32)buffer & 0xff));
		outb((channel - 4) * 2 + 0xc0, (((u32)buffer >> 8) & 0xff));
	}

	/* Be sure that order of bytes is correct */
	outb(0xc, 0xff);

	/* Send counter */
	if (channel < 4) {
		outb(channel * 2 + 1, (counter & 0xff));
		outb(channel * 2 + 1, ((counter >> 8) & 0xff));
	} else {
		outb((channel - 4) * 2 + 0xc1, (counter & 0xff));
		outb((channel - 4) * 2 + 0xc1, ((counter >> 8) & 0xff));
	}

	/* Unlock channel */
	outb(0xa, (channel % 4));
}
