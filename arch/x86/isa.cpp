/* Quarn OS
 *
 * ISA bus
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "general.h"
#include "lowlevel.h"

#include "manes/manec.h"
#include "resources/unknown.h"

#include "resources/device.h"
#include "resources/keybscr.h"
#include "resources/speaker.h"
#include "resources/fdc.h"

#include "resources/isa.h"

using namespace arch;
using namespace manes;
using namespace manes::cds;
using namespace resources;

int isa::scan_bus() {
	int dev_count = 0;

	/* Keyboard */
	p<isa_did> tty0_addr = new isa_did(this);
	p<device> tty = new_component(component_name::from_path("/type,keybscr")).cast<device>();
	if (tty->init_device(tty0_addr))
		dev_count++;
	/* Get number of installed rs232 ports */
	u8 *p_rs232_count = (u8*)0x411;
	u8 rs232_count = *p_rs232_count & 7;

	/* COM1 (RS232) serial port */
	if (rs232_count > 0) {
		u16 *com1_base_port = (u16*)0x400;
		p<isa_did> com1_addr = new isa_did(this);
		com1_addr->irq = 0x24;
		com1_addr->set_ioport((int)*com1_base_port);

/*		if (manec::get()->get<type>("/type,rs232")) {
			component *c = new_component((manes::type_name)"rs232");
			c->get<device>()->init_device(com1_addr);
		} else {*/
			p<component> c = new_component(component_name::from_path("/type,unknown"));
			c.cast<unknown>()->init_device(com1_addr);
//		}

		dev_count++;
	}

	/* COM2 (RS232) serial port */
	if (rs232_count > 1) {
		u16 *com2_base_port = (u16*)0x402;
		p<isa_did> com2_addr = new isa_did(this);
		com2_addr->irq = 0x23 ;
		com2_addr->set_ioport((int)*com2_base_port);

			p<component> c = new_component(component_name::from_path("/type,unknown"));
			c.cast<unknown>()->init_device(com2_addr);

		dev_count++;
	}

	/* Speaker */
	p<isa_did> speak_addr = new isa_did(this);
	p<speaker> speak = new_component(component_name::from_path("/type,speaker")).cast<speaker>();
	speak->init_device(speak_addr);
	dev_count++;

	/* RTC */

	/* Check if 1.44MB floppy disks drive exist (info stored int cmos) */
	outb(0x70, 0x10);
	u8 fd_drives = inb(0x71);

	/* FDC 1 (A:) */
	if ((fd_drives >> 4) == 4) {
		p<isa_did> fdc_addr = new isa_did(this);
		fdc_addr->irq = 0x26;
		fdc_addr->set_ioport(0x3f0);
		fdc_addr->dma_channel = 2;
		p<fdc> ofdc = new_component(component_name::from_path("/type,fdc")).cast<fdc>();
		ofdc->init_device(fdc_addr);
		dev_count++;
	}

	/* FDC 2 (B:) */
	if ((fd_drives & 0xF) == 4) {
		/* not ready yet 
		isa_address fdc_addr;
		fdc_addr.irq = 6;
		fdc_addr.ioport_base = 0x370;
		fdc_addr.dma_channel = 2;
		fdc *ofdc = (fdc*)rman->new_object("fdc");
		ofdc->init_device(fdc_addr);*/
		dev_count++;
	}

	return dev_count;
}
