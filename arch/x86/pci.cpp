/* Quarn OS
 *
 * PCI
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "lowlevel.h"
#include "resources/bus.h"
#include "manes/error.h"
#include "manes/manec.h"
#include "resources/pci.h"
#include "resources/device.h"
#include "resources/unknown.h"
#include "libs/pointer.h"

using namespace arch;
using namespace resources;

#define pci_register_number(bus, dev, func, dword, ioconf) \
	((ioconf) << 31 | (bus) << 16 | (dev) << 11 | (func) << 8 | (dword) << 2)

const int pci::max_pci_buses = 256;
const int pci::max_pci_devs  = 32;
const int pci::max_pci_funcs = 8;


unsigned int pci::get_conf(int bus, int dev, int func, int dword) const {
	outl(0xcf8, pci_register_number(bus, dev, func, dword, 1));
	return inl(0xcfc);
}

void pci::set_conf(int bus, int dev, int func, int dword, int value) {
	outl(0xcf8, pci_register_number(bus, dev, func, dword, 1));
	outl(0xcfc, value);
}

bool pci::dev_exist(int bus, int dev, int func) {
	return (get_conf(bus, dev, func, 0) & 0xffff) != 0xffff;
}

int pci::start_io = 0xa00;
int pci::allocate_io(int bar) {
	bar = bar & ~1;
	int size;
	asm ("bsf	%%eax, %%eax" : "=a" (size) : "a" (bar));
	size = 1 << size;
	u32 allocated = start_io;
	allocated = (allocated / size + 1) * size;
	start_io += allocated - start_io;
	return allocated & bar;
}

const int pci::free_irqs[] = { 5, 9, 10, 11 };
int pci::last_irq = 0;
int pci::allocate_irq() {
	last_irq++;
	return free_irqs[last_irq % 4];
}

void pci::alloc_resources(p<pci_did> id) {
//	assert("pci: attempt to get address of not existing device", !dev_exist(bus, dev, func));

	int bus = id->ibus;
	int dev = id->device;
	int func = id->function;

	/* Get IRQ line */
	//set_pci_conf(bus, dev, func, 15, (get_pci_conf(bus, dev, func, 15) & ~0xff) | allocate_pci_irq());
	id->irq = get_conf(bus, dev, func, 15) & 0xff;

	set_conf(bus, dev, func, 1, get_conf(bus, dev, func, 1) | 3);

	/* Set IO address space */
	for (int i = 0; i < 5; i++) {
		set_conf(bus, dev, func, 4 + i, 0xffffffff);
		u32 bar = get_conf(bus, dev, func, 4 + i);
		if ((bar & 1) == 1) {
			int ioaddr = allocate_io(bar);
			set_conf(bus, dev, func, i+4, ioaddr);
			id->set_ioport(ioaddr);
		} else if (bar) {
			set_conf(bus, dev, func, i+4, 0x0000);
		}
	}
}

unsigned int pci::get_devid(p<did> id) const {
	p<pci_did> pid = id.cast<pci_did>();
	return get_conf(pid->ibus, pid->device, pid->function, 0);
}

unsigned int pci::get_devclass(p<did> id) const {
	p<pci_did> pid = id.cast<pci_did>();
	return get_conf(pid->ibus, pid->device, pid->function, 2) >> 8;
}

void pci::device_found(int _bus, int dev, int func) {
	p<pci_did> id = new pci_did(this);

	id->ibus = _bus;
	id->device = dev;
	id->function = func;

	create_device(id);
}

int pci::scan_bus() {
	int devices = 0;
	for (int i = 0; i < max_pci_buses; i++) {
		for (int j = 0; j < max_pci_devs; j++) {
			for (int k = 0; k < max_pci_funcs; k++) {
				if (dev_exist(i,j,k)) {
					devices++;

					device_found(i, j, k);
				}
			}
		}
	}
	return devices;
}
