/* Quarn OS
 *
 * Low-level interface
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _LOWLEVEL_H_
#define _LOWLEVEL_H_

#include "irq.h"

#include "general.h"
#include "libs/delegate.h"

namespace arch {
	void outb(u32, u8);
	void outw(u32, u16);
	void outl(u32, u32);

	u8 inb(u32);
	u16 inw(u32);
	u32 inl(u32);

	/* EXPERMIENTAL, do not use
	class lowlevel {
	public:
		virtual void set_irq(int i, delegate<void>);
		virtual void unlock_irqs(int i);

		virtual void outb(u32, u8);
		virtual void outw(u32, u16);
		virtual void outl(u32, u32);

		virtual u8 inb(u32);
		virtual u16 inw(u32);
		virtual u32 inl(u32);
	}; */

	struct idt_record {
		u16 offset_lo;
		u16 segment;
		u16 flags;
		u16 offset_hi;
	} __attribute__((packed));
	extern idt_record *idt;

	struct gdt_record {
		u16 limit_lo;
		u16 base_lo;
		u8 base_mi;
		u8 flags;
		u8 flag_limit;
		u8 base_hi;
	} __attribute__((packed));
	extern gdt_record *gdt;
}

#endif
