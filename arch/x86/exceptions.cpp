/* Quarn OS
 *
 * Exceptions
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "irq.h"
#include "lowlevel.h"
#include "manes/error.h"

using namespace arch;
using namespace manes;

extern "C" void excp0_isr();
extern "C" void excp1_isr();
extern "C" void excp2_isr();
extern "C" void excp3_isr();
extern "C" void excp4_isr();
extern "C" void excp5_isr();
extern "C" void excp6_isr();
extern "C" void excp7_isr();
extern "C" void excp8_isr();
extern "C" void excp9_isr();
extern "C" void excp10_isr();
extern "C" void excp11_isr();
extern "C" void excp12_isr();
extern "C" void excp13_isr();
extern "C" void excp14_isr();
extern "C" void excp16_isr();
extern "C" void excp17_isr();
extern "C" void excp18_isr();
extern "C" void excp19_isr();

void set_excp(int i, void (*isr_low)()) {
	idt[i].offset_lo = (u16)(u32)isr_low;
	idt[i].segment = 8;
	idt[i].flags = IDTF_PRESENT | IDTF_DPL(PL_RING0) | IDTF_TYPE(IDTT_32INTGATE);
	idt[i].offset_hi = (u16)(((u32)isr_low)>>16);
}

void set_exceptions() {
	set_excp(0, excp0_isr);
	set_excp(1, excp1_isr);
	set_excp(2, excp2_isr);
	set_excp(3, excp3_isr);
	set_excp(4, excp4_isr);
	set_excp(5, excp5_isr);
	set_excp(6, excp6_isr);
	set_excp(7, excp7_isr);
	set_excp(8, excp8_isr);
	set_excp(9, excp9_isr);
	set_excp(10, excp10_isr);
	set_excp(11, excp11_isr);
	set_excp(12, excp12_isr);
	set_excp(13, excp13_isr);
	set_excp(14, excp14_isr);
	set_excp(16, excp16_isr);
	set_excp(17, excp17_isr);
	set_excp(18, excp18_isr);
	set_excp(19, excp19_isr);

}

extern "C" void excp0_call() {
	critical("Divide Error Exception (#DE)");
}

extern "C" void excp1_call() {
	critical("Debug Exception (#DB)");
}

extern "C" void excp2_call() {
	critical("NMI Interrupt");
}

extern "C" void excp3_call() {
	critical("Breakpoint Exception (#BP)");
}

extern "C" void excp4_call() {
	critical("Overflow Exception (#OF)");
}

extern "C" void excp5_call() {
	critical("BOUND Range Exceeded Exception (#BR)");
}

extern "C" void excp6_call() {
	critical("Invalid Opcode Exception (#UD)");
}

extern "C" void excp7_call() {
	critical("Device Not Available Exception (#NM)");
}

extern "C" void excp8_call() {
	critical("Double Fault Exception (#DF)");
}

extern "C" void excp9_call() {
	critical("Coprocessor Segment Overrun");
}

extern "C" void excp10_call() {
	critical("Invalid TSS Exception (#TS)");
}

extern "C" void excp11_call() {
	critical("Segment Not Present (#NP)");
}

extern "C" void excp12_call() {
	critical("Stack Fault Exception (#SS)");
}

extern "C" void excp13_call() {
	critical("General Protection Exception (#GP)");
}

extern "C" void excp14_call() {
	critical("Page-Fault Exception (#PF)");
}

extern "C" void excp16_call() {
	critical("x87 FPU Floating-Point Error (#MF)");
}

extern "C" void excp17_call() {
	critical("Alignment Check Exception (#AC)");
}

extern "C" void excp18_call() {
	critical("Machine-Check Exception (#MC)");
}

extern "C" void excp19_call() {
	critical("SIMD Floating Point Exception (#XM)");
}
