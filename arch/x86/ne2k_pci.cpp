/* Quarn OS
 *
 * NE2K on PCI
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

/* No opportunity to test the code. Implementation delayed */
#include "lowlevel.h"
#include "irq.h"
#include "hlt.h"
#include "resources/pci.h"
#include "manes/error.h"
#include "manes/manec.h"
#include "services/interrupt_manager.h"

using namespace resources;
using namespace arch;

enum ne2k_registers {
	ne2k_command = 0,
	ne2k_pstart = 1,
	ne2k_paddr = 1,
	ne2k_pstop = 2,
	ne2k_bnry = 3,
	ne2k_int_status = 7,
	ne2k_curr = 7,
	ne2k_rsar0 = 8,
	ne2k_maddr = 8,
	ne2k_rsar1 = 9,
	ne2k_rbcr0 = 0xa,
	ne2k_rbcr1 = 0xb,
	ne2k_recv_conf = 0xc,
	ne2k_trans_conf = 0xd,
	ne2k_data_conf = 0xe,
	ne2k_int_mask = 0xf,
	ne2k_dataport = 0x10
};

pci_did ne2k;

/* Command Register bits */
#define CR_STOP			1
#define CR_START		2
#define CR_TRANSMIT		4
#define CR_NODMA		0
#define CR_DMA_READ		8
#define CR_DMA_WRITE		0x10
#define CR_DMA_SEND		0x18
#define CR_DMA_ABORT		0x20
#define CR_PAGE0		0
#define CR_PAGE1		0x40
#define CR_PAGE2		0x80

/* Data Configuration Register bits */
#define DCR_16BIT_DMA		1
#define DCR_8BIT_DMA		0
#define DCR_BIGENDAIN		2
#define DCR_LITTLEENDAIN	0
#define DCR_S32DMA		4
#define DCR_D16DMA		0
#define DCR_LOOPBACKOFF		8
#define DCR_LOOPBACKON		0
#define DCR_SENDCMD		0x10
#define DCR_TH_2B		0
#define DCR_TH_4B		0x20
#define DCR_TH_8B		0x40
#define DCR_TH_12B		0x60

void ne2k_pci_read(pci_did pciaddr, u16 addr, u8 *buffer, u16 len) {
	/* Prepare for transmission */
	pciaddr.outb(ne2k_command, CR_START | CR_DMA_ABORT | CR_PAGE0);

	/* Set counter */
	pciaddr.outb(ne2k_rbcr1, (u8)len);
	pciaddr.outb(ne2k_rbcr0, len >> 8);

	/* Set address */
	pciaddr.outb(ne2k_rsar0, (u8)addr);
	pciaddr.outb(ne2k_rsar1, addr >> 8);

	/* Start transmission */
	pciaddr.outb(ne2k_command, CR_START | CR_DMA_READ | CR_PAGE0);

	for (int i = 0; i < len; i++) {
		buffer[i] = pciaddr.inw(ne2k_dataport);
	}

	/* Clear ISR */
	pciaddr.outb(ne2k_int_status, 0x40);
}

void ne2k_pci_write(pci_did pciaddr, u16 addr, u8 *buffer, u16 len) {
	/* Prepare for transmission */
	pciaddr.outb(ne2k_command, CR_START | CR_NODMA | CR_PAGE0);

	/* Set counter */
	pciaddr.outb(ne2k_rbcr0, (u8)len);
	pciaddr.outb(ne2k_rbcr1, len >> 8);

	/* Set address */
	pciaddr.outb(ne2k_rsar0, 0);
	pciaddr.outb(ne2k_rsar1, (u8)addr);

	/* Start transmission */
	pciaddr.outb(ne2k_command, CR_START | CR_DMA_WRITE | CR_PAGE0);

	for (int i = 0; i < len/2; i++) {
		pciaddr.outw(ne2k_dataport, ((u16*)buffer)[i]);
	}

	/* Clear ISR */
	pciaddr.outb(ne2k_int_status, 0x40);
}

void ne2k_pci_irq() {
	char data[200];
	ne2k_pci_read(ne2k, 0x26, (u8*)data, 20);
	asm("cli\nhlt"::"a"(data));
	unlock_irqs(ne2k.irq);
}

void ne2k_pci_send_packet(pci_did pciaddr, u8 *buffer, u16 len) {
	ne2k_pci_write(pciaddr, 0x40, buffer, len);

	pciaddr.outb(6, len >> 8);
	pciaddr.outb(5, (u8)len);

	pciaddr.outb(4, 0x40);

	pciaddr.outb(ne2k_command, 0x26);
}

void get_mac_addr(u8 *mac) {
       	ne2k_pci_read(ne2k, 0, mac, 6);
}

void ne2k_pci_init(pci_did pciaddr) {
	ne2k = pciaddr;

	lock_int();

	/* Turn off, disable DMA */
	pciaddr.outb(ne2k_command, CR_STOP | CR_DMA_ABORT | CR_PAGE0);

	/* Little-endain, no loopback, dual 16bit DMA */
	pciaddr.outb(ne2k_data_conf, DCR_16BIT_DMA | DCR_LITTLEENDAIN | DCR_D16DMA | DCR_LOOPBACKOFF | DCR_SENDCMD | DCR_TH_8B);

	/* Clear RBCR */
	pciaddr.outb(ne2k_rbcr0, 0);
	pciaddr.outb(ne2k_rbcr1, 0);

	/* Accept all packets */
	pciaddr.outb(ne2k_recv_conf, 0);

	/* Transmit page (?) */
	pciaddr.outb(4, 0x20);

	/* Loopback */
	pciaddr.outb(ne2k_trans_conf, 2);

	/* page start and  other strange things */
	pciaddr.outb(ne2k_pstart, 0x26);
	pciaddr.outb(ne2k_pstop,  0x40);
	pciaddr.outb(ne2k_bnry, 0x26);

	/* Turn to page 1 */
	pciaddr.outb(ne2k_command, 0x61);

	/* current page */
	pciaddr.outb(ne2k_curr, 0x26);

	/* Turn to page 0 */
	pciaddr.outb(ne2k_command, CR_STOP| CR_DMA_ABORT | CR_PAGE0);

	/* Clear ISR */
	pciaddr.outb(ne2k_int_status, 0xff);

	/* IRQ on data received and completed DMA transmission */
	//pciaddr.outb(ne2k_int_mask, 0x41);
	pciaddr.outb(ne2k_int_mask, 1);

	/* Launch device */
	pciaddr.outb(ne2k_command, CR_START | CR_DMA_ABORT | CR_PAGE0);

	/* Get MAC address */
	u8 mac[6];
	get_mac_addr(mac);

	/* Turn to page 1 */
	pciaddr.outb(ne2k_command, CR_STOP | CR_DMA_ABORT | CR_PAGE1);

	/* current page */
	pciaddr.outb(ne2k_curr, 0x26);

	/* Set MAC address */
	for (int i = 0; i < 6; i++)
		pciaddr.outb(i + ne2k_paddr, mac[i]);
	

	/* Set multicast address */
	for (int i = 0; i < 8; i++)
		pciaddr.outb(i + ne2k_maddr, 0);

	/* Turn to page 0 */
	pciaddr.outb(ne2k_command, CR_START | CR_DMA_ABORT | CR_PAGE0);

	/* No loopback */
	pciaddr.outb(ne2k_trans_conf, 0);

	manes::manec::get()->get<services::interrupt_manager>("/interrupt_manager")->register_interrupt(ne2k.irq + 0x20, delegate<void>::function(ne2k_pci_irq));

	unlock_int();

}

