/* Quarn OS
 *
 * Virtual 8086 mode support
 *
 * Copyright (C) 2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#define	GDT_KERNEL_CODE	8
#define GDT_KERNEL_DATA 0x10
#define GDT_USER_CODE	0x1b
#define GDT_USER_DATA	0x23
	
saved_stack:	.long 0
saved_ip:	.long 0
saved_ebp:	.long 0
	
.global jump_v86
jump_v86:
	movl	%esp, saved_stack
	movl	$back, saved_ip
	movl	%ebp, saved_ebp
	
	movl	$GDT_USER_DATA, %eax
	movw	%ax, %ds
	movw	%ax, %es
	movw	%ax, %fs
	movw	%ax, %gs

	movl	$test16, %esi
	movl	$0x7000, %edi
	movl	$0x100,	%ecx
	rep	movsb

pushl	0 //$GDT_USER_DATA
	pushl	0xffff //$0x3ff000
	
	pushf
	pop	%edx
	or	$0x20000, %edx
	push	%edx
	
	pushl	$0
	pushl	$0x7000
	iret
back:
	movl	%edx, %esp
	movl	saved_ebp, %ebp
	sti
	ret
	
.code16
test16:
	movw	$0x7fff, %sp
	movw	$0x000, %ax
	movw	%ax, %ss
	movw	%ax, %ds
	movw	%ax, %es
	
	movw	$0x03, %ax
	int	$0x10
	jmp	.
	
.code32
.global vm86_monitor
vm86_monitor:
	movl	8(%esp), %eax
	movl	12(%esp), %edx
	shl	$4, %edx
	orl	%edx, %eax
	movl	(%eax), %eax
/* 0x9c PUSHF */
vm86m_pushf:
	cmp	$0x9c, %al
	jne	vm86m_popf
	
	xorl	%eax, %eax
	movw	20(%esp), %ax
	subw	$2, %ax
	movw	%ax, 20(%esp)

	push	%ebx
	movw	20(%esp), %bx
	movw	%bx, (%eax)
	pop	%ebx

	movl	8(%esp), %eax
	inc	%eax
	movl	%eax, 8(%esp)
	
	pop	%eax
	add	$4, %esp
	
	iret
	
/* 0x9d POPF */
vm86m_popf:
	cmp	$0x9d, %al
	jne	vm86m_int
	
	movl	20(%esp), %eax
	addl	$2, %eax
	movl	%eax, 20(%esp)
	
	subl	$2, %eax
	
	movl	(%eax), %eax
	orl	$0x20000, %eax
	movl	%eax, 16(%esp)

	pop	%eax
	add	$4, %esp
	
	iret

/* 0xcd INT */
vm86m_int:
	cmp	$0xcd, %al
	jne	vm86m_iret
	
	movl	$0x10, %eax
	movw	%ax, %ds
	movw	%ax, %es
	
	movl	0x40, %eax
	movl	%eax, %edx
	shr	$0x10, %edx

	xorl	%ebx, %ebx
	movw	%ax, %bx
	
	movw	$0, %ax
	movw	%ax, %ds
	movw	%ax, %es

	pop	%eax
	
	pushl	$0
	pushl	20(%esp)
	/* consider using IOPL = 3 */
	pushl	$0x20202
	pushl	%edx
	pushl	%ebx
	
	iret
	
vm86m_iret:
	movl	saved_stack, %edx
	movl	saved_ip, %eax
	pushl	$0x10
	pushl	%edx
	pushl	$0
	pushl	$0x8
	pushl	%eax
	iret