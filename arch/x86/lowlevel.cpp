/* Quarn OS
 *
 * Low-level interface
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "general.h"
#include "lowlevel.h"
#include "libs/delegate.h"
#include "manes/error.h"
#include "irq.h"

using namespace arch;

namespace arch {
/* Send a byte to a port */
void outb(u32 port, u8 value) {
	asm ("\toutb\t%%al, %%dx" : : "a" (value), "d" (port));
}

/* Send a word to a port */
void outw(u32 port, u16 value) {
	asm ("\toutw\t%%ax, %%dx" : : "a" (value), "d" (port));
}

/* Send a double word to a port */
void outl(u32 port, u32 value) {
	asm ("\toutl\t%%eax, %%dx" : : "a" (value), "d" (port));
}

/* Get a byte from a port */
u8 inb(u32 port) {
	u8 value;
	asm ("\tinb\t%%dx, %%al" : "=a" (value) : "d" (port));
	return value;
}

/* Get a word from a port */
u16 inw(u32 port) {
	u16 value;
	asm ("\tinw\t%%dx, %%ax" : "=a" (value) : "d" (port));
	return value;
}

/* Get a double word from a port */
u32 inl(u32 port) {
	u32 value;
	asm ("\tinl\t%%dx, %%eax" : "=a" (value) : "d" (port));
	return value;
}

}

extern "C" void set_system_int(int i, void(*isr)()) {
}

extern "C" void set_normal_int(int i, void(*isr)()) {
	idt[i].offset_lo = (u16)(u32)isr;
	idt[i].segment = 8;
	idt[i].flags = IDTF_PRESENT | IDTF_DPL(PL_RING3) | IDTF_TYPE(IDTT_32TRPGATE);
	idt[i].offset_hi = (u16)(((u32)isr)>>16);
}

