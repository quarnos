/* Quarn OS
 *
 * PC speaker
 *
 * Copyright (C) 2008-2009 Pawel Dziepak
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "general.h"
#include "lowlevel.h"
#include "speaker.h"
#include "manes/error.h"

void arch::speaker_change_freq(int freq) {
	/* If freq == 0 then turn speaker off */
	if (freq == 0) {
		speaker_turn_off();
		return;
	} else if (freq > 1193180) {
		debug("speaker: incorrect frequency");
		return;
	}

	/* Count the divisor */
	freq = 1193180 / freq;

	/* Set divisor */
	outb(0x43, 0xb6);
	outb(0x42, (u8)freq);
	outb(0x42, freq >> 8);

	/* Unlock speaker (connect pit to speaker) */
	outb(0x61, inb(0x61) | 3);
}

void arch::speaker_turn_off() {
	/* Disconnect pit and speaker */
	outb(0x61, inb(0x61) & ~3);
}
